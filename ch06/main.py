"""
This module explores the scikit-learn Pipeline class which enables fitting a
model, including any arbitrary number of transformation steps and applying it
to make predictions about new data.

The data set used in this module is the Breast Cancer Wisconsin data set,
which contains 529 samples of malignant and benign tumor cells.  The first
two columns store the unique ID numbers of the samples and the corresponding
diagnoses (M = malignant, B = benign), respectively.  Columns 3-32 contain 30
real-valued features that have been computed from digitised images of the
cell nuclei, which can be used to build a model to predict whether a tumor is
benign or malignant.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import interp
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import (auc, confusion_matrix, f1_score, make_scorer,
                             precision_score, recall_score, roc_curve)
from sklearn.model_selection import (GridSearchCV, StratifiedKFold,
                                     cross_val_score, learning_curve,
                                     train_test_split, validation_curve)
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.utils import resample


def main():
    # Read the data set and split it into training and test data sets.
    df = pd.read_csv(
        'https://archive.ics.uci.edu/ml/'
        'machine-learning-databases'
        '/breast-cancer-wisconsin/wdbc.data',
        header=None)
    print('Breast Cancer Wisconsin data set:\n', df.head())
    print('\nShape:\n', df.shape)
    # Assign the 30 features to a NumPy array X.  Using a LabelEncoder object,
    # transform the class labels from their original string representations
    # 'M' and 'B' into integers.
    X = df.loc[:, 2:].values  # features
    y = df.loc[:, 1].values  # class labels
    le = LabelEncoder()
    y = le.fit_transform(y)
    print('\nEncoded classes:\n', le.classes_)
    # After encoding the class labels (diagnosis) in array y, the malignant
    # tumors are now represented as class 1, and the benign tumors are
    # represented as class 0, respectively.  We can check this mapping by
    # calling the transform() method of the fitted LabelEncoder on two dummy
    # class labels.
    print('\nCheck encoded class label mapping for "M" and "B":\n',
          le.transform(['M', 'B']))
    # Before constructing the first model pipeline, divide the data set into
    # a separate training data set (80%) and a separate test data set (20%)
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, stratify=y, random_state=1)

    # Combining transformers and estimators in a pipeline
    # Many learning algorithms require input features on the same scale for
    # optimum performance.  Thus we need to standardize the columns in the
    # Breast Cancer Wisconsin data set before it can be used with a linear
    # classifier, such as a logistic regression.  Additionally, assume we want
    # to compress the data from the initial 30 dimensions onto a lower 2-d
    # subspace via Principal Component Analysis (PCA), a feature extraction
    # technique for dimensionality reduction.  Instead of going through the
    # fitting and transformation steps for the training and test data sets
    # separately, we can chain the StandardScalar, PCA, and LogisticRegression
    # objects in a pipeline:
    pipe_lr = make_pipeline(
        StandardScaler(), PCA(n_components=2),
        LogisticRegression(random_state=1, solver='liblinear'))
    # The Pipeline fit() method calls the fit() and transform() methods on all
    # of the classes in the pipeline, including the final estimator class.  In
    # this case it called the fit() and transform() methods of the
    # StandardScalar on the training data set and passed the transformed
    # training data to the next object in the pipeline, the PCA() object.  This
    # executed the fit() and tranform() methods on the scaled input data and
    # passed it to the final element of the pipeline, the LogisticRegression
    # estimator.  Finally, the LogisticRegression estimator was fit to the
    # training data after it underwent transformations via StandardScalar and
    # PCA.  There is no limit to the number of intermediate steps in a
    # pipeline; however the last pipeline object has to be an estimator.  The
    # Pipeline looks like this conceptually:
    # Training Data Set -> Scaling -> Dimensional Reduction -> Learning
    #   Algorithm -> Predictive Model
    pipe_lr.fit(X_train, y_train)
    # Similar to calling fit() on a pipeline, Pipeline also implements a
    # predict() method.  Feeding a data set to the predict() of a Pipeline
    # object causes the data to pass through the intermediate steps via
    # transform() calls.  In the final step, the estimator object will return
    # a prediction on the transformed data.  The Pipeline looks like this:
    # Test Data Set -> Scaling -> Dimensional Reduction -> Learning Algorithm
    #   -> Predictive Model -> Class Labels
    y_pred = pipe_lr.predict(X_test)
    print('\nPCA & Logistic Regression pipeline test Accuracy: %.3f' %
          pipe_lr.score(X_test, y_test))

    # Using k-fold cross-validation to assess model performance
    # The holdout method
    # Holdout cross-validation is a classic and popular approach for estimating
    # the generalisation performance of machine learning models.  It involves
    # splitting the initial data set into a separate training and test set; the
    # former is used for model training, and the latter is used to estimate the
    # generalisation performance.  However, we are also interested in tuning
    # and comparing different parameter settings to further improve the
    # performance for making predictions on unseen data.  This process is
    # called model selection, where the term refers to a give classification
    # problem for which we ant to select the optimal values of tuning
    # parameters (hyperparameters).  However, if we reuse the same test data
    # set repeatedly during model selection, it will become part of our
    # data and the model will be more likely to overfit; this is not a good
    # machine learning practice.
    #
    # A better way of using the holdout method is to separate the data into
    # three parts:
    #   1. Training set - used to fit the different models.
    #   2. Validation set - evaluate model performance for model selection.
    #   3. Test set - data unseen by the model during training and model
    #                 selection.
    # The advantage of having a test set that the model hasn't seen before
    # during training and model selection is that we can obtain a less biased
    # estimate of its ability to generalise to new data.  The validation data
    # set is used to repeatedly evaluate the performance of the model after
    # training using different parameter values.  Once we are satisfied with
    # the tuning of the hyperparameter values, we estimate the model's
    # generalisation performance on the test data set.  A disadvantage of the
    # holdout method is that the performance estimate may be very sensitive to
    # how we partition the training set into the training and validation
    # subsets; the estimate will vary for different samples of the data.  A
    # more robust technique for performance estimation is the k-fold cross-
    # validation technique, where the holdout method is repeated k times on k
    # subsets of the training data.
    #
    # k-fold cross-validation
    # This technique randomly splits the training data set into k folds without
    # replacement, where k-1 folds are used for the model training, and one
    # fold is used for performance evaluation.  This procedure is repreated k
    # times so that k models are obtained along with performance estimates.
    # The average performance of the models is calculated based on the
    # different, independent folds to obtain a performance estimate that is
    # less sensitive to the sub-partitioning of the training data compared to
    # the holdout method.  Typically k-fold cross-validation is used for model
    # tuning, finding the optimal hyperparameter values that yields a
    # satisfying generalisation performance.  Once satisfactory hyperparameter
    # values have been found, the model can be retrained on the complete
    # training set and obtain a final performance estimate using the
    # independent test set.  The rationale behind fitting a model to the whole
    # training data set after k-fold cross-validation is that providing more
    # training samples to a learning algorithm usually results in a more
    # accurate and robust model.  Since k-fold cross-validation is a
    # resampling technique without replacement, the advantage of this approach
    # is that each sample point will be used for training and validation (as
    # part of a test fold) exactly once, which yields a lower variance
    # estimate of the model performance than the holdout method.  A good
    # standard value for k in k-fold cross-validation is 10, as empirical
    # evidence shows.  A slight improvement over the standard k-fold cross-
    # validation approach is stratified d-fold cross-validation, which can
    # yield better bias and variance estimates, especially in cases of unequal
    # class proportions.  In stratified cross-validation, the class proportions
    # are preserved in each fold to ensure that each fold is representative of
    # the class proportions in the training data set.
    # Generate indicies to split the data into training and test sets.
    kfold = StratifiedKFold(
        n_splits=10, random_state=1).split(X_train, y_train)
    scores = []
    print('\nStratified k-fold:')
    for k, (train, test) in enumerate(kfold):
        # Use returned indices in train to fit the logistic regression pipeline
        pipe_lr.fit(X_train[train], y_train[train])
        # Use the returned indices in test to calculate the accuracy score
        score = pipe_lr.score(X_train[test], y_train[test])
        # k-iteration score accumulator
        scores.append(score)
        print('Fold: %2d, Class dist.: %s, Acc: %.3f' %
              (k + 1, np.bincount(y_train[train]), score))
    # Output average accuracy and stdandard deviation of the estimate
    print('\nStratified k-fold CV accuracy: %.2f +/_ %.3f' % (np.mean(scores),
                                                              np.std(scores)))
    # scikit-learn implements a k-fold cross-validation scorer, which can be
    # used to evaluate the model using stratified k-fold cross-validation:
    scores = cross_val_score(
        estimator=pipe_lr, X=X_train, y=y_train, cv=10, n_jobs=4)
    print(
        '\nscikit-learn cross-validation for pipeline CV accuracy scores:\n%s'
        % scores)
    print(
        '\nscikit-learn cross-validation for pipeline CV accuracy: %.3f +/- %.3f'
        % (np.mean(scores), np.std(scores)))

    # Debugging algorithms with learning and validation curves
    # This section examines two very simple yet powerful diagnostic tools that
    # can help improve the performance of learning algorithms: learning curves
    # and validation curves.  Learning curves can be used to diagnose whether
    # a learning algorithm has a problem with overfitting (high variance) or
    # underfitting (high bias).  Validation curves can be used to help address
    # common issues of a learning algorithm.

    # Diagnosing bias and variance problems with learning curves
    # If a model is too complex for a given training data set - there are too
    # many degrees of freedom or parameters in the model - the model tends to
    # overfit the training data and does not generalise well to unseen data.

    # Examine how to evaluate the model using the learning curve function from
    # scikit-learn:
    pipe_lr = make_pipeline(
        StandardScaler(),
        LogisticRegression(penalty='l2', random_state=1, solver='liblinear'))

    train_sizes, train_scores, test_scores = learning_curve(
        estimator=pipe_lr,
        X=X_train,
        y=y_train,
        train_sizes=np.linspace(0.1, 1.0, 10),
        cv=10,
        n_jobs=4)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(
        train_sizes,
        train_mean,
        color='blue',
        marker='o',
        markersize=5,
        label='training accuracy')

    plt.fill_between(
        train_sizes,
        train_mean + train_std,
        train_mean - train_std,
        alpha=0.15,
        color='blue')

    plt.plot(
        train_sizes,
        test_mean,
        color='green',
        linestyle='--',
        marker='s',
        markersize=5,
        label='validation accuracy')

    plt.fill_between(
        train_sizes,
        test_mean + test_std,
        test_mean - test_std,
        alpha=0.15,
        color='green')

    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='lower right')
    plt.ylim([0.8, 1.03])
    plt.title('Learning Curve')
    plt.show()

    # Addressing over and underfitting with validation curves
    # Validation curves are useful tools for improving the performance of a
    # model by addressing issues such as overfitting or underfitting.
    # Validaiton curves are related to learning curves, but instead of plotting
    # the training and test accuracies as functions of the sample size, we vary
    # the values of the model parameters, for example, the inverse
    # regularisation parameter C in the logistic regression.  Create validation
    # curves using scikit-learn:
    param_range = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0]
    # Evaluate the inverse regularisation parameter of the LogisticRegression
    train_scores, test_scores = validation_curve(
        estimator=pipe_lr,
        X=X_train,
        y=y_train,
        param_name='logisticregression__C',
        param_range=param_range,
        cv=10)

    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)

    plt.plot(
        param_range,
        train_mean,
        color='blue',
        marker='o',
        markersize=5,
        label='training accuracy')

    plt.fill_between(
        param_range,
        train_mean + train_std,
        train_mean - train_std,
        alpha=0.15,
        color='blue')

    plt.plot(
        param_range,
        test_mean,
        color='green',
        linestyle='--',
        marker='s',
        markersize=5,
        label='validation accuracy')

    plt.fill_between(
        param_range,
        test_mean + test_std,
        test_mean - test_std,
        alpha=0.15,
        color='green')

    plt.grid()
    plt.xscale('log')
    plt.legend(loc='lower right')
    plt.xlabel('Parameter C')
    plt.ylabel('Accuracy')
    plt.ylim([0.8, 1.0])
    plt.title('Validation Curve')
    plt.show()

    # Fine-tuning machine learning models via grid search
    # In machine learning there are two types of parameters: those that are
    # learned from the training data, e.g. the weights in logistic regression,
    # and the parameters of the learning algorithm that are optimised
    # separately - the tuning parameters, also called hyperparameters, e.g. the
    # regularisation parameters in logistic regression, or the depth parameter
    # of a decision tree.  This section explores a popular hyperparameter
    # optimisation technique called grid search that can further help improve
    # the performance of a model by finding the optimal combination of
    # hyperparameter values.

    # Tuning hyperparameters via grid search
    # Grid search is a brute force exhaustive search technique where a list of
    # values for different hyperparameters is specified, and the computer
    # evaluates the model performance for each combination of those to obtain
    # the optimal combination of values from that set.
    pipe_svc = make_pipeline(StandardScaler(), SVC(random_state=1))
    param_range = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
    param_grid = [{
        'svc__C': param_range,
        'svc__kernel': ['linear']
    },
                  {
                      'svc__C': param_range,
                      'svc__gamma': param_range,
                      'svc__kernel': ['rbf']
                  }]
    gs = GridSearchCV(
        estimator=pipe_svc,
        param_grid=param_grid,
        scoring='accuracy',
        cv=10,
        n_jobs=4)
    gs = gs.fit(X_train, y_train)
    print('\nGridSearch SVC pipeline best score:\n', gs.best_score_)
    print('\nGridSearch SVC pipeline best params:\n', gs.best_params_)
    # Use the independent test data set to estimate the performance of the best
    # selected model, which is available via the best_estimator_ attribute of
    # GridSearchCV.
    clf = gs.best_estimator_
    clf.fit(X_train, y_train)
    print('\nUsing best estimator; Test accuracy: %.3f' % clf.score(
        X_test, y_test))

    # Algorithm selection with nested cross-validation
    # Another recommended approach to select among different machine learning
    # algorithms is nested cross-validation.  Studies have found that the true
    # error of the estimate is almost unbiased relative to the test set when
    # nested cross-validation is used.  This technique uses an outer k-fold
    # cross-validation loop to split the data into training and test folds,
    # and an inner loop is used to select the model using k-fold cross-
    # validation on the training fold.  After model selection, the test fold
    # is then used to evaluate the model performance.  The next code segment
    # shows how nested cross-validation can be performed using scikit-learn.
    gs = GridSearchCV(
        estimator=pipe_svc, param_grid=param_grid, scoring='accuracy', cv=2)
    scores = cross_val_score(gs, X_train, y_train, scoring='accuracy', cv=5)
    print('\nGridSearchCV SVC pipeline nested CV accuracy: %.3f +/- %.3f' %
          (np.mean(scores), np.std(scores)))
    # The nested cross-validation approach can be used to compare an SVM model
    # to a simple decision tree classifier (for simplicity, we only tune its
    # depth parameter).
    gs = GridSearchCV(
        estimator=DecisionTreeClassifier(random_state=0),
        param_grid=[{
            'max_depth': [1, 2, 3, 4, 5, 6, 7, None]
        }],
        scoring='accuracy',
        cv=2)
    scores = cross_val_score(gs, X_train, y_train, scoring='accuracy', cv=5)
    print(
        '\nGridSeearchCV Decision Tree Classifier nested CV accuracy: %.3f +/- %.3f'
        % (np.mean(scores), np.std(scores)))

    # Looking at different performance evaluation metrics
    #
    # Up to now model accuracy was the metric used to quantify the performance
    # of a model in general.  There are several other performance metrics that
    # can be used to measure a model's relevance, such as precision, recall,
    # and the F1-score.
    #
    # Reading a confusion matrix
    # A confusion matrix lays out the performance of a learning algorithm.  It
    # reports the counts of True Positive (TP), True Negative (TN), False
    # Positive (FP), and False Negative (FN) predictions of a classifier.
    # Scikit-learn provides a convenient confusion_matrix()function that can
    # be used to compute the confusion matrix.
    pipe_svc.fit(X_train, y_train)
    y_pred = pipe_svc.predict(X_test)
    confmat = confusion_matrix(y_true=y_test, y_pred=y_pred)
    print('\nSVC pipeline confusion matrix:\n', confmat)
    fig, ax = plt.subplots(figsize=(2.5, 2.5))
    # Map the confusion matrix onto a plot using Matplotlib's matshow()
    ax.matshow(confmat, cmap=plt.cm.Blues, alpha=0.3)
    for i in range(confmat.shape[0]):
        for j in range(confmat.shape[1]):
            ax.text(x=j, y=i, s=confmat[i, j], va='center', ha='center')

    plt.xlabel('Predicted label')
    plt.ylabel('True label')
    plt.title('Confusion Matrix')
    plt.show()

    # Optimising the precision and recall of a classification model
    # Both the prediction error (ERR) and accuracy (ACC) provide general
    # information about how many samples are misclassified.  The error is the
    # sum of all false predictions divided by the number of total predictions,
    # and the accuracy is calculated as the sum of correct predictions divided
    # by the total number of predictions.  The scoring metrics are all
    # implemented in scikit-learn and can be imported from the metrics module.
    print('\nscikit-learn module; Precision: %.3f' % precision_score(
        y_true=y_test, y_pred=y_pred))
    print('\nscikit-learn module; Recall: %.3f' % recall_score(
        y_true=y_test, y_pred=y_pred))
    print('\nscikit-learn module; F1: %.3f' % f1_score(
        y_true=y_test, y_pred=y_pred))
    # The positive class in scikit-learn is the class that is labeled as 1.
    # If we want to specify a different positive label, we can construct a
    # custom scorer via the make_scorer() function, which we can provide as
    # an argument to the scoring parameter in GridSearchCV.  In this example
    # using f1_score as a metric.
    scorer = make_scorer(f1_score, pos_label=0)
    c_gamma_range = [0.01, 0.1, 1.0, 10.0]
    param_grid = [{
        'svc__C': c_gamma_range,
        'svc__kernel': ['linear']
    },
                  {
                      'svc__C': c_gamma_range,
                      'svc__gamma': c_gamma_range,
                      'svc__kernel': ['rbf']
                  }]
    gs = GridSearchCV(
        estimator=pipe_svc,
        param_grid=param_grid,
        scoring=scorer,
        cv=10,
        n_jobs=-1)
    gs = gs.fit(X_train, y_train)
    print('\nCustom scorer best score:', gs.best_score_)
    print('\nCustom scorer bets parameters:\n', gs.best_params_)

    # Plotting a receiver operating characteristic
    # Receiver Operating Characteristic (ROC) graphs are are useful tools to
    # select models for classification based on their performance with respect
    # to FPR and TPR, which are computed by shifting the decision threshold of
    # the classifier.  The diagonal of a ROC graph can be interpreted as
    # random guessing, and classification models that fall below the diagonal
    # are considered as worse than random guessing.  A perfect classifier
    # would fall into the top left corner of the graph with a TPR of 1 and a
    # FPR of 0.  Based on the ROC curve, we can compute the ROC Area Under the
    # Curve (ROC AUC) to characterise the performance of a classification
    # model.  The following code plots a ROC curve of a classifier that only
    # uses two features from the Breast Cancer Wisconsin data set to predict
    # whether a tumor is benign or malignant.  Although we uses the same
    # logistic regression pipeline defined previously, we make the
    # classification task more challenging for the classifier so that the
    # resulting ROC curve becomes visually more interesting.  For similar
    # reasons, we also reduce the number of folds in the StratifiedKFold
    # validator to 3.
    pipe_lr = make_pipeline(
        StandardScaler(), PCA(n_components=2),
        LogisticRegression(
            penalty='l2', random_state=1, C=100.0, solver='liblinear'))
    X_train2 = X_train[:, [4, 14]]
    cv = list(
        StratifiedKFold(n_splits=3, random_state=1).split(X_train, y_train))
    fig = plt.figure(figsize=(7, 5))
    mean_tpr = 0.0
    mean_fpr = np.linspace(0, 1, 100)
    all_tpr = []
    for i, (train, test) in enumerate(cv):
        probas = pipe_lr.fit(X_train2[train],
                             y_train[train]).predict_proba(X_train2[test])
        fpr, tpr, thresholds = roc_curve(
            y_train[test], probas[:, 1], pos_label=1)
        mean_tpr += interp(mean_fpr, fpr, tpr)
        mean_tpr[0] = 0.0
        roc_auc = auc(fpr, tpr)
        plt.plot(
            fpr, tpr, label='ROC fold %d (area = %0.2f)' % (i + 1, roc_auc))

    plt.plot([0, 1], [0, 1],
             linestyle='--',
             color=(0.6, 0.6, 0.6),
             label='random guessing')

    mean_tpr /= len(cv)
    mean_tpr[-1] = 1.0
    mean_auc = auc(mean_fpr, mean_tpr)
    plt.plot(
        mean_fpr,
        mean_tpr,
        'k--',
        label='mean ROC (area = %0.2f)' % mean_auc,
        lw=2)
    plt.plot([0, 0, 1], [0, 1, 1],
             linestyle=':',
             color='black',
             label='perfect performance')

    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('false positive rate')
    plt.ylabel('true positive reate')
    plt.legend(loc='lower right')
    plt.title('Receiver Operating Characteristic (ROC) Curve')
    plt.show()

    # Scoring metrics for multiclass classification
    # The scoring metics discussed in this module are specific to binary
    # classification systems.  However, scikit-learn also implements macro and
    # micro averaging methods to extend the scoring metrics to multiclass
    # problems via One-versus-All (OvA) classicication.  Micro-averaging is
    # useful if we ant to weight each instance or prediction equally, whereas
    # macro-averaging weights all classes equally to evaluate the overall
    # performance of a classifier with regard to the most frequent class
    # labels.  If we are  using binary performance metrics to evaluate
    # multiclass classification models in scikit-learn, a normalised or
    # weighted variant of the macro-average is used by default.  The weighted
    # macro-average is calculated by weighting the score of each class label by
    # the number of true instances when calculating the average.  The weighted
    # macro-average is useful if we are dealing with class imbalances, i.e.
    # different numbers of instances for each label.  While the weighted
    # macro-average is the default for multiclass problems in scikit-learn, we
    # can specify the averaging method via the average parameter inside the
    # different scoring functions that we import from sklearn.metrics module,
    # e.g. the precision_score() or make_scorer() functions.
    pre_scorer = make_scorer(
        score_func=precision_score,
        pos_label=1,
        greater_is_better=True,
        average='micro')

    # Dealing with class imbalance
    # Class imbalance occurs when samples from one class or multiple classes
    # are over represented in a data set.  Create an imbalanced data set from
    # the breast cancer data set, which originally consisted of 357 benign
    # tumors (class 0) and 212 malignant tumors (class 1).  Take all 357 benign
    # tumor samples and stack them with the first 40 malignant samples to
    # create a stark imbalance.
    X_imb = np.vstack((X[y == 0], X[y == 1][:40]))
    y_imb = np.hstack((y[y == 0], y[y == 1][:40]))
    # If we computed the accuracy of a model that always predicts the majority
    # class (benign, class 0), we would achieve a prediction accuracy of
    # approximately 90%.
    y_pred = np.zeros(y_imb.shape[0])
    print('\nImbalance mean:', np.mean(y_pred == y_imb) * 100)
    # The scikit-learn library implements a simple resample() function that can
    # help with the upsampling of the minority class by drawing new samples
    # from the data set with replacement.  The following code will take the
    # minority class from the imbalanced breast cancer data set (here class 1)
    # and repeatedly draw new samples from it unitl it contains the same number
    # of samples as class label 0:
    print('\nNumber of class 1 samples before:', X_imb[y_imb == 1].shape[0])
    X_upsampled, y_upsampled = resample(
        X_imb[y_imb == 1],
        y_imb[y_imb == 1],
        replace=True,
        n_samples=X_imb[y_imb == 0].shape[0],
        random_state=123)
    print('\nNumber of class 1 samples after:', X_upsampled.shape[0])
    # After resampling, we can then stack the original class 0 samples with the
    # upsampled class 1 subset to obtain a balanced data set:
    X_bal = np.vstack((X[y == 0], X_upsampled))
    y_bal = np.hstack((y[y == 0], y_upsampled))
    # Consequently, a majority vote prediction rule would only achieve 50%
    # accuracy:
    y_pred = np.zeros(y_bal.shape[0])
    print('\nMean:', np.mean(y_pred == y_bal) * 100)


if __name__ == '__main__':
    main()
