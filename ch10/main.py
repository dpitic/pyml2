"""
This module implements the Regression Analysis examples using the Housing data
set.
"""
import os
import urllib

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import (ElasticNet, Lasso, LinearRegression,
                                  RANSACRegressor, Ridge)
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.tree import DecisionTreeRegressor

import lin_reg_gd as lrgd
import mlutils


def main():
    # Load the Housing data set
    filename = 'housing.data.txt'
    URL = 'https://raw.githubusercontent.com/rasbt/python-machine-learning-book-2nd-edition/master/code/ch10/housing.data.txt'
    if not os.path.isfile(filename):
        # No local copy, so download from server
        print('Downloading Housing data set ...')
        urllib.request.urlretrieve(url=URL, filename=filename)

        # Alternative code to download file directly into DataFrame
        # df = pd.read_csv(URL, header=None, sep='\s+')
    else:
        # Already downloaded, just load the file from disk
        print('Loading Housing data set from file ...')

    df = pd.read_csv(filename, header=None, sep='\s+')

    df.columns = [
        'CRIM', 'ZN', 'INDUS', 'CHAS', 'NOX', 'RM', 'AGE', 'DIS', 'RAD', 'TAX',
        'PTRATIO', 'B', 'LSTAT', 'MEDV'
    ]
    print('Housing data head:')
    print(df.head())

    # Visualising the imported Housing data set
    cols = ['LSTAT', 'INDUS', 'NOX', 'RM', 'MEDV']
    sns.pairplot(df[cols], height=2.5)
    plt.tight_layout()
    plt.show()

    # Looking at relationships using a correlation matrix
    # Correlation matrix is used to quantify and summarise linear
    # relationships between variables.  The correlation matrix is a square
    # matrix that contains the Pearson product-moment correlation coefficient
    # (abbreviated as Person's r), which measures the linear dependence
    # between pairs of features.  The correlation coefficients are in the
    # range -1 to 1.  Two features have perfect positive correlation if r=1,
    # no correlation if r=0, and perfect negative correlation if r=-1.  Use
    # NumPy's corrcoef() function on the five feature columns that were
    # previously plotted, and use Seaborn's heatmap() function to plot the
    # correlation matrix array as a heat map.
    cm = np.corrcoef(df[cols].values.T)
    sns.set(font_scale=1.5)
    hm = sns.heatmap(cm,
                     cbar=True,
                     annot=True,
                     square=True,
                     fmt='.2f',
                     annot_kws={'size': 15},
                     yticklabels=cols,
                     xticklabels=cols)
    plt.show()

    # Solving regression for regression parameters with gradient descent.
    # Use the custom implementation of LinearRegressionGD to predict the
    # MEDV (house prices) from the RM (number of rooms) variable.  Standardise
    # the variables for better convergence of the GD algorithm.
    X = df[['RM']].values
    y = df['MEDV'].values
    sc_x = StandardScaler()
    sc_y = StandardScaler()
    X_std = sc_x.fit_transform(X)
    # Most transformers in skikit-learn expect data to be stored in 2D arrays.
    # Add a new dimension to the y array and then convert back to 1D array
    # after StandardScaler returned the scaled variable.
    y_std = sc_y.fit_transform(y[:, np.newaxis]).flatten()
    lr = lrgd.LinearRegressoinGD()
    lr.fit(X_std, y_std)
    # Plot the cost as a function of the number of epochs passes over the
    # training data set.
    sns.reset_orig()  # reset matplotlit style
    plt.plot(range(1, lr.n_iter + 1), lr.cost_)
    plt.ylabel('SSE')
    plt.xlabel('Epochs')
    plt.show()
    # Plot number of rooms against house price, with linear regression line
    mlutils.lin_regplot(X_std, y_std, lr)
    plt.xlabel('Average number of rooms [RM] (standardised)')
    plt.ylabel('Price in $1000s [MEDV] (standardised)')
    plt.show()
    print('\nSlope: %.3f' % lr.w_[1])
    print('Intercept: %.3f' % lr.w_[0])
    # Scale the predicted price outcome back onto the Price in $1000s axis to
    # predict the price of a house with 5 rooms.
    num_rooms_std = sc_x.transform([[5.0]])
    price_std = lr.predict(num_rooms_std)
    print('Price in $1000s: %.3f' % sc_y.inverse_transform(price_std))

    # Estimating coefficient of a regression model using scikit-learn.  This
    # works with unstandardised variables
    slr = LinearRegression()
    slr.fit(X, y)
    print(
        '\nRegression model using scikit-learn with unstandardised variables')
    print('Slope: %.3f' % slr.coef_[0])
    print('Intercept: %.3f' % slr.intercept_)
    mlutils.lin_regplot(X, y, slr)
    plt.xlabel('Average number of rooms [RM]')
    plt.ylabel('Price in $1000s [MEDV]')
    plt.show()
    # As an alternative to using machine learning libraries, there is also a
    # closed-form solution for solving OLS involving a system of linear
    # equations.
    # Add a column vector of 1s
    Xb = np.hstack((np.ones((X.shape[0], 1)), X))
    w = np.zeros(X.shape[1])
    z = np.linalg.inv(np.dot(Xb.T, Xb))
    w = np.dot(z, np.dot(Xb.T, y))
    print('\nUsing closed-form linear algebra calculation')
    print('Slope: %.3f' % w[1])
    print('Intercept: %.3f' % w[0])

    # Fitting a robust regression model using RANSAC
    # Wrap our linear model in the RANSAC algorithm using scikit-learn's
    # RANSACRegressor class.
    ransac = RANSACRegressor(LinearRegression(),
                             max_trials=100,
                             min_samples=50,
                             loss='absolute_loss',
                             residual_threshold=5.0,
                             random_state=0)
    ransac.fit(X, y)
    # Obtain the inliers and outliers from the fitted RANSAC linear regression
    # model and plot them together with the linear fit.
    inlier_mask = ransac.inlier_mask_
    outlier_mask = np.logical_not(inlier_mask)
    line_X = np.arange(3, 10, 1)
    line_y_ransac = ransac.predict(line_X[:, np.newaxis])
    plt.scatter(X[inlier_mask],
                y[inlier_mask],
                c='steelblue',
                edgecolor='white',
                marker='o',
                label='Inliers')
    plt.scatter(X[outlier_mask],
                y[outlier_mask],
                c='limegreen',
                edgecolor='white',
                marker='s',
                label='Outliers')
    plt.plot(line_X, line_y_ransac, color='black', lw=2)
    plt.xlabel('Average number of rooms [RM]')
    plt.ylabel('Price in $1000s [MEDV]')
    plt.legend(loc='upper left')
    plt.title('RANSAC Model')
    plt.show()
    # The slope and intercept will be slightly different using RANSAC
    print('RANSAC slope: %.3f' % ransac.estimator_.coef_[0])
    print('RANSAC intercept: %.3f' % ransac.estimator_.intercept_)

    # Evaluating the performance of linear regression models
    # Split the data set into separate training and test data sets, using all
    # variables to train a multiple regression model.
    X = df.iloc[:, :-1].values
    y = df['MEDV'].values
    X_train, X_test, y_train, y_test = train_test_split(X,
                                                        y,
                                                        test_size=0.3,
                                                        random_state=0)
    slr = LinearRegression()
    slr.fit(X_train, y_train)
    y_train_pred = slr.predict(X_train)
    y_test_pred = slr.predict(X_test)
    # Since the model uses multiple explanatory variables, the linear
    # regression line (or hyperplane) cannot be visualised in a 2D plot.  The
    # residuals (differences or vertical distances between the actual and
    # predicted values) versus the predicted values can be plotted to diagnose
    # the regression model.
    plt.scatter(y_train_pred,
                y_train_pred - y_train,
                c='steelblue',
                marker='o',
                edgecolor='white',
                label='Training data')
    plt.scatter(y_test_pred,
                y_test_pred - y_test,
                c='limegreen',
                marker='s',
                edgecolor='white',
                label='Test data')
    plt.xlabel('Predicted values')
    plt.ylabel('Residuals')
    plt.legend(loc='upper left')
    plt.hlines(y=0, xmin=-10, xmax=50, color='black', lw=2)
    plt.xlim([-10, 50])
    plt.title('Residuals')
    plt.show()
    # Another useful quantitative measure of a model's performance is the
    # Mean Square Error (MSE), which is the average value of the SSE cost that
    # is minimised to fit the linear regression model.
    print('MSE train: %.3f, test: %.3f' % (mean_squared_error(
        y_train, y_train_pred), mean_squared_error(y_test, y_test_pred)))
    print('R^2 train: %.3f, test: %.3f' %
          (r2_score(y_train, y_train_pred), r2_score(y_test, y_test_pred)))
    # Ridge regression model
    ridge = Ridge(alpha=1.0)
    # LASSO regression model
    lasso = Lasso(alpha=1.0)
    # ElasticNet regression model
    elanet = ElasticNet(alpha=1.0, l1_ratio=0.5)

    # Adding polynomial terms using scikit-learn
    # This block of code uses the PolynomialFeatures transformer class from
    # scikit-learn to add a quadratic term (d=2) to a simple regression
    # problem with one explanatory variable.
    X = np.array(
        [258.0, 270.0, 294.0, 320.0, 342.0, 368.0, 396.0, 446.0, 480.0,
         586.0])[:, np.newaxis]
    y = np.array(
        [236.4, 234.4, 252.8, 298.6, 314.2, 342.2, 360.8, 368.0, 391.2, 390.8])
    lr = LinearRegression()
    pr = LinearRegression()
    quadratic = PolynomialFeatures(degree=2)
    X_quad = quadratic.fit_transform(X)
    # Fit a simple linear regression model for comparison
    lr.fit(X, y)
    X_fit = np.arange(250, 600, 10)[:, np.newaxis]
    y_lin_fit = lr.predict(X_fit)
    # Fit a multiple regression model on the transformed features for
    # polynomial regression
    pr.fit(X_quad, y)
    y_quad_fit = pr.predict(quadratic.fit_transform(X_fit))
    # Plot results
    plt.scatter(X, y, label='training points')
    plt.plot(X_fit, y_lin_fit, label='linear fit', linestyle='--')
    plt.plot(X_fit, y_quad_fit, label='quadratic fit')
    plt.legend(loc='upper left')
    plt.title('Polynomial Regression')
    plt.show()
    y_lin_pred = lr.predict(X)
    y_quad_pred = pr.predict(X_quad)
    print('\nTraining MSE linear: %.3f, quadratic: %.3f' % (mean_squared_error(
        y, y_lin_pred), mean_squared_error(y, y_quad_pred)))
    print('Training R^2 linear: %.3f, quadratic: %.3f' %
          (r2_score(y, y_lin_pred), r2_score(y, y_quad_pred)))

    # Modelling nonlinear relationships in the Housing data set
    # This block of code applies the nonlinear polynomial regression analysis
    # concepts to the Housing data set.  It models the relationship between
    # the house prices and LSTAT (percent lower status of the population)
    # using a second degree (quadratic) and third degree (cubic) polynomial
    # regression model for comparison to the linear fit.
    X = df[['LSTAT']].values
    y = df['MEDV'].values

    regr = LinearRegression()

    # Create polynomial features
    quadratic = PolynomialFeatures(degree=2)
    cubic = PolynomialFeatures(degree=3)
    X_quad = quadratic.fit_transform(X)
    X_cubic = cubic.fit_transform(X)

    # Fit features
    X_fit = np.arange(X.min(), X.max(), 1)[:, np.newaxis]

    regr = regr.fit(X, y)
    y_lin_fit = regr.predict(X_fit)
    linear_r2 = r2_score(y, regr.predict(X))

    regr = regr.fit(X_quad, y)
    y_quad_fit = regr.predict(quadratic.fit_transform(X_fit))
    quadratic_r2 = r2_score(y, regr.predict(X_quad))

    regr = regr.fit(X_cubic, y)
    y_cubic_fit = regr.predict(cubic.fit_transform(X_fit))
    cubic_r2 = r2_score(y, regr.predict(X_cubic))

    # Plot results
    plt.scatter(X, y, label='training points', color='lightgray')

    plt.plot(X_fit,
             y_lin_fit,
             label='linear (d=1), $R^2=%.2f$' % linear_r2,
             color='blue',
             lw=2,
             linestyle=':')
    plt.plot(X_fit,
             y_quad_fit,
             label='quadratic (d=2), $R^2=%.2f$' % quadratic_r2,
             color='red',
             lw=2,
             linestyle='-')
    plt.plot(X_fit,
             y_cubic_fit,
             label='cubic (d=3), $R^2=%.2f$' % cubic_r2,
             color='green',
             lw=2,
             linestyle='--')

    plt.xlabel('% lower status of the population [LSTAT]')
    plt.ylabel('Price in $1000s [MEDV]')
    plt.title('Housing Polynomial Regression Analysis')
    plt.legend(loc='upper right')
    plt.show()

    # Looking at the MEDV-LSTAT scatter plot may lead to the hypothesis that
    # a log transformation of the LSTAT feature variable and the square root
    # of MEDV may project the data onto a linear feature space suitable for
    # linear regression fit.  Test this hypothesis here, which will show that
    # after transforming the explanatory variable onto the log space and
    # taking the square root of the target variables, it is possible to
    # capture the relationship between the two variables with a linear
    # regression line that appears to fit the data better (R^2=0.69) than any
    # of the polynomial feature transformations previously used.
    # Transform features
    X_log = np.log(X)
    y_sqrt = np.sqrt(y)

    # Fit features
    X_fit = np.arange(X_log.min() - 1, X_log.max() + 1, 1)[:, np.newaxis]

    regr = regr.fit(X_log, y_sqrt)
    y_lin_fit = regr.predict(X_fit)
    linear_r2 = r2_score(y_sqrt, regr.predict(X_log))

    # Plot the results
    plt.scatter(X_log, y_sqrt, label='training points', color='lightgray')

    plt.plot(X_fit,
             y_lin_fit,
             label='linear (d=1), $R^2=%.2f$' % linear_r2,
             color='blue',
             lw=2)
    plt.xlabel('log(% lower status of the population [LSTAT])')
    plt.ylabel('$\sqrt{Price \; in \; \$1000s \; [MEDV]}$')
    plt.legend(loc='lower left')
    plt.title('Log Transformation')
    plt.show()

    # Dealing with nonlinear relationships using random forests
    # Decision tree regression
    # Plot the line fit of a decision tree using the DecisionTreeRegressor
    # class implemented in scikit-learn to model the nonlinear relationship
    # between the MEDV and LSTAT variables.
    X = df[['LSTAT']].values
    y = df['MEDV'].values
    tree = DecisionTreeRegressor(max_depth=3)
    tree.fit(X, y)
    sort_idx = X.flatten().argsort()
    mlutils.lin_regplot(X[sort_idx], y[sort_idx], tree)
    plt.xlabel('% lower status of the population [LSTAT]')
    plt.ylabel('Price in $1000s [MEDV]')
    plt.title('Decision Tree Line Fit')
    plt.show()

    # Random forest regression
    # Use all features in the Housing data set to fit a random forest
    # regression model on 60% of the samples and evaluate its performance on
    # the remaining 40%.
    X = df.iloc[:, :-1].values
    y = df['MEDV'].values
    X_train, X_test, y_train, y_test = train_test_split(X,
                                                        y,
                                                        test_size=0.4,
                                                        random_state=1)
    forest = RandomForestRegressor(n_estimators=1000,
                                   criterion='mse',
                                   random_state=1,
                                   n_jobs=-1)
    forest.fit(X_train, y_train)
    y_train_pred = forest.predict(X_train)
    y_test_pred = forest.predict(X_test)
    print('\nRandom forest regression:')
    print('MSE train: %.3f, test: %.3f' % (mean_squared_error(
        y_train, y_train_pred), mean_squared_error(y_test, y_test_pred)))
    print('R^2 train: %.3f, test: %.3f' %
          (r2_score(y_train, y_train_pred), r2_score(y_test, y_test_pred)))
    # Unfortunately, the random forest tends to overfit the training data.
    # However, it is still able to explain the relationship between the target
    # and explanatory variables relatively well (R^2=0.87) on the test data
    # set.  Now plot the residuals of the prediction.
    plt.scatter(y_train_pred,
                y_train_pred - y_train,
                c='steelblue',
                edgecolor='white',
                marker='o',
                s=35,
                alpha=0.9,
                label='training data')
    plt.scatter(y_test_pred,
                y_test_pred - y_test,
                c='limegreen',
                edgecolor='white',
                marker='s',
                s=35,
                alpha=0.9,
                label='test data')
    plt.xlabel('Predicted values')
    plt.ylabel('Residuals')
    plt.legend(loc='upper left')
    plt.hlines(y=0, xmin=-10, xmax=50, lw=2, color='black')
    plt.xlim([-10, 50])
    plt.title('Random Forest Regression')
    plt.show()


if __name__ == '__main__':
    main()
