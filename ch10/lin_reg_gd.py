"""
This module implements an ordinary least squares (OLS) linear regression
model.
"""
import numpy as np


class LinearRegressoinGD():
    """Ordinary Least Squares (OLS) linear regression model with gradient
    descent.
    """

    def __init__(self, eta=0.001, n_iter=20):
        self.eta = eta
        self.n_iter = n_iter

    def fit(self, X, y):
        """Train the model using features and labels.

        :param X: Feature matrix.
        :param y: Label vector.
        :return: Trained model object.
        """
        self.w_ = np.zeros(1 + X.shape[1])
        self.cost_ = []

        for i in range(self.n_iter):
            output = self.net_input(X)
            errors = (y - output)
            self.w_[1:] += self.eta * X.T.dot(errors)
            self.w_[0] += self.eta * errors.sum()
            cost = (errors**2).sum() / 2.0
            self.cost_.append(cost)
        return self

    def net_input(self, X):
        """Return the net input of the neuron.

        :param X: Feature matrix.
        :return: Net input of the neuron.
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def predict(self, X):
        """Predict class labes from the given feature matrix.

        Using the trained model, predict the unknown class labels for the
        given feature matrix X.
        :param X: Feature matrix to predict class labels for.
        :return: Vector of predicted class labels.
        """
        return self.net_input(X)
