"""
Implementation of the Logistic Regression with Gradient Descent algorithm.
"""
import numpy as np


class LogisticRegressionGD(object):
    """
    Logistic Regression classifier using gradient descent.
    """

    def __init__(self, eta=0.05, n_iter=100, random_state=1):
        """
        Initialise a new logistic regression classifier with gradient descent
        object.
        :param eta:           Learning rate (between 0.0 and 1.0).
        :param n_iter:        Number of passes over the training data set
                              (epochs).
        :param random_state:  Random number generator seed for random weight
                              initialisation.
        """
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state

    def fit(self, X, y):
        """
        Fit training data.
        :param X: array-like, shape = [n_samples, n_features]
                  Training vectors matrix.
        :param y: array-like, shape = [n_samples]
                  Target values vector.
        :return:  Trained model object.
        """
        rgen = np.random.RandomState(self.random_state)
        # 1d-array of weights after fitting
        self.w_ = rgen.normal(loc=0.0, scale=0.01, size=1 + X.shape[1])
        # Logistic cost function value in each epoch.
        self.cost_ = []

        for i in range(self.n_iter):
            net_input = self.net_input(X)
            output = self.activation(net_input)
            errors = y - output
            # Calculate weights for 1 to m for whole training data set
            self.w_[1:] += self.eta * X.T.dot(errors)
            # For bias unit (zero weight) calculate gradient based on the whole
            # training data set.
            self.w_[0] += self.eta * errors.sum()
            # Compute the logistic cost now instead of the sum of squared errors
            # cost.
            cost = -y.dot(np.log(output)) - ((1 - y).dot(np.log(1 - output)))
            self.cost_.append(cost)
        return self

    def net_input(self, X):
        """
        Calculate the net input for the neuron.

        Calculate z, the net input for the neuron, which is the dot product of
        the weights and the inputs.
        :param X: Features matrix (shape = [n_samples, n_features]).
        :return:  Net input vector (shape = [n_samples]).
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, z):
        """
        Compute the logistic sigmoid activation.
        :param z: Net input vector (shape = [n_features]).
        :return:  Activated vector (shape = [n_samples]).
        """
        return 1.0 / (1.0 + np.exp(-np.clip(z, -250, 250)))

    def predict(self, X):
        """
        Return the class label after unit step.

        Applies the decision (threshold) function to the net input to return
        the class label 1 if net input > 0, or 0 otherwise.
        :param X: Features matrix (shape [n_samples, n_features]).
        :return:  Class label 1 or 0, after unit step.
        """
        return np.where(self.net_input(X) >= 0.0, 1, 0)
