"""
This module is used to explore the scikit-learn Python library by training a
perceptron model using the Iris data set.  Scikit-learn provides APIs and the
Iris data set.
"""
import matplotlib.pyplot as plt
import numpy as np
from pydotplus import graph_from_dot_data
from sklearn import datasets
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, Perceptron, SGDClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier, export_graphviz

import logistic_regression_gd as model
import mlutils


def main():
    # Scikit-learn provides the Iris data set.  Third column is petal length,
    # fourth column is petal width.  Classes are already converted to integer
    # labels where 0=Iris-Setosa, 1=Iris-Versicolor, 2=Iris-Virginica.
    iris = datasets.load_iris()
    # Assign petal length and petal width to feature matrix X
    X = iris.data[:, [2, 3]]
    y = iris.target
    print('Class labels:', np.unique(y))

    # Randomly split the data into 70% training and 30% test data
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.3, random_state=1, stratify=y)
    print('Labels counts in y:', np.bincount(y))
    print('Labels counts in y_train:', np.bincount(y_train))
    print('Labels counts in y_test:', np.bincount(y_test))

    # Standardise the features for optimal algorithm performance
    sc = StandardScaler()
    # Compute the mean and standard deviation for each feature dimension from
    # the training data to be used for scaling train and test data.
    sc.fit(X_train)
    # Perform standardisation by centering and scaling train and test features
    X_train_std = sc.transform(X_train)
    X_test_std = sc.transform(X_test)

    # Train the perceptron model.  Most algorithms in scikit-learn support
    # multiclass classification by default via the One-versus-Rest (OvR) method,
    # so all three flower classes can be passed to the perceptron.
    ppn = Perceptron(max_iter=40, eta0=0.1, random_state=1)
    ppn.fit(X_train_std, y_train)

    # Make predictions
    y_pred = ppn.predict(X_test_std)
    print(f'Misclassified samples: {(y_test != y_pred).sum()}')
    print(f'Accuracy: {accuracy_score(y_test, y_pred):.2f}')
    print(f'Accuracy: {ppn.score(X_test_std, y_test):.2f}')

    # Plot decision regions
    X_combined_std = np.vstack((X_train_std, X_test_std))
    y_combined = np.hstack((y_train, y_test))
    mlutils.plot_decision_regions(
        X=X_combined_std,
        y=y_combined,
        classifier=ppn,
        test_idx=range(105, 150))
    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.legend(loc='upper left')
    plt.show()

    # Logistic regression intuition and conditional probabilities
    z = np.arange(-7, 7, 0.1)
    phi_z = mlutils.sigmoid(z)

    # Plot the logistic sigmoid function
    plt.plot(z, phi_z)
    plt.axvline(0.0, color='k')
    plt.ylim(-0.1, 1.1)
    plt.xlabel('z')
    plt.ylabel('$\phi(z)$')

    # y axis ticks and gridline
    plt.yticks([0.0, 0.5, 1.0])
    ax = plt.gca()
    ax.yaxis.grid(True)
    plt.show()

    # Learning the weights of the logistic cost function
    z = np.arange(-10, 10, 0.1)
    phi_z = mlutils.sigmoid(z)

    c1 = [mlutils.cost_1(x) for x in z]
    plt.plot(phi_z, c1, label='J(w) if y = 1')

    c0 = [mlutils.cost_0(x) for x in z]
    plt.plot(phi_z, c0, linestyle='--', label='J(w) if y = 0')

    plt.ylim(0.0, 5.1)
    plt.xlim([0, 1])
    plt.xlabel('$\phi$(z)')
    plt.ylabel('J(w)')
    plt.legend(loc='best')
    plt.show()

    # Logistric regression model (setosa = 0, versicolor = 1)
    X_train_01_subset = X_train[(y_train == 0) | (y_train == 1)]
    y_train_01_subset = y_train[(y_train == 0) | (y_train == 1)]

    lrgd = model.LogisticRegressionGD(eta=0.05, n_iter=1000, random_state=1)
    lrgd.fit(X_train_01_subset, y_train_01_subset)

    mlutils.plot_decision_regions(
        X=X_train_01_subset, y=y_train_01_subset, classifier=lrgd)

    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.legend(loc='upper left')
    plt.show()

    # Training a logistic regression model with scikit-learn
    lr = LogisticRegression(C=100.0, random_state=1,
                            solver='liblinear', multi_class='auto')
    lr.fit(X_train_std, y_train)
    mlutils.plot_decision_regions(
        X_combined_std, y_combined, classifier=lr, test_idx=range(105, 150))
    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.legend(loc='upper left')
    plt.show()

    # Calculate the probability that training examples belong to a certain
    # class (look at first 3 rows).  Each row corresponds to the class
    # membership for each flower in the standardised test data set.  The sum of
    # the column values in each row is 1.
    print(lr.predict_proba(X_test_std[:3, :]))
    print(lr.predict_proba(X_test_std[:3, :]).sum(axis=1))
    # Obtain the predicted class labels by identifying the largest column in
    # each row.  The returned class indices correspond to Iris-virginica,
    # Iris-setosa, and Iris-setosa.
    print(lr.predict_proba(X_test_std[:3, :]).argmax(axis=1))
    # This demonstrated a manual approach to calling the predict() method
    print(lr.predict(X_test_std[:3, :]))
    # In order to predict the class label of a single flower sample, the
    # scikit-learn api expects a two-dimensional array as data input.  The
    # single row slice has to be converted into a 2-D array.
    print(lr.predict(X_test_std[0, :].reshape(1, -1)))

    # Tackling overfitting via regularisation
    weights, params = [], []
    # Fit some logistic regression models with different values for the
    # inverse regularisation parameter C
    for c in np.arange(-5, 5):
        lr = LogisticRegression(C=10.**c, random_state=1,
                                solver='liblinear', multi_class='auto')
        lr.fit(X_train_std, y_train)
        # Collect weight coefficients for class 1, the 2nd class in the data
        # set, Iris-versicolor.
        weights.append(lr.coef_[1])
        params.append(10.**c)

    weights = np.array(weights)
    plt.plot(params, weights[:, 0], label='petal length')
    plt.plot(params, weights[:, 1], linestyle='--', label='petal width')
    plt.ylabel('weight coefficient')
    plt.xlabel('C')
    plt.legend(loc='upper left')
    plt.xscale('log')
    plt.title('Iris-versicolor')
    plt.show()

    # Maximum margin classification with support vector machines (SVM)
    svm = SVC(kernel='linear', C=1.0, random_state=1)
    svm.fit(X_train_std, y_train)
    mlutils.plot_decision_regions(
        X_combined_std, y_combined, classifier=svm, test_idx=range(105, 150))
    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.legend(loc='upper left')
    plt.title('Support Vector Machine')
    plt.show()

    # Alternative implementations in scikit-learn.  The scikit-learn library
    # Perceptron and LogisticRegression classes use the LIBLINEAR library
    # which is a highly optimised C/C++ library.  The SVC class uses the
    # LIBSVM C/C++ library.  The advantage of these libraries over native
    # Python implementations is that they allow for extremely fast training of
    # large amounts of linear classifiers.  However, sometimes the data sets
    # are too large to fit into memory, and scikit-learn provides alternative
    # implementations via the SGDClassifier class, which also supports online
    # learning via the partial_fit() method.  Stochastic gradient descent
    # version of the Perceptron, Logistic Regression, and Support Vector
    # Machine with default parameters would be initialised as follows:
    ppn = SGDClassifier(loss='perceptron')
    lr = SGDClassifier(loss='log')
    svm = SGDClassifier(loss='hinge')

    # Solving nonlinear problems using a kernel SVM.
    # SVMs are popular because they can be easily kernelised to solve nonlinear
    # classificaiton problems.
    # Create a sample data set to visualise a nonlinear classification problem,
    # which has the form of an XOR gate using the logical_or() function from
    # NumPy, where 100 samples will be assigned the class label 1, and 100
    # samples will be assigned the class label -1:
    np.random.seed(1)
    X_xor = np.random.randn(200, 2)
    y_xor = np.logical_xor(X_xor[:, 0] > 0, X_xor[:, 1] > 0)
    y_xor = np.where(y_xor, 1, -1)

    plt.scatter(
        X_xor[y_xor == 1, 0],
        X_xor[y_xor == 1, 1],
        c='b',
        marker='x',
        label='1')
    plt.scatter(
        X_xor[y_xor == -1, 0],
        X_xor[y_xor == -1, 1],
        c='r',
        marker='s',
        label='-1')

    plt.xlim([-3, 3])
    plt.ylim([-3, 3])
    plt.legend(loc='best')
    plt.title('Nonlinear Classification Sample Data')
    plt.show()

    # Using the kernel trick to find separating hyperplanes in high-dimensional
    # space.  Use the Radial Basis Functions (RBF) kernel (Gaussian kernel).
    svm = SVC(kernel='rbf', random_state=1, gamma=0.1, C=10.0)
    svm.fit(X_xor, y_xor)
    mlutils.plot_decision_regions(X_xor, y_xor, classifier=svm)
    plt.legend(loc='upper left')
    plt.title('Kernel SVM Decision Boundary')
    plt.show()

    # Apply the RBF kernel SVM to the Iris flower data set, using a relatively
    # small cut-off parameter (gamma)
    svm = SVC(kernel='rbf', random_state=1, gamma=0.2, C=1.0)
    svm.fit(X_train_std, y_train)

    mlutils.plot_decision_regions(
        X_combined_std, y_combined, classifier=svm, test_idx=range(105, 150))
    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.legend(loc='upper left')
    plt.title('RBF Kernel SVM (small gamma = 0.2)')
    plt.show()

    # Increase the cut-off parameter (gamma), which will increase the influence
    # or reach of the training samples, which leads to a tighter and bumpier
    # decision boundary.
    svm = SVC(kernel='rbf', random_state=1, gamma=100.0, C=1.0)
    svm.fit(X_train_std, y_train)

    mlutils.plot_decision_regions(
        X_combined_std, y_combined, classifier=svm, test_idx=range(105, 150))
    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.legend(loc='upper left')
    plt.title('RBF Kernel SVM (large gamma = 100.0)')
    plt.show()

    # Decision tree learning
    # Decision tree classifiers are based on a model that breaks down the data
    # by making decisions based on asking a series of questions.  Based on the
    # features in the training data set, the decision tree model learns a series
    # of questions to infer the class labels of the samples.
    x = np.arange(0.0, 1.0, 0.01)

    ent = [mlutils.entropy(p) if p != 0 else None for p in x]
    sc_ent = [e * 0.5 if e else None for e in ent]
    err = [mlutils.error(i) for i in x]

    fig = plt.figure()
    ax = plt.subplot(111)
    for i, lab, ls, c, in zip([ent, sc_ent, mlutils.gini(x), err], [
            'Entropy', 'Entropy (scaled)', 'Gini Impurity',
            'Misclassification Error'
    ], ['-', '-', '--', '-.'], ['black', 'lightgray', 'red', 'green', 'cyan']):
        line = ax.plot(x, i, label=lab, linestyle=ls, lw=2, color=c)

    ax.legend(
        loc='upper center',
        bbox_to_anchor=(0.5, 1.15),
        ncol=5,
        fancybox=True,
        shadow=False)

    ax.axhline(y=0.5, linewidth=1, color='k', linestyle='--')
    ax.axhline(y=1.0, linewidth=1, color='k', linestyle='--')
    plt.ylim([0, 1.1])
    plt.xlabel('p(i=1)')
    plt.ylabel('Impurity Index')
    plt.title('Impurity Measures')
    plt.show()

    # Building a decision tree
    # Decision trees can build complex decision boundaries by dividing the
    # feature space into rectangles.  Caution, the deeper the decision tree,
    # the more complex the decision boundary becomes, which can easily result
    # in overfitting.

    # Using scikit-learn, train a decision tree with a maximum depth of 4,
    # using Gini Impurity as a criterion for impurity.  While feature scaling
    # may be desired for visualisation, it is not a requirement for decision
    # tree algorithms.
    tree = DecisionTreeClassifier(
        criterion='gini', max_depth=4, random_state=1)
    tree.fit(X_train, y_train)

    X_combined = np.vstack((X_train, X_test))
    y_combined = np.hstack((y_train, y_test))
    mlutils.plot_decision_regions(
        X_combined, y_combined, classifier=tree, test_idx=range(105, 150))

    plt.xlabel('petal length [cm]')
    plt.ylabel('petal width [cm]')
    plt.legend(loc='upper left')
    plt.title('Decision Tree Decision Boundary')
    plt.show()

    # Create an image of the decision tree in PNG format in local directory.
    dot_data = export_graphviz(
        tree,
        filled=True,
        rounded=True,
        class_names=['Setosa', 'Versicolor', 'Virginica'],
        feature_names=['petal length', 'petal width'],
        out_file=None)
    graph = graph_from_dot_data(dot_data)
    graph.write_png('tree.png')

    # Combining multiple decision trees via random forests
    forest = RandomForestClassifier(
        criterion='gini', n_estimators=25, random_state=1, n_jobs=2)
    forest.fit(X_train, y_train)

    mlutils.plot_decision_regions(
        X_combined, y_combined, classifier=forest, test_idx=range(105, 150))

    plt.xlabel('petal length [cm]')
    plt.ylabel('petal width [cm]')
    plt.legend(loc='upper left')
    plt.title('Combining weak to strong learners via random forests')
    plt.show()

    # KNN model using Euclidean distance metric
    knn = KNeighborsClassifier(n_neighbors=5, p=2, metric='minkowski')
    knn.fit(X_train_std, y_train)
    mlutils.plot_decision_regions(
        X_combined_std, y_combined, classifier=knn, test_idx=range(105, 150))
    plt.xlabel('petal length [standardised]')
    plt.ylabel('petal width [standardised]')
    plt.title('KNN Using Euclidean Distance Metric')
    plt.legend(loc='upper left')
    plt.show()


if __name__ == '__main__':
    main()
