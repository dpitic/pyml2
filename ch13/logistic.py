"""Choosing activation functions for multilayer networks."""
import matplotlib.pyplot as plt
import numpy as np


def net_input(X, w):
    return np.dot(X, w)


def logistic(z):
    return 1.0 / (1.0 + np.exp(-z))


def logistic_activation(X, w):
    z = net_input(X, w)
    return logistic(z)


def softmax(z):
    return np.exp(z) / np.sum(np.exp(z))


def tanh(z):
    e_p = np.exp(z)
    e_m = np.exp(-z)
    return (e_p - e_m) / (e_p + e_m)


def main():
    # Arbitrary logistic neuron feature values
    X = np.array([1, 1.4, 2.5])
    # Neuron weights
    w = np.array([0.4, 0.3, 0.5])
    print('Probability that the sample x belongs to the positive class:')
    print('P(y=1|x) = %.3f' % logistic_activation(X, w))

    # W : array with shape = (n_output_units, n_hidden_units + 1)
    # Note: the first column are the bias units
    W = np.array([[1.1, 1.2, 0.8, 0.4], [0.2, 0.4, 1.0, 0.2],
                  [0.6, 1.5, 1.2, 0.7]])

    # A : data array with shape = (n_hidden_units + 1, n_samples)
    # Note: the first column of this array must be 1
    A = np.array([[1, 0.1, 0.4, 0.6]])

    Z = np.dot(W, A[0])
    y_probas = logistic(Z)
    print('Net Input:\n', Z)
    print('Output Units:\n', y_probas)

    # Predict the class label using the maximum value
    y_class = np.argmax(Z, axis=0)
    print('Predicted class label: %d' % y_class)

    # Estimating the class probabilities in a multiclass classification via
    # the softmax function

    # The softmax function is a soft form of the argmax function.  Instead of
    # giving a single class index, it provides the probability of each class.
    y_probas = softmax(Z)
    print('Probabilities using softmax:\n', y_probas)
    print('Confirm sum of probabilities equal 1: sum = %d' % np.sum(y_probas))

    # Broadening the output spectrum using a hyperbolic tangent

    # The hyperbolic tangent (tanh) is another sigmoid function that is often
    # used in the hidden layers of artificial neural networks, which can be
    # interpreted as a rescaled version of the logistic function.  The
    # advantage of the hyperbolic tangent over the logistic function is that
    # it has a broader output spectrum and ranges in the open interval (-1,1),
    # which can improve the convergence of the back propagation algorithm.  In
    # contrast, the logistic function returns an output signal that ranges in
    # the open interval (0,1).  To obtain an intuitive comparison of the
    # logistic function and the hyperbolic tangent, plot the two sigmoid
    # functions.
    z = np.arange(-5, 5, 0.005)
    log_act = logistic(z)
    tan_act = tanh(z)

    plt.ylim([-1.5, 1.5])
    plt.xlabel('net input $z$')
    plt.ylabel('activation $\phi(z)$')
    plt.axhline(1, color='black', linestyle=':')
    plt.axhline(0.5, color='black', linestyle=':')
    plt.axhline(0, color='black', linestyle=':')
    plt.axhline(-0.5, color='black', linestyle=':')
    plt.axhline(-1, color='black', linestyle=':')

    plt.plot(z, tan_act, linewidth=3, linestyle='--', label='tanh')
    plt.plot(z, log_act, linewidth=3, label='logistic')
    plt.legend(loc='lower right')
    plt.title('Logistic & Hyperbolic Tangent')
    plt.show()


if __name__ == '__main__':
    main()
