"""Simple TensorFlow demonstration."""

import numpy as np
import tensorflow as tf


def main():
    # Demonstrate use of simple scalars from TensorFlow to compute the net
    # input z of a sample point x in a one-dimensional data set with weight w
    # and bias b, according to the equation z = w * x + b

    # Create a graph
    g = tf.Graph()
    with g.as_default():
        # Create placeholder for x to enable feeding values in an element
        # by element form and as a batch of input data at once
        x = tf.placeholder(dtype=tf.float32, shape=(None), name='x')
        w = tf.Variable(2.0, name='weight')
        b = tf.Variable(0.7, name='bias')

        z = w * x + b

        init = tf.global_variables_initializer()

    # Create a session and pass in graph g
    with tf.Session(graph=g) as sess:
        # Initialise w and b
        sess.run(init)
        # Evaluate z
        for t in [1.0, 0.6, -1.8]:
            # Feed in x values in element-by-element form
            print('x=%4.1f --> z=%4.1f' % (t, sess.run(z, feed_dict={x: t})))

    with tf.Session(graph=g) as sess:
        sess.run(init)
        # Feed x values as batch input of all data at once
        print(sess.run(z, feed_dict={x: [1.0, 2.0, 3.0]}))

    # Working with array structures
    # Demonstration of how to use array structure in TensorFlow.  Create a
    # simple rank-3 tensor or size batchsize * 2 * 3, reshape it, calculate
    # the column sums using TensorFlow's optimised expressions.  The batch size
    # is not known at the outset, so specify shape=None for the x placeholder.
    g = tf.Graph()
    with g.as_default():
        x = tf.placeholder(
            dtype=tf.float32, shape=(None, 2, 3), name='input_x')

        x2 = tf.reshape(x, shape=(-1, 6), name='x2')

        # Calculate the sum of each column
        xsum = tf.reduce_sum(x2, axis=0, name='col_sum')

        # Calculate the mean of each column
        xmean = tf.reduce_mean(x2, axis=0, name='col_mean')

    with tf.Session(graph=g) as sess:
        x_array = np.arange(18).reshape(3, 2, 3)
        print('input shape: ', x_array.shape)
        print('Reshaped:\n', sess.run(x2, feed_dict={x: x_array}))
        print('Column Sums:\n', sess.run(xsum, feed_dict={x: x_array}))
        print('Column Means:\n', sess.run(xmean, feed_dict={x: x_array}))


if __name__ == '__main__':
    main()
