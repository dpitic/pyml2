"""Linear regression classifier implemented using TensorFlow."""
import tensorflow as tf


class TfLinreg():
    """Linear regression classifier implemented in TensorFlow."""

    def __init__(self, x_dim, learning_rate=0.01, random_seed=None):
        self.x_dim = x_dim
        self.learning_rate = learning_rate
        self.g = tf.Graph()
        # Build the model
        with self.g.as_default():
            # Set graph-level random seed
            tf.set_random_seed(random_seed)

            self.build()
            # Create initialiser
            self.init_op = tf.global_variables_initializer()

    def build(self):
        # Placeholders for inputs
        self.X = tf.placeholder(
            dtype=tf.float32, shape=(None, self.x_dim), name='x_input')
        self.y = tf.placeholder(dtype=tf.float32, shape=(None), name='y_input')
        print(self.X)
        print(self.y)
        # Define weight matrix and bias vector
        w = tf.Variable(tf.zeros(shape=(1)), name='weight')
        b = tf.Variable(tf.zeros(shape=(1)), name='bias')
        print(w)
        print(b)

        # Define linear regression model
        self.z_net = tf.squeeze(w * self.X + b, name='z_net')
        print(self.z_net)

        sqr_errors = tf.square(self.y - self.z_net, name='sqr_errors')
        print(sqr_errors)
        # Define cost function to be Mean of Squared Error (MSE)
        self.mean_cost = tf.reduce_mean(sqr_errors, name='mean_cost')

        # Learn the weights using the gradient descent optimiser
        optimiser = tf.train.GradientDescentOptimizer(
            learning_rate=self.learning_rate, name='GradientDescent')
        self.optimiser = optimiser.minimize(self.mean_cost)
