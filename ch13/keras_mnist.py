"""Developing a multilayer neural network with Keras.

Linear regression classification model for MNIST using Keras.  Keras is one of
the most popular and widely used libraries that is built on top of Theano and
TensorFlow.  Similarly to TensorFlow, Keras can utilise GPUs to accelerate
neural network training.  One of its prominent features is that it has a very
intuitive and user-friendly API which makes it possible to implement neural
networks in only a few lines of code.
"""

import numpy as np
import tensorflow as tf
import tensorflow.contrib.keras as keras

from mlutils import load_mnist


def main():
    # Load the MNIST data set
    MNIST_PATH = './mnist/'
    X_train, y_train = load_mnist('%s' % MNIST_PATH, kind='train')
    print('MNIST training data set:')
    print('Rows: %d,  Columns: %d' % (X_train.shape[0], X_train.shape[1]))
    X_test, y_test = load_mnist(MNIST_PATH, kind='t10k')
    print('MNIST test data set:')
    print('Rows: %d,  Columns: %d' % (X_test.shape[0], X_test.shape[1]))
    # Mean centring and normalisation
    mean_vals = np.mean(X_train, axis=0)
    std_val = np.std(X_train)

    X_train_centred = (X_train - mean_vals) / std_val
    X_test_centred = (X_test - mean_vals) / std_val

    del X_train, X_test

    print(f'Centred feature matrix: {X_train_centred.shape}, '
          f'Centred training labels: {y_train.shape}')
    print(f'Centred test feature matrix: {X_test_centred.shape}, '
          f'Centred test labels: {y_test.shape}')

    # Set the random seed for NumPy and TensorFlow to get consistent results
    np.random.seed(123)
    tf.set_random_seed(123)

    # Convert the class labels (integers 0-9) into the one-hot format. Keras
    # provides a convenient tool for this.
    y_train_onehot = keras.utils.to_categorical(y_train)

    print('First 3 labels: ', y_train[:3])
    print('\nFirst 3 labels (one-hot):\n', y_train_onehot[:3])

    # The neural network will have three layers, where the first two layers
    # each have 50 hidden units with the tanh() activation function and the
    # last layer has 10 layers for the 10 class labels and uses softmax() to
    # give the probability of each class.  Initialise a new model using the
    # Sequential class to implement a feed forward neural network.
    model = keras.models.Sequential()

    # Add as many layers to the model as needed.  First layer is the input
    # layer, so the input_dim attribute must match the number of features
    # (columns) in the training data set (784 features or pixels in the
    # neural network implementation).  Also, ensure the number of output units
    # 'units' and input units 'input_dim' of two consecutive layers match.
    # The 'glorot_uniform' kernel initialiser provides a more robust way of
    # initialisation for deep neural networks.
    model.add(
        keras.layers.Dense(units=50,
                           input_dim=X_train_centred.shape[1],
                           kernel_initializer='glorot_uniform',
                           bias_initializer='zeros',
                           activation='tanh'))

    # Add 2 hidden layers with 50 hidden units plus 1 bias unit each.  The
    # number of units in the output layer should be equal to the number of
    # unique class labels - the number of columns in the one-hot-encoded class
    # label array.
    model.add(
        keras.layers.Dense(units=50,
                           input_dim=50,
                           kernel_initializer='glorot_uniform',
                           bias_initializer='zeros',
                           activation='tanh'))

    model.add(
        keras.layers.Dense(units=y_train_onehot.shape[1],
                           input_dim=50,
                           kernel_initializer='glorot_uniform',
                           bias_initializer='zeros',
                           activation='softmax'))

    # Define an optimiser: stochastic gradient descent
    sgd_optimiser = keras.optimizers.SGD(lr=0.001, decay=1e-7, momentum=0.9)

    # Set the cost (or loss) function to 'categorical_crossentropy' and
    # compile the model.
    model.compile(optimizer=sgd_optimiser, loss='categorical_crossentropy')

    # Train the model using mini-batch stochastic gradient with a batch size
    # of 64 training samples per batch.  Train over 50 epochs and output the
    # optimisation of the cost function during training 'verbose=1'.
    history = model.fit(X_train_centred,
                        y_train_onehot,
                        batch_size=64,
                        epochs=50,
                        verbose=1,
                        validation_split=0.1)

    # Predict the class labels to return the class labels directly as integers
    y_train_pred = model.predict_classes(X_train_centred, verbose=0)
    print('First 3 predictions: ', y_train_pred[:3])

    # Print the model accuracy on training and test data sets
    correct_preds = np.sum(y_train == y_train_pred, axis=0)
    train_acc = correct_preds / y_train.shape[0]

    print('Training accuracy: %.2f%%' % (train_acc * 100))

    y_test_pred = model.predict_classes(X_test_centred, verbose=0)

    correct_preds = np.sum(y_test == y_test_pred, axis=0)
    test_acc = correct_preds / y_test.shape[0]
    print('Test accuracy: %.2f%%' % (test_acc * 100))


if __name__ == '__main__':
    main()
