"""Training neural networks efficiently with high level TensorFlow APIs.

This module demonstrates two high-level TensorFlow APIs, the Layers API
(tensorflow.layers or tf.layers) and the Keras API (tensorflow.contrib.keras)
using the MNIST handwriting data set.  The data set is in the mnist
subdirectory.  Although TensorFlow provides the same data set in the
tensorflow.examples.tutorials.mnist module, this module uses an externally
downloaded MNIST data set.
"""
import numpy as np
import tensorflow as tf

from mlutils import load_mnist


def create_batch_generator(X, y, batch_size=128, shuffle=False):
    """Generate batches of data.

    :param X: Feature matrix.
    :param y: Class label vector.
    :param batch_size: Number of elements in batch, default=128.
    :param shuffle: Whether to randomly shuffle the data or not, default=False.
    :return: A copy of the feature matrix and class label vector with number of
    elements equal to batch_size.
    """
    X_copy = np.array(X)
    y_copy = np.array(y)

    if shuffle:
        data = np.column_stack((X_copy, y_copy))
        np.random.shuffle(data)
        X_copy = data[:, :-1]
        y_copy = data[:, -1].astype(int)

    for i in range(0, X.shape[0], batch_size):
        yield (X_copy[i:i + batch_size, :], y_copy[i:i + batch_size])


def main():
    # Load the MNIST data set
    MNIST_PATH = './mnist/'
    X_train, y_train = load_mnist('%s' % MNIST_PATH, kind='train')
    print('MNIST training data set:')
    print('Rows: %d,  Columns: %d' % (X_train.shape[0], X_train.shape[1]))
    X_test, y_test = load_mnist(MNIST_PATH, kind='t10k')
    print('MNIST test data set:')
    print('Rows: %d,  Columns: %d' % (X_test.shape[0], X_test.shape[1]))
    # Mean centring and normalisation
    mean_vals = np.mean(X_train, axis=0)
    std_val = np.std(X_train)

    X_train_centred = (X_train - mean_vals) / std_val
    X_test_centred = (X_test - mean_vals) / std_val

    del X_train, X_test

    print(f'Centred feature matrix: {X_train_centred.shape}, '
          f'Centred training labels: {y_train.shape}')
    print(f'Centred test feature matrix: {X_test_centred.shape}, '
          f'Centred test labels: {y_test.shape}')

    # Build model of multilayer perceptron with 3 fully connected layers but
    # replace logistic units in the hidden layer with hyperbolic tangent
    # activation functions (tanh), and replace the logistic function in the
    # output layer with softmax(), and add an additional hidden layer.
    n_features = X_train_centred.shape[1]
    n_classes = 10
    random_seed = 123
    np.random.seed(random_seed)

    # TensorFlow graph
    g = tf.Graph()
    with g.as_default():
        tf.set_random_seed(random_seed)
        # Placeholders for features and labels
        tf_x = tf.placeholder(dtype=tf.float32,
                              shape=(None, n_features),
                              name='tf_x')
        tf_y = tf.placeholder(dtype=tf.int32, shape=None, name='tf_y')
        y_onehot = tf.one_hot(indices=tf_y, depth=n_classes)

        h1 = tf.layers.dense(inputs=tf_x,
                             units=50,
                             activation=tf.tanh,
                             name='layer1')
        h2 = tf.layers.dense(inputs=h1,
                             units=50,
                             activation=tf.tanh,
                             name='layer2')

        logits = tf.layers.dense(inputs=h2,
                                 units=10,
                                 activation=None,
                                 name='layer3')

        predictions = {
            'classes': tf.argmax(logits, axis=1, name='predicted_classes'),
            'probabilities': tf.nn.softmax(logits, name='softmax_tensor')
        }

        # Define the cost function and optimiser
        with g.as_default():
            cost = tf.losses.softmax_cross_entropy(onehot_labels=y_onehot,
                                                   logits=logits)
            optimiser = tf.train.GradientDescentOptimizer(learning_rate=0.001)
            train_op = optimiser.minimize(loss=cost)
            init_op = tf.global_variables_initializer()

    # Create a TensorFlow session to launch the graph
    sess = tf.Session(graph=g)
    # Run the variable initialisation operator
    sess.run(init_op)

    # Display the average training less after each epoch
    training_costs = []
    for epoch in range(50):
        batch_generator = create_batch_generator(X_train_centred,
                                                 y_train,
                                                 batch_size=64)
        for batch_X, batch_y in batch_generator:
            # Prepare a dict to feed data to network
            feed = {tf_x: batch_X, tf_y: batch_y}
            _, batch_cost = sess.run([train_op, cost], feed_dict=feed)
            training_costs.append(batch_cost)

        print(' -- Epoch %2d  '
              'Avg. Training Loss: %.4f' %
              (epoch + 1, np.mean(training_costs)))

    # Use the trained model to perform predictions on the test data set
    feed = {tf_x: X_test_centred}
    y_pred = sess.run(predictions['classes'], feed_dict=feed)
    print('Test Accuracy: %.2f%%' %
          (100 * np.sum(y_pred == y_test) / y_test.shape[0]))


if __name__ == '__main__':
    main()
