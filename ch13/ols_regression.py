"""Simple example of implementing Ordinary Least Squares (OLS) regression."""
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import tf_linreg


def train_linreg(sess, model, X_train, y_train, num_epochs=10):
    """Linear regression training function.

    This function is used to train the model in order to learn the weights of
    the linear regression model.
    :param sess: TensorFlow session.
    :param model: Linear regression model object.
    :param X_train: Training feature matrix.
    :param y_train: Training class label vector.
    :param num_epochs: Number of iterations for training.
    :return: List of training costs for each training epoch.
    """
    # Initialise all variables: W & b
    sess.run(model.init_op)

    training_costs = []
    for i in range(num_epochs):
        _, cost = sess.run([model.optimiser, model.mean_cost],
                           feed_dict={
                               model.X: X_train,
                               model.y: y_train
                           })
        training_costs.append(cost)

    return training_costs


def predict_linreg(sess, model, X_test):
    """Make linear regression predictions based on the input features.

    :param sess: TensorFlow session.
    :param model: Linear regression model object.
    :param X_test: Test data set.
    :return: Linear prediction.
    """
    y_pred = sess.run(model.z_net, feed_dict={model.X: X_test})
    return y_pred


def main():
    # Small 1-dimensional data set with 10 training samples
    X_train = np.arange(10).reshape((10, 1))
    y_train = np.array([1.0, 1.3, 3.1, 2.0, 5.0, 6.3, 6.6, 7.4, 8.0, 9.0])
    # Given this data set, we want to train a linear regression model to
    # predict the output y from the input x using the custom TfLinreg class.
    lrmodel = tf_linreg.TfLinreg(x_dim=X_train.shape[1], learning_rate=0.01)
    # Create a new TensorFlow session to launch the lrmodel.g graph and train
    # the model
    sess = tf.Session(graph=lrmodel.g)
    training_costs = train_linreg(sess, lrmodel, X_train, y_train)

    # Visualise the training costs for each epoch to determine convergence
    plt.plot(range(1, len(training_costs) + 1), training_costs)
    plt.xlabel('Epoch')
    plt.ylabel('Training Cost')
    plt.title('TensorFlow Linear Regression Training Cost')
    plt.show()

    # Plot the linear regression fit on the training data
    plt.scatter(X_train, y_train, marker='s', s=50, label='Training Data')
    plt.plot(
        range(X_train.shape[0]),
        predict_linreg(sess, lrmodel, X_train),
        color='gray',
        marker='o',
        markersize=6,
        linewidth=3,
        label='LinReg Model')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend()
    plt.title('Linear Regression on Training Data')
    plt.show()


if __name__ == '__main__':
    main()
