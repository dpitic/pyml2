"""
Classifying handwritten digits from the MNIST data set.
"""
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np

from mlutils import load_mnist
from neuralnet import NeuralNetMLP


def main():
    # MNIST compressed array NumPy file
    filename = 'mnist_scaled.npz'
    if not os.path.isfile(filename):
        print('Compressed NumPy files does not exist, loading raw data ...')
        # Load MNIST training data set into NumPy arrays (60,000 samples)
        X_train, y_train = load_mnist('', kind='train')
        # Load MNIST test data set into NumPy arrays (10,000 samples)
        X_test, y_test = load_mnist('', kind='t10k')

        # Save compressed MNIST NumPy arrays
        np.savez_compressed(filename,
                            X_train=X_train,
                            y_train=y_train,
                            X_test=X_test,
                            y_test=y_test)
        print('Arrays saved to disk file', filename)
    else:
        print(
            f'Found compressed NumPy file {filename}, loading into memory ...')
        # Load MNIST NumPy arrays from compressed file
        mnist = np.load(filename)
        print('MNIST NumPy array files')
        print(mnist.files)
        # Load arrays into variables
        X_train, y_train, X_test, y_test = [mnist[f] for f in mnist.files]

    # Explain the NumPy array data
    print('MNIST training data set images')
    print(f'Rows: {X_train.shape[0]}, columns: {X_train.shape[1]}')

    print('MNIST test data set images')
    print(f'Rows: {X_test.shape[0]}, columns: {X_test.shape[1]}')

    # Visualise examples of the digits 0-9 after reshaping the 784 pixel
    # vectors from the feature matrix into the original 28 x 28 image that can
    # be plotted using Matplotlib's imshow() function.
    fig, ax = plt.subplots(nrows=2, ncols=5, sharex=True, sharey=True)
    ax = ax.flatten()
    for i in range(10):
        img = X_train[y_train == i][0].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys')

    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.show()
    # Plot multiple examples of the same digit to see how different the
    # handwriting is.  Show the first 25 variants of the digit 7.
    fig, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True)
    ax = ax.flatten()
    for i in range(25):
        img = X_train[y_train == 7][i].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys')

    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.show()

    # Serialised MLP object
    filename = 'NeuralNetMLP.pkl'

    if not os.path.isfile(filename):
        # Initialise a new 784-100-10 MLP (neural network with 784 input units
        # (n_features), 100 hidden units (n_hidden), and 10 output units
        # (n_output).
        nn = NeuralNetMLP(n_hidden=100,
                          l2=0.01,
                          epochs=200,
                          eta=0.0005,
                          minibatch_size=100,
                          shuffle=True,
                          seed=1)
        # Train the MLP using 55,000 samples from the shuffled MNIST training
        # data set and use the remaining 5,000 samples for validation during
        # training.
        print('Training MLP model ...')
        nn.fit(X_train=X_train[:55000],
               y_train=y_train[:55000],
               X_valid=X_train[55000:],
               y_valid=y_train[55000:])

        # Serialise the estimator
        with open(filename, 'wb') as f:
            pickle.dump(nn, f, pickle.HIGHEST_PROTOCOL)

        print('Saved MLP object to file:', filename)
    else:
        print(f'Found serialised MLP object, loading from file {filename} ...')
        # Load pickled MLP object
        with open(filename, 'rb') as f:
            nn = pickle.load(f)

    # Visualise the cost
    plt.plot(range(nn.epochs), nn.eval_['cost'])
    plt.ylabel('Cost')
    plt.xlabel('Epochs')
    plt.title('NeuralNetMLP Cost')
    plt.show()

    # Visualise the training and validation accuracy.  The plot shows that the
    # gap between training and validation accuracy increases with more epochs
    # used for training.  At approximately the 50th epoch, the training and
    # validation accuracy values are equal, and after that, the network starts
    # overfitting the training data.  One way to decrease the effect of
    # overfitting, is to increase the regularisation strength, e.g. setting
    # l2=0.1; another useful technique is "dropout".
    plt.plot(range(nn.epochs), nn.eval_['train_acc'], label='training')
    plt.plot(range(nn.epochs),
             nn.eval_['valid_acc'],
             label='validation',
             linestyle='--')
    plt.ylabel('Accuracy')
    plt.xlabel('Epochs')
    plt.legend()
    plt.title('MLP Training and Validation Accuracy')
    plt.show()

    # Evaluate the generalisation performance of the model by calculating the
    # prediction accuracy on the test data set
    y_test_pred = nn.predict(X_test)
    acc = (np.sum(y_test == y_test_pred).astype(np.float) / X_test.shape[0])
    print('Test accuracy: %.2f%%' % (acc * 100))

    # Visualise some of the images the MLP struggled with
    miscl_img = X_test[y_test != y_test_pred][:25]
    correct_lab = y_test[y_test != y_test_pred][:25]
    miscl_lab = y_test_pred[y_test != y_test_pred][:25]

    fig, ax = plt.subplots(nrows=5, ncols=5, sharex=True, sharey=True)
    ax = ax.flatten()
    for i in range(25):
        img = miscl_img[i].reshape(28, 28)
        ax[i].imshow(img, cmap='Greys', interpolation='nearest')
        ax[i].set_title('%d) t: %d p: %d' %
                        (i + 1, correct_lab[i], miscl_lab[i]))

    ax[0].set_xticks([])
    ax[0].set_yticks([])
    plt.show()


if __name__ == '__main__':
    main()
