"""Multilayer perceptron."""

import sys

import numpy as np


class NeuralNetMLP:
    """Feed forward neural network / Multilayer perceptron classifier.

    This class implements a MLP with one input, one hidden, and out one output
    layer.

    :param int n_hidden (default: 30): Number of hidden units.
    :param float l2 (default: 0.): Lambda value for L2-regularisation.  No
                                   regularisation if l2=0. (default)
    :param int epochs (default: 100): Number of passes over the training set.
    :param float eta (default: 0.001): Learning rate.
    :param bool shuffle (default: True): Shuffles training data every epoch if
                                         True to prevent circles.
    :param int minibatch_size (default: 1): Number of training samples per
                                            minibatch.
    :param int seed (default: None): Random seed for initialising weights and
                                     shuffling.
    :param dict eval_: Dictionary collecting the cost, training accuracy, and
                       validation accuracy for each epoch during training.
    """

    def __init__(self,
                 n_hidden=30,
                 l2=0.0,
                 epochs=100,
                 eta=0.001,
                 shuffle=True,
                 minibatch_size=1,
                 seed=None):
        self.random = np.random.RandomState(seed)
        self.n_hidden = n_hidden
        self.l2 = l2
        self.epochs = epochs
        self.eta = eta
        self.shuffle = shuffle
        self.minibatch_size = minibatch_size

    def _onehot(self, y, n_classes):
        """Encode labels into one-hot representation.

        :param array y: Target values; shape = [n_samples]
        :return array onehot: shape = [n_samples, n_labels]
        """
        onehot = np.zeros((n_classes, y.shape[0]))
        for idx, val in enumerate(y.astype(int)):
            onehot[val, idx] = 1.0
        return onehot.T

    def _sigmoid(self, z):
        """Compute logistic function (sigmoid).

        :param z: net input.
        :return: logistic function (sigmoid).
        """
        return 1.0 / (1.0 + np.exp(-np.clip(z, -250, 250)))

    def _forward(self, X):
        """Compute forward propagation step.

        :param array X: Input layer with original features;
                        shape = [n_samples, n_features]
        :return array z_h: net input of hidden layer
                           shape = [n_samples, n_hidden]
        :return array a_h: activation of hidden layer
        :return array z_out: net input of output layer
                             shape = [n_samples, n_classlabels]
        :return array a_out: activation of output layer
        """
        # Step 1: net input of hidden layer
        # [n_samples, n_features] dot [n_features, n_hidden]
        # -> [n_samples, n_hidden]
        z_h = np.dot(X, self.w_h) + self.b_h

        # Step 2: activation of hidden layer
        a_h = self._sigmoid(z_h)

        # Step 3: net input of output layer
        # [n_samples, n_hidden] dot [n_hidden, n_classlabels]
        # -> [n_samples, n_classlabels]
        z_out = np.dot(a_h, self.w_out) + self.b_out

        # Step 4: activation output layer
        a_out = self._sigmoid(z_out)

        return z_h, a_h, z_out, a_out

    def _compute_cost(self, y_enc, output):
        """Compute the cost function.

        :param array y_enc: one-hot encoded class labels,
                            shape = [n_samples, n_labels].
        :param array output: Activation of the output layer (forward
                             propagation), shape = [n_samples, n_output_units].
        :return float cost: Reularised cost.
        """
        L2_term = (self.l2 * (np.sum(self.w_h**2.0) + np.sum(self.w_out**2.0)))
        term1 = -y_enc * (np.log(output))
        term2 = (1.0 - y_enc) * np.log(1.0 - output)
        cost = np.sum(term1 - term2) + L2_term

        # If this cost function is applied to other data sets where activation
        # values may become more extreme (closer to zero or 1), it is possible
        # to encounter a "ZeroDivisionError" due to numerical instabilities in
        # Python and NumPy for the current implementation, i.e. the code tries
        # to evaluate log(0), which is undefined.  To address this issue, add
        # a small constant to the activation values that are passed to the log
        # function, e.g.:
        #
        # term 1 = -y_enc * (np.log(output + 1e-5))
        # term 2 = (1.0 - y_enc) * np.log(1.0 - output + 1e-5)
        return cost

    def predict(self, X):
        """Predict class labels.

        :param array X: Input layer with original features,
                        shape = [n_samples, n_features].
        :return array y_pred: Predicted class labels, shape = [n_samples].
        """
        z_h, a_h, z_out, a_out = self._forward(X)
        y_pred = np.argmax(z_out, axis=1)
        return y_pred

    def fit(self, X_train, y_train, X_valid, y_valid):
        """Learn weights from training data.

        :param array X_train: Input layer with original features,
                              shape = [n_samples, n_features].
        :param array y_train: Target class labels, shape = [n_samples].
        :param array X_valid: Sample features for validation during training,
                              shape = [n_samples, n_features].
        :param array y_valid: Sample labels for validation during training,
                              shape = [n_samples].
        :return: self.
        """
        n_output = np.unique(y_train).shape[0]  # number of class labels
        n_features = X_train.shape[1]

        # Weight initialisation

        # Weights for input -> hidden
        self.b_h = np.zeros(self.n_hidden)
        self.w_h = self.random.normal(loc=0.0,
                                      scale=0.1,
                                      size=(n_features, self.n_hidden))

        # Weights for hidden -> output
        self.b_out = np.zeros(n_output)
        self.w_out = self.random.normal(loc=0.0,
                                        scale=0.1,
                                        size=(self.n_hidden, n_output))

        epoch_strlen = len(str(self.epochs))  # for progress formatting
        self.eval_ = {'cost': [], 'train_acc': [], 'valid_acc': []}

        y_train_enc = self._onehot(y_train, n_output)

        # Iterate over training epochs
        for i in range(self.epochs):
            # Iterate over minibatches
            indices = np.arange(X_train.shape[0])

            if self.shuffle:
                self.random.shuffle(indices)

            for start_idx in range(0,
                                   indices.shape[0] - self.minibatch_size + 1,
                                   self.minibatch_size):
                batch_idx = indices[start_idx:start_idx + self.minibatch_size]

                # Forward propagation to obtain the activation of the output
                # layer a_out
                z_h, a_h, z_out, a_out = self._forward(X_train[batch_idx])

                # Back propagation

                # Error vector of the output layer [n_samples, n_classlabels]
                sigma_out = a_out - y_train_enc[batch_idx]

                # [n_samples, n_hidden]
                sigmoid_derivative_h = a_h * (1.0 - a_h)

                # Error term of the hidden layer sigma_h
                # [n_samples, n_classlabels] dot [n_classlabels, n_hidden]
                # -> [n_samples, n_hidden]
                sigma_h = (np.dot(sigma_out, self.w_out.T) *
                           sigmoid_derivative_h)

                # [n_features, n_samples] dot [n_samples, n_hidden]
                # -> [n_features, n_hidden]
                grad_w_h = np.dot(X_train[batch_idx].T, sigma_h)
                grad_b_h = np.sum(sigma_h, axis=0)

                # [n_hidden, n_samples] dot [n_samples, n_classlabels]
                # -> [n_hidden, n_classlabels]
                grad_w_out = np.dot(a_h.T, sigma_out)
                grad_b_out = np.sum(sigma_out, axis=0)

                # Regularisation and weight updates
                delta_w_h = (grad_w_h + self.l2 * self.w_h)
                delta_b_h = grad_b_h  # bias is not regularised
                self.w_h -= self.eta * delta_w_h
                self.b_h -= self.eta * delta_b_h

                delta_w_out = (grad_w_out + self.l2 * self.w_out)
                delta_b_out = grad_b_out  # bias is not regularised
                self.w_out -= self.eta * delta_w_out
                self.b_out -= self.eta * delta_b_out

            # Evaluation

            # Evaluation after each epoch during training
            z_h, a_h, z_out, a_out = self._forward(X_train)

            cost = self._compute_cost(y_enc=y_train_enc, output=a_out)

            y_train_pred = self.predict(X_train)
            y_valid_pred = self.predict(X_valid)

            train_acc = ((np.sum(y_train == y_train_pred)).astype(np.float) /
                         X_train.shape[0])
            valid_acc = ((np.sum(y_valid == y_valid_pred)).astype(np.float) /
                         X_valid.shape[0])

            sys.stderr.write('\r%0*d/%d | Cost: %.2f '
                             '| Train/Valid Acc.: %.2f%%/%.2f%% ' %
                             (epoch_strlen, i + 1, self.epochs, cost,
                              train_acc * 100, valid_acc * 100))
            sys.stderr.flush()

            self.eval_['cost'].append(cost)
            self.eval_['train_acc'].append(train_acc)
            self.eval_['valid_acc'].append(valid_acc)

        return self
