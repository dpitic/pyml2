"""TensorFlow features and concepts."""
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf


def build_classifier(data, labels, n_classes=2):
    """Helper function that builds and returns a classifier.

    This helper function is used in the "Reusing variables" example where a
    hypothetical complex neural network model that has a classifier whose
    input data comes from more than one source.  Assume data (X[A], y[A])
    comes from source A and data (X[B], y[B]) comes from source B.  The graph
    is designed in such a way that it will use the data from only one source
    as input tensor to build the network.  Then the data from the other source
    can be fed to the same classifier.

    Assume that data from source A is fed through a placeholder, and source B
    is the output of a generator network, which is built by calling the
    build_generator() function within the generator scope.  Then a classifier
    is added by calling build_classifier within the classifier scope.
    """
    data_shape = data.get_shape().as_list()
    weights = tf.compat.v1.get_variable(name='weights',
                                        shape=(data_shape[1], n_classes),
                                        dtype=tf.float32)
    bias = tf.compat.v1.get_variable(name='bias',
                                     initializer=tf.zeros(shape=n_classes))
    print(weights)
    print(bias)
    logits = tf.add(tf.matmul(data, weights), bias, name='logits')
    print(logits)
    return logits, tf.nn.softmax(logits)


def build_generator(data, n_hidden):
    """Helper function that builds a generator network.

    The generator network is build within the generator scope and a classifier
    is added by calling build_classifier() within the classifier scope.
    """
    data_shape = data.get_shape().as_list()
    w1 = tf.compat.v1.Variable(tf.random.normal(shape=(data_shape[1],
                                                       n_hidden)),
                               name='w1')
    b1 = tf.compat.v1.Variable(tf.zeros(shape=n_hidden), name='b1')
    hidden = tf.add(tf.matmul(data, w1), b1, name='hidden_pre-activation')
    hidden = tf.nn.relu(hidden, 'hidden_activation')

    w2 = tf.compat.v1.Variable(tf.random.normal(shape=(n_hidden,
                                                       data_shape[1])),
                               name='w2')
    b2 = tf.compat.v1.Variable(tf.zeros(shape=data_shape[1]), name='b2')
    output = tf.add(tf.matmul(hidden, w2), b2, name='output')
    return output, tf.nn.sigmoid(output)


def main():
    # How to get the rank and shape of a tensor
    g = tf.Graph()
    ## Define the computation graph
    with g.as_default():
        ## Define tensors t1, t2, t3
        t1 = tf.constant(np.pi)
        t2 = tf.constant([1, 2, 3, 4])
        t3 = tf.constant([[1, 2], [3, 4]])

        ## Get the ranks of the tensors
        r1 = tf.rank(t1)
        r2 = tf.rank(t2)
        r3 = tf.rank(t3)

        ## Get the shapes of the tensors
        s1 = t1.get_shape()
        s2 = t2.get_shape()
        s3 = t3.get_shape()
        print('Shapes:', s1, s2, s3)

    with tf.compat.v1.Session(graph=g) as sess:
        print('Ranks:', r1.eval(), r2.eval(), r3.eval())

    # Understanding TensorFlow's computational graph
    g = tf.Graph()

    ## Add nodes to the graph
    with g.as_default():
        a = tf.constant(1, name='a')
        b = tf.constant(2, name='b')
        c = tf.constant(3, name='c')

        # Expression to evaluate
        z = 2 * (a - b) + c

    ## Launch the graph
    with tf.compat.v1.Session(graph=g) as sess:
        print('2*(a-b)+c => ', sess.run(z))

    # Defining placeholders
    g = tf.Graph()
    with g.as_default():
        tf_a = tf.compat.v1.placeholder(tf.int32, shape=[], name='tf_a')
        tf_b = tf.compat.v1.placeholder(tf.int32, shape=[], name='tf_b')
        tf_c = tf.compat.v1.placeholder(tf.int32, shape=[], name='tf_c')

        r1 = tf_a - tf_b
        r2 = 2 * r1
        z = r2 + tf_c

    ## Launch the graph
    with tf.compat.v1.Session(graph=g) as sess:
        feed = {tf_a: 1, tf_b: 2, tf_c: 3}
        print('z:', sess.run(z, feed_dict=feed))

    # Defining placeholders for data with arrays with varying batch sizes
    # Placeholders can specify None for the dimension that is varying in size
    g = tf.Graph()
    with g.as_default():
        tf_x = tf.compat.v1.placeholder(tf.float32,
                                        shape=[None, 2],
                                        name='tf_x')
        x_mean = tf.reduce_mean(tf_x, axis=0, name='mean')

    np.random.seed(123)
    np.set_printoptions(precision=2)

    with tf.compat.v1.Session(graph=g) as sess:
        x1 = np.random.uniform(low=0, high=1, size=(5, 2))
        print('Feeding data with shape', x1.shape)
        print('Result:', sess.run(x_mean, feed_dict={tf_x: x1}))
        x2 = np.random.uniform(low=0, high=1, size=(10, 2))
        print('Feeding data with shape', x2.shape)
        print('Result:', sess.run(x_mean, feed_dict={tf_x: x2}))

    print(tf_x)

    # Variables in TensorFlow
    # This example demonstrates creating a variable object where the initial
    # values are created from a NumPy array.  The dtype data type of this
    # tensor is tf.int64 which is automatically inferred from its NumPy array
    # input.
    g1 = tf.Graph()
    with g1.as_default():
        w = tf.Variable(np.array([[1, 2, 3, 4], [5, 6, 7, 8]]), name='w')
        print('TensorFlow variable w:', w)

    # Initialising variables
    # Tensors defined as variables are not allocated in memory and contain no
    # values until they are initialised.
    with tf.compat.v1.Session(graph=g1) as sess:
        sess.run(tf.compat.v1.global_variables_initializer())
        print('After variable initialisation w:\n', sess.run(w))

    ## Add the init_op to the graph
    with g1.as_default():
        init_op = tf.compat.v1.global_variables_initializer()

    ## Initialise w with init_op and evaluate it
    with tf.compat.v1.Session(graph=g1) as sess:
        sess.run(init_op)
        print('Initialised with init_op w:\n', sess.run(w))

    # Define variable w1, then define operator init_op, followed by variable
    # w2
    g2 = tf.Graph()
    with g2.as_default():
        w1 = tf.Variable(1, name='w1')
        init_op = tf.compat.v1.global_variables_initializer()
        # init_op defined prior to adding w2 to the graph; will not initialise
        # w2
        w2 = tf.Variable(2, name='w2')
    # Evaluate w1
    with tf.compat.v1.Session(graph=g2) as sess:
        sess.run(init_op)
        print('w1:', sess.run(w1))
    # Error if variable is not initialised
    with tf.compat.v1.Session(graph=g2) as sess:
        try:
            # This will not initialise w2 because w2 was added to the graph
            # after init_op was defined
            sess.run(init_op)
            print('w2:', sess.run(w2))
        except tf.errors.FailedPreconditionError as e:
            print(e)

    # Variable scope
    # Define two scopes named 'net_A' and 'net_B' and each layer can be
    # defined within one of those scopes.
    g = tf.Graph()
    with g.as_default():
        with tf.compat.v1.variable_scope('net_A'):
            with tf.compat.v1.variable_scope('layer-1'):
                w1 = tf.Variable(tf.random.normal(shape=(10, 4)),
                                 name='weights')
            with tf.compat.v1.variable_scope('layer-2'):
                w2 = tf.Variable(tf.random.normal(shape=(20, 10)),
                                 name='weights')
        with tf.compat.v1.variable_scope('net_B'):
            with tf.compat.v1.variable_scope('layer-1'):
                w3 = tf.Variable(tf.random.normal(shape=(10, 4)),
                                 name='weights')

        print(w1)
        print(w2)
        print(w3)

    # Reusing variables
    ## Defining the graph
    batch_size = 64
    g = tf.Graph()

    with g.as_default():
        tf_X = tf.compat.v1.placeholder(shape=(batch_size, 100),
                                        dtype=tf.float32,
                                        name='tf_X')
        ## Build the generator
        with tf.compat.v1.variable_scope('generator'):
            gen_out1 = build_generator(data=tf_X, n_hidden=50)
        ## Build the classifier
        with tf.compat.v1.variable_scope('classifier') as scope:
            ## Classifier for the original data
            cls_out1 = build_classifier(data=tf_X,
                                        labels=tf.ones(shape=batch_size))
            ## Reuse the classifier for generated data
            scope.reuse_variables()
            cls_out2 = build_classifier(data=gen_out1[1],
                                        labels=tf.zeros(shape=batch_size))

            init_op = tf.compat.v1.global_variables_initializer()

    ## Alternative implementation reusing variables
    g = tf.Graph()

    with g.as_default():
        tf_X = tf.compat.v1.placeholder(shape=(batch_size, 100),
                                        dtype=tf.float32,
                                        name='tf_X')
        ## Build the generator
        with tf.compat.v1.variable_scope('generator'):
            gen_out1 = build_generator(data=tf_X, n_hidden=50)
        ## Build the classifier
        with tf.compat.v1.variable_scope('classifier'):
            ## Classifier for the original data
            cls_out1 = build_classifier(data=tf_X,
                                        labels=tf.ones(shape=batch_size))
        # Reuse variables
        with tf.compat.v1.variable_scope('classifier', reuse=True):
            ## Reuse the classifier for generated data
            cls_out2 = build_classifier(data=gen_out1[1],
                                        labels=tf.zeros(shape=batch_size))

            init_op = tf.compat.v1.global_variables_initializer()


if __name__ == '__main__':
    main()
