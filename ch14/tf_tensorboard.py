"""Visualising the graph with TensorBoard.
This module demonstrates visualisation of a computational graph using
TensorBoard.  It depends on the module that defines build_generator() and
build_classifier() functions.  The computational graph defined in this module
consists of a generator and classifier part.
"""
import tensorflow as tf

import tf as tf_util


def main():
    batch_size = 64
    g = tf.Graph()

    with g.as_default():
        tf_X = tf.compat.v1.placeholder(shape=(batch_size, 100),
                                        dtype=tf.float32,
                                        name='tf_X')

        ## Build the generator
        with tf.compat.v1.variable_scope('generator'):
            gen_out1 = tf_util.build_generator(data=tf_X, n_hidden=50)

        ## Build the classifier
        with tf.compat.v1.variable_scope('classifier') as scope:
            ## Classifier for the original data
            cls_out1 = tf_util.build_classifier(
                data=tf_X, labels=tf.ones(shape=batch_size))

            ## Reuse the classifier for generated data
            scope.reuse_variables()
            cls_out2 = tf_util.build_classifier(
                data=gen_out1[1], labels=tf.zeros(shape=batch_size))

    with tf.compat.v1.Session(graph=g) as sess:
        sess.run(tf.compat.v1.global_variables_initializer())

        file_writer = tf.compat.v1.summary.FileWriter(logdir='logs/', graph=g)


if __name__ == '__main__':
    main()
