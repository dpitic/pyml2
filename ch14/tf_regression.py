"""Building a regression model.
This module builds an example model for regression analysis, where the goal is
to implement a linear regression model y = w * x + b

In this model, w and b are the two parameters of this simple regression model
that need to be defined as variables.  Note that x is the input to the model
which can be defined as a placeholder.  For training this model, a cost
function must be formulated, and we will use the Mean Squared Error (MSE) cost
function.

Variable y is the true value which is given as the input to this model for
training.  Therefore, y needs to be defined as a placeholder also.  Finally
the predicted output will be computed using TensorFlow operations.

Input x: tf_x defined as a placeholder
Input y: tf_y defined as a placeholder
Model parameter w: weight defined as a variable
Model parameter b: bias defined as a variable
Model output y: y_hat returned by the TensorFlow operations to compute the
prediction using the regression model.
"""
import os

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf


def make_random_data():
    """Create random regression data with one feature."""
    x = np.random.uniform(low=-2, high=4, size=200)
    y = []
    for t in x:
        r = np.random.normal(loc=0.0, scale=(0.5 + t * t / 3), size=None)
        y.append(r)
    return x, 1.726 * x - 0.84 + np.array(y)


def main():
    # TensorFlow computational graph
    g = tf.Graph()

    with g.as_default():
        tf.compat.v1.set_random_seed(123)
        ## Placeholders
        tf_x = tf.compat.v1.placeholder(shape=(None),
                                        dtype=tf.float32,
                                        name='tf_x')
        tf_y = tf.compat.v1.placeholder(shape=(None),
                                        dtype=tf.float32,
                                        name='tf_y')

        ## Define the variable (model parameters)
        weight = tf.compat.v1.Variable(tf.random.normal(shape=(1, 1),
                                                        stddev=0.25),
                                       name='weight')
        bias = tf.compat.v1.Variable(0.0, name='bias')

        ## Build the model
        y_hat = tf.add(weight * tf_x, bias, name='y_hat')
        print(y_hat)

        ## Compute the cost
        cost = tf.compat.v1.reduce_mean(tf.square(tf_y - y_hat), name='cost')
        print(cost)

        ## Train
        optim = tf.compat.v1.train.GradientDescentOptimizer(
            learning_rate=0.001)
        train_op = optim.minimize(cost, name='train_op')

    ## Create a random sample data set for regression
    np.random.seed(0)
    x, y = make_random_data()
    plt.plot(x, y, 'o')
    plt.title('Random Data Set for Regression')
    plt.show()

    # Train the model
    ## Train/test splits
    x_train, y_train = x[:100], y[:100]
    x_test, y_test = x[100:], y[100:]

    ## Training the model
    n_epochs = 500
    training_costs = []
    with tf.compat.v1.Session(graph=g) as sess:
        ## Run the variables initialiser
        sess.run(tf.compat.v1.global_variables_initializer())

        ## Train the model for n_epochs
        for e in range(n_epochs):
            c, _ = sess.run([cost, train_op],
                            feed_dict={
                                tf_x: x_train,
                                tf_y: y_train
                            })
            training_costs.append(c)
            if not e % 50:
                print('Epoch %4d: %.4f' % (e, c))

    plt.plot(training_costs)
    plt.title('Training Costs')
    plt.show()

    # Executing objects in a TensorFlow graph using their names
    training_costs = []
    with tf.compat.v1.Session(graph=g) as sess:
        ## Run the variables initializer
        sess.run(tf.compat.v1.global_variables_initializer())

        ## Train the model for n_epochs
        for e in range(n_epochs):
            c, _ = sess.run(['cost:0', 'train_op'],
                            feed_dict={
                                'tf_x:0': x_train,
                                'tf_y:0': y_train
                            })
            training_costs.append(c)
            if not e % 50:
                print('Epoch %4d: %.4f' % (e, c))

    # Saving and restoring a model in TensorFlow
    ## Add saver to the graph
    with g.as_default():
        saver = tf.compat.v1.train.Saver()

    ## Train the model
    n_epochs = 500
    training_costs = []
    MODEL_FILE = 'trained-model'

    with tf.compat.v1.Session(graph=g) as sess:
        ## Run the variables initializer
        sess.run(tf.compat.v1.global_variables_initializer())

        ## Train the model for n_epochs
        for e in range(n_epochs):
            c, _ = sess.run(['cost:0', 'train_op'],
                            feed_dict={
                                'tf_x:0': x_train,
                                'tf_y:0': y_train
                            })
            training_costs.append(c)
            if not e % 50:
                print('Epoch %4d: %.4f' % (e, c))

        ## Save model if it doesn't exist
        if not os.path.isfile(MODEL_FILE + '.meta'):
            print('Saving trained model', MODEL_FILE)
            saver.save(sess, MODEL_FILE)
        else:
            print('Trained model file already exists', MODEL_FILE)

    # Restoring a trained model requires two steps:
    # 1. Rebuilding the graph that has the same nodes and names as the saved
    #    model.
    # 2. Restore the saved variables in a new tf.Session environment.
    ## Load a trained model and run the model on test set
    g2 = tf.Graph()
    with tf.compat.v1.Session(graph=g2) as sess:
        new_saver = tf.compat.v1.train.import_meta_graph(MODEL_FILE + '.meta')
        new_saver.restore(sess, MODEL_FILE)

        y_pred = sess.run('y_hat:0', feed_dict={'tf_x:0': x_test})

    print('SSE: %.4f' % (np.sum(np.square(y_pred - y_test))))

    # Visualise the predictions
    x_arr = np.arange(-2, 4, 0.1)

    g2 = tf.Graph()
    with tf.compat.v1.Session(graph=g2) as sess:
        new_saver = tf.compat.v1.train.import_meta_graph(MODEL_FILE + '.meta')
        new_saver.restore(sess, MODEL_FILE)

        y_arr = sess.run('y_hat:0', feed_dict={'tf_x:0': x_arr})

    plt.figure()
    plt.plot(x_train, y_train, 'bo')
    plt.plot(x_test, y_test, 'bo', alpha=0.3)
    plt.plot(x_arr, y_arr.T[:, 0], '-r', lw=3)
    plt.title('Training, Test and Regression')
    plt.show()


if __name__ == '__main__':
    main()
