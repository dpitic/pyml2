"""Transforming Tensors as multidimensional data arrays.
This module demonstrates a selection of operators that can be used to transform
tensors.  Some of these operators work very similar to NumPy array
transformations.
"""
import numpy as np
import tensorflow as tf


def main():
    # In NumPy, use the arr.shape attribute to get the shape of an array.  In
    # TensorFlow use the tf.get_shape() function.
    g = tf.Graph()
    with g.as_default():
        arr = np.array([[1., 2., 3., 3.5], [4., 5., 6., 6.5],
                        [7., 8., 9., 9.5]])
        T1 = tf.compat.v1.constant(arr, name='T1')
        print(T1)
        s = T1.get_shape()
        print('Shape of T1 is', s)
        # Use s to create T2
        T2 = tf.compat.v1.Variable(tf.random.normal(shape=s))
        print('T2', T2)
        # Cannot slice or index s, so convert s into regular Python list and
        # then use normal indexing to create T3
        T3 = tf.compat.v1.Variable(tf.random.normal(shape=(s.as_list()[0], )))
        print('T3', T3)

    # Demonstration of how to reshape tensors.  In NumPy use np.reshape or
    # arr.reshape.  In TensorFlow use tf.reshape().  As in the case for NumPy,
    # one dimension can be set to -1 so that the size of the new dimension
    # will be inferred based on the total size of the array and the other
    # remaining dimensions that are specified.  The following code will
    # reshape tensor T1 to T4 and T4, both of which have rank 3.
    with g.as_default():
        T4 = tf.reshape(T1, shape=[1, 1, -1], name='T4')
        print(T4)
        T5 = tf.reshape(T1, shape=[1, 3, -1], name='T5')
        print(T5)

    # Print the elements of T4 and T5
    with tf.compat.v1.Session(graph=g) as sess:
        print('T4:\n', sess.run(T4))
        print()
        print('T4:\n', sess.run(T5))

    # Transposing arrays
    # In NumPy: arr.T, arr.transpose() and np.transpose(arr)
    # In TensorFlow: tf.transpose(), and additionally the order of the
    # dimensions can be changed by specifying perm=[...]
    with g.as_default():
        T6 = tf.transpose(T5, perm=[2, 1, 0], name='T6')
        print(T6)
        T7 = tf.transpose(T5, perm=[0, 2, 1], name='T7')
        print(T7)

    # Split a tensor into a list of subtensors using tf.split()
    with g.as_default():
        t5_split = tf.split(T5, num_or_size_splits=2, axis=2, name='T8')
        print(t5_split)

    # Concatination of multiple tensors.
    # A list of tensors with the same shape and dtype can be combined into one
    # big tensor using tf.concat()
    g = tf.Graph()
    with g.as_default():
        t1 = tf.ones(shape=(5, 1), dtype=tf.float32, name='t1')
        t2 = tf.zeros(shape=(5, 1), dtype=tf.float32, name='t2')
        print(t1)
        print(t2)

    # Concatenate tensors
    with g.as_default():
        t3 = tf.concat([t1, t2], axis=0, name='t3')
        print(t3)
        t4 = tf.concat([t1, t2], axis=1, name='t4')
        print(t4)

    # Print the values of the concatenated tensors
    with tf.compat.v1.Session(graph=g) as sess:
        print('t3:\n', t3.eval())
        print()
        print('t4:\n', t4.eval())


if __name__ == '__main__':
    main()
