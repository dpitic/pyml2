"""
Module containing reusable utility functions for machine learning.
"""
import os
import struct

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap


def plot_decision_regions(X, y, classifier, test_idx=None, resolution=0.02):
    """
    Plot decision regions for classifier and optionally highlight test data.

    :param X:           Features matrix shape = [n_samples, n_features].
    :param y:           Target values vector size n_samples.
    :param classifier:  Machine learning model classifier.
    :param test_idx:    Tuple containing range of test data.
    :param resolution:  Step parameter passed to arange() in meshgrid().
    :return:            None.
    """
    # Setup marker generator and colour map
    markers = ('s', 'x', 'o', '^', 'v')
    colours = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colours[:len(np.unique(y))])

    # Plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(
        np.arange(x1_min, x1_max, resolution),
        np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # Plot class samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(
            x=X[y == cl, 0],
            y=X[y == cl, 1],
            alpha=0.8,
            c=colours[idx],
            marker=markers[idx],
            label=cl,
            edgecolor='black')

    # Highlight test samples
    if test_idx:
        # Plot all samples
        X_test, y_test = X[test_idx, :], y[test_idx]

        plt.scatter(
            X_test[:, 0],
            X_test[:, 1],
            c='',
            edgecolor='black',
            alpha=1.0,
            linewidth=1,
            marker='o',
            s=100,
            label='test set')


def sigmoid(z):
    """
    Logistic sigmoid function.
    Used in predicting the probability that a particular sample belongs to a
    particular class.
    :param z: Net input value for which to evaluate the function.
    :return:  Value of the function at the given net input point.
    """
    return 1.0 / (1.0 + np.exp(-z))


def cost_1(z):
    """
    Cost function if y = 1
    :param z: Net input value for which to evaluate the cost function.
    :return:  Value of the cost function for the net input.
    """
    return -np.log(sigmoid(z))


def cost_0(z):
    """
    Cost function if y = 0
    :param z: Net input value for which to evaluate the cost function.
    :return:  Value of the cost function for the net input.
    """
    return -np.log(1 - sigmoid(z))


def gini(p):
    """
    Gini impurity measure or splitting criteria for binary decision tree.
    :param p: Proportion of samples that belong to class c for a particular
              node t.
    :return:  Gini impurity measure.
    """
    return p * (1 - p) + (1 - p) * (1 - (1 - p))


def entropy(p):
    """
    Entropy impurity measure or splitting criteria for binary decision tree.
    :param p: Proportion of samples that belong to class c for a particular
              node t.
    :return:  Entropy impurity measure.
    """
    return -p * np.log2(p) - (1 - p) * np.log2(1 - p)


def error(p):
    """
    Classification error impurity measure or splitting criteria for binary
    decision tree.
    :param p: Proportion of samples that belong to class c for a particular
              node t.
    :return:  Classification error measure.
    """
    return 1 - np.max([p, 1 - p])


def lin_regplot(X, y, model):
    """Plot scatter plot of X and y with linear regression line.
    :param X: Data vector for x-axis.
    :param y: Data vector for y-axis.
    :param obj model: Predictor model used to generate linear regression line.
    """
    plt.scatter(X, y, c='steelblue', edgecolor='white', s=70)
    plt.plot(X, model.predict(X), color='black', lw=2)


def load_mnist(path, kind='train'):
    """Load MNIST data from path.

    Load MNIST data from the specified path and return images and labels.  The
    training data set consists of 60,000 training digits and the test data set
    contains 10,000 samples.  The images consist of 28 x 28 pixels, and each
    pixel is represented by a grey scale intensity value.  The 28 x 28 pixels
    are unrolled into one-dimensional row vectors, which represent the rows in
    the images array (784 per row or image).  The image pixels are normalised
    to the range -1 to 1.  The labels array contains the corresponding target
    variable, the class labels (integers 0-9) of the handwritten digits.

    :param str path: Path to MNIST data files.
    :param str kind: MNIST data file kind prefix.  Default is 'train' for
    training data set, or 't10k' for images and labels data set.
    :return: images n x m dimensional NumPy array, where n is the number of
    samples and m is the number of features (pixels); labels array contains
    the target variables, the class labels (integers 0-9) of the handwritten
    digits.
    """
    labels_path = os.path.join(path, '%s-labels-idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images-idx3-ubyte' % kind)

    # Load labels
    with open(labels_path, 'rb') as lbpath:
        # Magic number is a description of the file protocol, n is the number
        # of items.  Read big endian, 2 unsigned integers.
        magic, n = struct.unpack('>II', lbpath.read(8))
        labels = np.fromfile(lbpath, dtype=np.uint8)

    # Load images
    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack('>IIII', imgpath.read(16))
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 784)
        # Normalise the pixels to the range -1 to 1
        images = ((images / 255.) - 0.5) * 2

    return images, labels


def batch_generator(X, y, batch_size=64, shuffle=False, random_seed=None):
    """Return a generator with a tuple for a match of samples.

    This function iterates through mini-batches of data to normalise the data
    (mean centering and division by the standard deviation) for better
    training performance and convergence.
    :param X: Feature matrix.
    :param y: Class label vector.
    :param batch_size: Number of elements in batch, default=64.
    :param shuffle: Whether to randomly shuffle the data or not, default=False.
    :param int random_seed: Random number generator seed.
    :return: Feature matrix and class label vector with number of elements
    equal to batch_size.
    """
    idx = np.arange(y.shape[0])

    if shuffle:
        rng = np.random.RandomState(random_seed)
        rng.shuffle(idx)
        X = X[idx]
        y = y[idx]

    for i in range(0, X.shape[0], batch_size):
        yield (X[i:i + batch_size, :], y[i:i + batch_size])
