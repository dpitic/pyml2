"""
Learning with ensembles.
"""

from itertools import product

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, auc, roc_curve
from sklearn.model_selection import (GridSearchCV, cross_val_score,
                                     train_test_split)
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.tree import DecisionTreeClassifier

import mvc
from ensemble import ensemble_error


def main():
    # Calculate the error rate of an ensemble consisting of 11 base
    # classifiers (n=11), where each classifier has an error rate of 0.25
    # i.e. epsilon=0.25
    print(
        '\nEnsemble error of 11 classifiers where each classifier has an'
        '\nerror rate of 0.25\n', ensemble_error(n_classifier=11, error=0.25))
    # Compute the ensemble error rates for a range of different base errors
    # to visualise the relationship between ensemble and base errors.  The
    # plot shows that the error probability of an ensemble is always better
    # than the error of an individual classifier, as long as the base
    # classifiers perform better than random guessing (epsilon < 0.5).
    error_range = np.arange(0.0, 1.01, 0.01)
    ens_errors = [
        ensemble_error(n_classifier=11, error=error) for error in error_range
    ]
    plt.plot(error_range, ens_errors, label='Ensemble error', linewidth=2)
    plt.plot(
        error_range,
        error_range,
        linestyle='--',
        label='Base error',
        linewidth=2)
    plt.xlabel('Base error')
    plt.ylabel('Base/Ensemble error')
    plt.legend(loc='upper left')
    plt.grid(alpha=0.5)
    plt.title('Ensemble Error Probability')
    plt.show()

    # Combining classifiers via majority vote
    # The NumPy functions argmax()and bincount() can be used to demonstrate
    # the concept of weighted majority.  Assume that an ensemble of three
    # base classifiers, and we want to predict the class label of a given
    # sample instance, x.  Two out of three base classifiers predict the class
    # label 0, and one predicts the sample belongs to class 1.  Assume the
    # weights are 0.2 for C1 and C2, and 0.6 for C3.
    print('\nargmax:',
          np.argmax(np.bincount([0, 0, 1], weights=[0.2, 0.2, 0.6])))
    # Certain classifiers in scikit-learn can also return the probability of a
    # predicted class label via the predict_proba() method.  Using the
    # predicted class probabilities instead of the class labels for majority
    # voting can be useful if the classifiers in the ensemble are well
    # calibrated.  Assume the classifiers return the following class
    # membership probabilities for a particular sample x:
    #  C1(x) -> [0.9, 0.1]
    #  C2(x) -> [0.8, 0.2]
    #  C3(x) -> [0.4, 0.6]
    # To implement the weighted majority vote based on class probabilities, we
    # can use the NumPy numpy.average() and numpy.argmax() functions.
    ex = np.array([[0.9, 0.1], [0.8, 0.2], [0.4, 0.6]])
    p = np.average(ex, axis=0, weights=[0.2, 0.2, 0.6])
    print('\nAverage:\n', p)

    # Using the majority voting principle to make predictions.
    # This section demonstrates the use of the MajorityVoteClassifier using
    # using the Iris data set from scikit-learn's data set module.  We only
    # select two features, sepal width and petal length, to make the
    # classification task more challenging for illustration purposes.
    # Although the MajorityVoteClassifier generalises to multiclass problems,
    # we will only classify flower samples from the Iris-versicolor and
    # Iris-virginica classes, with which we will compute the ROC AUC later.
    iris = datasets.load_iris()
    X, y = iris.data[50:, [1, 2]], iris.target[50:]
    le = LabelEncoder()
    y = le.fit_transform(y)
    # Split the Iris samples into 50% training and 50% test data sets.
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.5, random_state=1, stratify=y)
    # Define the three different classifiers used to train the models
    clf1 = LogisticRegression(
        penalty='l2', C=0.001, solver='liblinear', random_state=1)
    clf2 = DecisionTreeClassifier(
        max_depth=1, criterion='entropy', random_state=0)
    clf3 = KNeighborsClassifier(n_neighbors=1, p=2, metric='minkowski')

    pipe1 = Pipeline([['sc', StandardScaler()], ['clf', clf1]])
    pipe3 = Pipeline([['sc', StandardScaler()], ['clf', clf3]])

    clf_labels = ['Logistic regression', 'Decision Tree', 'KNN']
    # Evaluate the model performance of each classifier via 10-fold cross-
    # validation on the training data set.
    print('\n10-fold cross validation\n')
    for clf, label in zip([pipe1, clf2, pipe3], clf_labels):
        scores = cross_val_score(
            estimator=clf, X=X_train, y=y_train, cv=10, scoring='roc_auc')
        print('ROC AUCc: %0.2f (+/- %0.2f) [%s]' % (scores.mean(),
                                                    scores.std(), label))
    # Combine the individual classifiers for majority rule voting using the
    # custom implementation MajorityVoteClassifier
    mv_clf = mvc.MajorityVoteClassifier(classifiers=[pipe1, clf2, pipe3])
    clf_labels += ['Majority voting']
    all_clf = [pipe1, clf2, pipe3, mv_clf]

    print('\nWith majority voting:\n')
    for clf, label in zip(all_clf, clf_labels):
        scores = cross_val_score(
            estimator=clf, X=X_train, y=y_train, cv=10, scoring='roc_auc')
        print("ROC AUC:%0.2f (+/- %0.2f) [%s]" % (scores.mean(), scores.std(),
                                                  label))
    # Evaluating and tuning the ensemble classifier
    # This section computes the ROC curves from the test set to check that the
    # MajorityVoteClassifier generalises well with unseen data.  The test
    # set is not to be used for model selection; its purpose is to report an
    # unbiased estimate of the generalisation performance of a classifier
    # system.
    colours = ['black', 'orange', 'blue', 'green']
    linestyles = [':', '--', '-.', '-']
    for clf, label, clr, ls in zip(all_clf, clf_labels, colours, linestyles):
        # Assuming the label of the positive class is 1
        y_pred = clf.fit(X_train, y_train).predict_proba(X_test)[:, 1]
        fpr, tpr, threesholds = roc_curve(y_true=y_test, y_score=y_pred)
        roc_auc = auc(x=fpr, y=tpr)
        plt.plot(
            fpr,
            tpr,
            color=clr,
            linestyle=ls,
            label='%s (auc = %0.2f)' % (label, roc_auc))

    plt.legend(loc='lower right')
    plt.plot([0, 1], [0, 1], linestyle='--', color='gray', linewidth=2)

    plt.xlim([-0.1, 1.1])
    plt.ylim([-0.1, 1.1])
    plt.grid(alpha=0.5)
    plt.xlabel('False positive rate (FPR)')
    plt.ylabel('True positive rate (TPR)')
    plt.title('ROC Curves')
    plt.show()
    # This section will plot the decision regions of the ensemble classifiers.
    # Although it is not necessary to standardise the training features prior
    # to model fitting, because the logistic regression and k-nearest
    # neighbours pipeline will automatically take care of it, we will
    # standardise the training data set so that the decision regions of the
    # decision tree will be on the same scale for visual purposes.
    sc = StandardScaler()
    X_train_std = sc.fit_transform(X_train)
    all_clf = [pipe1, clf2, pipe3, mv_clf]
    x_min = X_train_std[:, 0].min() - 1
    x_max = X_train_std[:, 0].max() + 1
    y_min = X_train_std[:, 1].min() - 1
    y_max = X_train_std[:, 1].max() + 1
    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, 0.1), np.arange(y_min, y_max, 0.1))
    f, axarr = plt.subplots(
        nrows=2, ncols=2, sharex='col', sharey='row', figsize=(7, 5))
    for idx, clf, tt in zip(product([0, 1], [0, 1]), all_clf, clf_labels):
        clf.fit(X_train_std, y_train)
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        axarr[idx[0], idx[1]].contourf(xx, yy, Z, alpha=0.3)
        axarr[idx[0], idx[1]].scatter(
            X_train_std[y_train == 0, 0],
            X_train_std[y_train == 0, 1],
            c='blue',
            marker='^',
            s=50)
        axarr[idx[0], idx[1]].scatter(
            X_train_std[y_train == 1, 0],
            X_train_std[y_train == 1, 1],
            c='green',
            marker='o',
            s=50)
        axarr[idx[0], idx[1]].set_title(tt)
    plt.text(
        -3.5,
        -5.,
        s='Sepal width [standardised]',
        ha='center',
        va='center',
        fontsize=12)
    plt.text(
        -12.5,
        4.5,
        s='Petal length [standardised]',
        ha='center',
        va='center',
        fontsize=12,
        rotation=90)
    plt.show()
    # Before tuning the individual classifier's parameters for ensemble
    # classification, call the get_params() method to get a basic idea of how
    # we can access the individual parameters inside a GridSearch object.
    print('\nIndividual parameters inside the GridSearch:\n',
          mv_clf.get_params())
    # Tune the inverse regularisation parameter C of the logistic regression
    # classifier and the decision tree depth via a grid search.
    params = {
        'decisiontreeclassifier__max_depth': [1, 2],
        'pipeline-1__clf__C': [0.001, 0.1, 100.0]
    }
    grid = GridSearchCV(
        estimator=mv_clf, param_grid=params, cv=10, scoring='roc_auc')
    grid.fit(X_train, y_train)
    # Print the different hyperparameter value combinations and the average
    # ROC AUC scores comuted via a 10-fold cross-validation
    for r, _ in enumerate(grid.cv_results_['mean_test_score']):
        print('%0.3f +/- %0.2f %r' % (grid.cv_results_['mean_test_score'][r],
                                      grid.cv_results_['std_test_score'][r] /
                                      2.0, grid.cv_results_['params'][r]))
    print('\nBest parameters: %s' % grid.best_params_)
    print('\nAccuracy: %.2f' % grid.best_score_)

    # Bagging - building an ensemble of classifiers from bootstrap samples
    # Bagging is an ensemble learning technique that is closely related to
    # MajorityVoteClassifier.  Instead of using the same training set to fit
    # the individual classifiers in the ensemble, we draw bootstrap samples
    # (random samples with replacement) from the initial training set, which is
    # why bagging is also known as bootstrap aggregating.  This section will
    # demonstrate a simple example of bagging by hand and use scikit-learn for
    # classifying wine samples.
    #
    # Applying bagging to classifying samples in the Wine data set
    # Here we will only consider the Wine classes 2 and 3, and select two
    # features: Alcohol and OD280/OD315 of diluted wines.
    df_wine = pd.read_csv(
        'https://archive.ics.uci.edu/ml/'
        'machine-learning-databases/wine/wine.data',
        header=None)
    df_wine.columns = [
        'Class label', 'Alcohol', 'Malic acid', 'Ash', 'Alcalinity of ash',
        'Magnesium', 'Total phenols', 'Flavanoids', 'Nonflavanoid phenols',
        'Proanthocyanins', 'Colour intensity', 'Hue',
        'OD280/OD315 of diluted wines', 'Proline'
    ]
    # Drop 1 class
    df_wine = df_wine[df_wine['Class label'] != 1]

    y = df_wine['Class label'].values
    X = df_wine[['Alcohol', 'OD280/OD315 of diluted wines']].values

    le = LabelEncoder()
    y = le.fit_transform(y)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=1, stratify=y)
    # Use an unpruned decision tree as the base classifier and create an
    # enseble of 500 decision trees fit on different bootstrap samples of the
    # training data set.
    tree = DecisionTreeClassifier(
        criterion='entropy', max_depth=None, random_state=1)
    bag = BaggingClassifier(
        base_estimator=tree,
        n_estimators=500,
        max_samples=1.0,
        max_features=1.0,
        bootstrap=True,
        bootstrap_features=False,
        n_jobs=-1,
        random_state=1)
    # Calculate the accuracy score of the prediction on the training and test
    # data set to compare the performance of the bagging classifier to the
    # performance of a single unpruned decision tree.
    tree = tree.fit(X_train, y_train)
    y_train_pred = tree.predict(X_train)
    y_test_pred = tree.predict(X_test)

    tree_train = accuracy_score(y_train, y_train_pred)
    tree_test = accuracy_score(y_test, y_test_pred)
    print('\nDecision tree train/test accuracites %.3f/%.3f' % (tree_train,
                                                                tree_test))
    # The unpruned decision tree predicts all the class labels of the training
    # samples correctly; however, the substantially lower test accuracy
    # indicates high variance (overfitting) of the model.
    bag = bag.fit(X_train, y_train)
    y_train_pred = bag.predict(X_train)
    y_test_pred = bag.predict(X_test)

    bag_train = accuracy_score(y_train, y_train_pred)
    bag_test = accuracy_score(y_test, y_test_pred)
    print('\nBagging train/test accuracies %.3f/%.3f' % (bag_train, bag_test))
    # Compare the decision regions between the decision tree and the bagging
    # classifier.
    x_min = X_train[:, 0].min() - 1
    x_max = X_train[:, 0].max() + 1
    y_min = X_train[:, 1].min() - 1
    y_max = X_train[:, 1].max() + 1

    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, 0.1), np.arange(y_min, y_max, 0.1))
    f, axarr = plt.subplots(
        nrows=1, ncols=2, sharex='col', sharey='row', figsize=(8, 3))
    for idx, clf, tt in zip([0, 1], [tree, bag], ['Decision tree', 'Bagging']):
        clf.fit(X_train, y_train)

        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        axarr[idx].contourf(xx, yy, Z, alpha=0.3)
        axarr[idx].scatter(
            X_train[y_train == 0, 0],
            X_train[y_train == 0, 1],
            c='blue',
            marker='^')
        axarr[idx].scatter(
            X_train[y_train == 1, 0],
            X_train[y_train == 1, 1],
            c='green',
            marker='o')
        axarr[idx].set_title(tt)

    axarr[0].set_ylabel('Alcohol', fontsize=12)
    plt.text(
        10.2,
        -1.2,
        s='OD280/OD315 of diluted wines',
        ha='center',
        va='center',
        fontsize=12)
    plt.show()

    # Leveraging weak learners via adptive boosting.
    # This section explores boosting, with special focus on its most common
    # implementation AdaBoost (Adaptive Boosting).  In boosting, the ensemble
    # consists of very simple base classifiers, also often referred to as
    # weak learners, which often only have a light performance advantage over
    # random guessing.  A typical example of a weak learner is a decision tree
    # stump.  The key concept behind boosting is to focus on training samples
    # that are hard to classify, that si, to let the weak learners subsequently
    # learn from misclassified training samples to improve the performance of
    # the ensemble.
    #
    # How boosting works
    # In contrast to bagging, the initial formulation of boosting, the
    # algorithm uses subsets of training samples drawn from the training data
    # set without replacement.  Boosting can lead to a decrease in bias as well
    # as variance, compared to bagging models.  In parctice however, algorithms
    # such as AdaBoost are also known for their high variance, that is, the
    # tendency to overfit the training data.
    #
    # Unlike the original boosting procedure, AdaBoost uses the complete
    # training set to train the weak learners where the training samples are
    # reweighted in each iteration to build a strong classifier that learns
    # from the mistakes of the previous weak learners in the ensemble.
    #
    # Applying AdaBoost using scikit-learn
    # This section demonstrates how to train an AdaBoost ensemble classifier
    # via scikit-learn.  It will use the same Wine subset used in the previous
    # section to train the bagging meta-classifier.  Using the base_estimator
    # attribute, we will train the AdaBoostClassifier on 500 decision tree
    # stumps.
    print('\nAdaptive Boosting\n')
    tree = DecisionTreeClassifier(
        criterion='entropy', max_depth=1, random_state=1)
    ada = AdaBoostClassifier(
        base_estimator=tree,
        n_estimators=500,
        learning_rate=0.1,
        random_state=1)
    tree = tree.fit(X_train, y_train)
    y_train_pred = tree.predict(X_train)
    y_test_pred = tree.predict(X_test)

    tree_train = accuracy_score(y_train, y_train_pred)
    tree_test = accuracy_score(y_test, y_test_pred)
    print('\nDecision tree train/test accuracies %.3f/%.3f' % (tree_train,
                                                               tree_test))

    ada = ada.fit(X_train, y_train)
    y_train_pred = ada.predict(X_train)
    y_test_pred = ada.predict(X_test)

    ada_train = accuracy_score(y_train, y_train_pred)
    ada_test = accuracy_score(y_test, y_test_pred)
    print('\nAdaBoost train/test accuracies %.3f/%.3f' % (ada_train, ada_test))
    # Plot decision regions
    x_min = X_train[:, 0].min() - 1
    x_max = X_train[:, 0].max() + 1
    y_min = X_train[:, 1].min() - 1
    y_max = X_train[:, 1].max() + 1
    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, 0.1), np.arange(y_min, y_max, 0.1))
    f, axarr = plt.subplots(1, 2, sharex='col', sharey='row', figsize=(8, 3))
    for idx, clf, tt in zip([0, 1], [tree, ada],
                            ['Decision Tree', 'AdaBoost']):
        clf.fit(X_train, y_train)
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)
        axarr[idx].contourf(xx, yy, Z, alpha=0.3)
        axarr[idx].scatter(
            X_train[y_train == 0, 0],
            X_train[y_train == 0, 1],
            c='blue',
            marker='^')
        axarr[idx].scatter(
            X_train[y_train == 1, 0],
            X_train[y_train == 1, 1],
            c='green',
            marker='o')
        axarr[idx].set_title(tt)

    axarr[0].set_ylabel('Alcohol', fontsize=12)
    plt.text(
        10.2,
        -0.5,
        s='OD280/OD315 of diluted wines',
        ha='center',
        va='center',
        fontsize=12)
    plt.show()


if __name__ == '__main__':
    main()
