"""
Ensemble utility functions.
"""
import math

from scipy.special import comb


def ensemble_error(n_classifier, error):
    """
    @brief      Probability mass function.

    @details    This function is used to compare an idealistic ensemble
                classifier to a base classifier over a range of different base
                error rates.

    @param      n_classifier: number of classifiers
    @param      error: error rate of each individual classifier.

    @return     Ensemble error rate.
    """
    k_start = int(math.ceil(n_classifier / 2.))
    probs = [
        comb(n_classifier, k) * error ** k * (1 - error) ** (n_classifier - k)
        for k in range(k_start, n_classifier + 1)
    ]
    return sum(probs)
