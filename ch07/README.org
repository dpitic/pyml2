* Chapter 7: Combining Different Models for Ensemble Learning

This chapter explores different methods for constructing a set of classifiers
that can often have a better predictive performance than any of its
individual members.  The topics covered include:

- Making predictions based on majority voting
- Using bagging to reduce overfitting by drawing random combinations of the
  of the training set with repetition
- Application of boosting to build powerful models from weak learners that
  learn from their mistakes.

* Learning with ensembles

Goal of ensemble methods is to combine different classifiers into a meta-
classifier that has better generalisation performance than each individual
classifier alone.

This chapter focuses on the most popular ensemble methods that use the
majority voting principle.  This means that the class label is selected that
has been predicted by the majority of classifiers, i.e. received more than
50% of the votes.  In the strict sense, majority vote refers to binary
class settings only, however, it is possible to generalise the majority
voting principle to multi-class settings, called plurality voting.  The class
label that received the most votes (mode) is selected.

Ensemble methods combine different classification models to cancel out their
individual weaknesses, which often results in stable and well performing
models that are very attractive for industrial applications.
