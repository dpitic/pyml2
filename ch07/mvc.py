"""
This module implements the Majority Vote Classifier.
"""

import operator

import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin, clone
from sklearn.externals import six
from sklearn.pipeline import _name_estimators
from sklearn.preprocessing import LabelEncoder


class MajorityVoteClassifier(BaseEstimator, ClassifierMixin):
    """
    @brief      A majority vote ensemble classifier.  Based on BaseEstimator
                and ClassifierMixin.
    """

    def __init__(self, classifiers, vote='classlabel', weights=None):
        """
        @brief      Initialise a MajorityVoteClassifier object.

        @details    detailed description

        @param      classifiers: array-like, shape = [n_classifiers]
                    Different classifiers for the ensemble.
        @param      vote: string {'classlabel', 'probability'}
                    (default='classlabel') If 'classlabel' the prediction is
                    is based on the argmax of class labels.  Else if
                    'probability', the argmax of the sum of probabilities is
                    used to predict the class label (recommended for
                    calibrated classifiers).
        @param      weights: array-like, shape = [n_classifiers], optional
                    (default=None) If a list of 'int' or 'float' values are
                    provided, the classifiers are weighted by importance.
                    Uses uniform weights if 'weights=None'.

        @return     Initialised MajorityVoteClassifier object.
        """
        self.classifiers = classifiers
        self.named_classifiers = {
            key: value
            for key, value in _name_estimators(classifiers)
        }
        self.vote = vote
        self.weights = weights

    def fit(self, X, y):
        """
        @brief      Fit classifiers.

        @details    detailed description

        @param      X: array-like, sparse matrix,
                    shape = [n_samples, n_features] Matrix of training samples.
        @param      y: array-like, shape = [n_samples] Vector of target class
                    labels.

        @return     self: object
        """
        if self.vote not in ('probability', 'classlabel'):
            raise ValueError("Vote must be 'probability' or 'classlabel'"
                             "; got (vote=%r)" % self.vote)
        if self.weights and len(self.weights) != len(self.classifiers):
            raise ValueError('Number of classifiers and weights must be equal'
                             ' got %d weights, %d classifiers' % (len(
                                 self.weights), len(self.classifiers)))

        # Use LabelEncoder to ensure class labels start with 0, which is
        # important for np.argmax() call in self.predict
        self.labelenc_ = LabelEncoder()
        self.labelenc_.fit(y)
        self.classes = self.labelenc_.classes_
        self.classifiers_ = []
        for clf in self.classifiers:
            fitted_clf = clone(clf).fit(X, self.labelenc_.transform(y))
            self.classifiers_.append(fitted_clf)
        return self

    def predict(self, X):
        """
        @brief      Predict class labels for X.

        @details    Predict the class label via majority vote based on the
                    class labels if the object is initialised with vote=
                    'classlabel'.  Alternatively, predict the class label
                    based on the class membership probabilities if the object
                    is initialised with vote='probability'.

        @param      X: array-like, sparse matrix,
                    shape = [n_samples, n_features]
                    Matrix of training samples.

        @return     maj_vote: array-like, shape = [n_samples] Predicted class
                    labels.
        """
        if self.vote == 'probability':
            maj_vote = np.argmax(self.predict_proba(X), axis=1)
        else:
            # 'classlabel' vote.  Collect results from clf.predict calls
            predictions = np.asarray(
                [clf.predict(X) for clf in self.classifiers_]).T

            maj_vote = np.apply_along_axis(
                lambda x: np.argmax(np.bincount(x, weights=self.weights)),
                axis=1,
                arr=predictions)
            maj_vote = self.labelenc_.inverse_transform(maj_vote)
            return maj_vote

    def predict_proba(self, X):
        """
        @brief      Predict class probabilities for X.

        @details    Return the averaged probabilities, useful when computing
                    ROC AUC.

        @param      X: array-like, sparse matrix,
                    shape =[n_samples, n_features] Training vectors, where
                    n_samples is the number of samples and n_features is the
                    number of features.

        @return     avg_proba: array-like, shape = [n_samples, n_features]
                    Weighted average probability for each class per sample.
        """
        probas = np.asarray(
            [clf.predict_proba(X) for clf in self.classifiers_])
        avg_proba = np.average(probas, axis=0, weights=self.weights)
        return avg_proba

    def get_params(self, deep=True):
        """
        @brief      Get classifier parameter names for GirdSearch.

        @details    Overridden method that uses _name_estimators() function
                    to access the parameters of individual classifiers in the
                    ensemble.

        @param      deep: boolean flag specifying deep copy.

        @return     Classifier parameters reference or deep copy.
        """
        if not deep:
            return super(MajorityVoteClassifier, self).get_params(deep=False)
        else:
            out = self.named_classifiers.copy()
            for name, step in six.iteritems(self.named_classifiers):
                for key, value in six.iteritems(step.get_params(deep=True)):
                    out['%s__%s' % (name, key)] = value
            return out
