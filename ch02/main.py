"""
Main module used to run the Perceptron and Adaline machine learning algorithms.
It loads the Iris data set from the current directory, trains the models, and
produces visualisations.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import adalinegd
import adalinesgd
import mlutils
import perceptron


def main():
    # Load the Iris data set into a DataFrame object.  The column attributes:
    # +----------------------------------------------------------------------+
    # | Col. | Description                                                   |
    # +------+---------------------------------------------------------------+
    # | 0    | Sepal length [cm]                                             |
    # | 1    | Sepal width [cm]                                              |
    # | 2    | Petal length [cm]                                             |
    # | 3    | Petal width [cm]                                              |
    # | 4    | Class { 'Iris Setosa', 'Iris Versicolor', 'Iris Virginica' }  |
    # +----------------------------------------------------------------------+
    df = pd.read_csv('./iris.data', header=None)
    print(df.tail())

    # Select setosa and versicolor (first 100 rows of data)
    y = df.iloc[0:100, 4].values
    # Convert class labels into 2 integer class labels: -1 = setosa,
    # 1 = versicolor
    y = np.where(y == 'Iris-setosa', -1, 1)

    # Extract sepal length and petal length into feature matrix which has
    # shape = [n_samples, n_features]; columns are sepal length & petal length
    X = df.iloc[0:100, [0, 2]].values

    # Plot data: x-axis = sepal length, y-axis = petal length
    plt.scatter(X[:50, 0], X[:50, 1], color='red', marker='o', label='setosa')
    plt.scatter(
        X[50:100, 0],
        X[50:100, 1],
        color='blue',
        marker='x',
        label='versicolor')
    plt.xlabel('sepal length [cm]')
    plt.ylabel('petal length [cm]')
    plt.legend(loc='upper left')
    plt.title('Iris Training Data')
    plt.show()

    # Training the perceptron model
    ppn = perceptron.Perceptron(eta=0.1, n_iter=10)
    ppn.fit(X, y)

    # Plot the misclassification error for each epoch to check whether the
    # algorithm converged and found a decision boundary
    plt.plot(range(1, len(ppn.errors_) + 1), ppn.errors_, marker='o')
    plt.xlabel('Epochs')
    plt.ylabel('Number of updates')
    plt.title('Training the Perceptron Model')
    plt.show()

    # Plot decision regions
    mlutils.plot_decision_regions(X, y, classifier=ppn)
    plt.xlabel('sepal length [cm]')
    plt.ylabel('petal length [cm]')
    plt.legend(loc='upper left')
    plt.title('Decision Regions')
    plt.show()

    # Adaline model
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(10, 4))

    # Plot the cost against the number of epochs for two different learning
    # rates.
    ada1 = adalinegd.AdalineGD(n_iter=10, eta=0.01).fit(X, y)
    ax[0].plot(range(1, len(ada1.cost_) + 1), np.log10(ada1.cost_), marker='o')
    ax[0].set_xlabel('Epochs')
    ax[0].set_ylabel('log(Sum-squared-error)')
    ax[0].set_title('Adaline - Learning rate 0.01')

    ada2 = adalinegd.AdalineGD(n_iter=10, eta=0.0001).fit(X, y)
    ax[1].plot(range(1, len(ada2.cost_) + 1), ada2.cost_, marker='o')
    ax[1].set_xlabel('Epochs')
    ax[1].set_ylabel('Sum-squared-error')
    ax[1].set_title('Adaline - Learning rate 0.0001')
    plt.show()

    # Improve gradient descent through feature scaling.
    # Use a feature scaling method called standardisation, which gives the data
    # the property of a standard normal distribution, which helps gradient
    # descent learning to converge more quickly.  It shifts the mean of each
    # feature so that it is centred at zero, and each feature has a standard
    # deviation of 1.
    X_std = np.copy(X)
    X_std[:, 0] = (X[:, 0] - X[:, 0].mean()) / X[:, 0].std()
    X_std[:, 1] = (X[:, 1] - X[:, 1].mean()) / X[:, 1].std()
    # After standardisation, train Adaline again and see that it now
    # converges after a small number of epochs using learning rate 0.01
    ada = adalinegd.AdalineGD(n_iter=15, eta=0.01)
    ada.fit(X_std, y)

    mlutils.plot_decision_regions(X_std, y, classifier=ada)
    plt.title('Adaline - Gradient Descent')
    plt.xlabel('sepal length [standardised]')
    plt.ylabel('petal length [standardised]')
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.show()

    plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')
    plt.xlabel('Epochs')
    plt.ylabel('Sum-squared-error')
    plt.title('Adaline GD Learning')
    plt.show()

    # Adaline with stochastic gradient descent (with random shuffle)
    ada = adalinesgd.AdalineSGD(eta=0.01, n_iter=15, random_state=1)
    ada.fit(X_std, y)

    mlutils.plot_decision_regions(X_std, y, classifier=ada)
    plt.title('Adaline - Stochastic Gradient Descent')
    plt.xlabel('sepal length [standardised]')
    plt.ylabel('petal length [standardised]')
    plt.legend(loc='upper left')

    plt.tight_layout()
    plt.show()

    plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')
    plt.xlabel('Epochs')
    plt.ylabel('Average Cost')
    plt.title('Adaline SGD Learning')

    plt.tight_layout()
    plt.show()

    # The model can be updated, for example in an online learning scenario with
    # streaming data, by calling the partial_fit() method on individual samples.
    ada.partial_fit(X_std[0, :], y[0])


if __name__ == '__main__':
    main()
