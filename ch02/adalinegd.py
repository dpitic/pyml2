"""
Implementation of ADAptive LInear NEuron (Adaline), which is a single layer
neural network, and is considered an improvement on the Rosenblatt perceptron
algorithm.  The key difference between the Adaline rule (Widrow-Hoff rule) and
Rosenblatt's perceptron is that the weights are updated based on a linear
activation function, rather than a unit step function (like in the perceptron).
In Adaline, this linear activation function is simply the identity function of
the net input.  While the linear activation function is used for learning the
weights, a threshold function is still used to make the final prediction, which
is similar to the unit step function.
"""
import numpy as np


class AdalineGD(object):
    """
    @brief      ADAptive LInear Neuron classifier.

    @details    Adaline implementation with cost function minimisation using
                gradient descent.
    """

    def __init__(self, eta=0.01, n_iter=50, random_state=1):
        """
        @brief      Initialise a new AdalineGD object.

        @details    Create a new AdalineGD object.

        @param      eta:          float; Learning rate (between 0.0 and 1.0)
        @param      n_iter:       int; Number of passes over the training data
                                  set.
        @param      random_state: int; Random number generator seed for
                                  random weight initialisation.

        @return     New AdalineGD object.
        """
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state

    def fit(self, X, y):
        """
        @brief      Fit training data.

        @details    Train adaptive linear neuron and update weights by
                    minimising the cost function via gradient descent.

        @param      X: array-like, shape = [n_samples, n_features]
                       Training vectors matrix, where n_samples is the number
                       of samples and n_features is the number of features.
        @param      y: array-like, shape = [n_samples]
                       Target values vector.

        @return     self: object
        """
        rgen = np.random.RandomState(self.random_state)
        # Initialise weights
        self.w_ = rgen.normal(loc=0.0, scale=0.01, size=1 + X.shape[1])
        # Cost vector is sum of squares cost function value in each epoch
        self.cost_ = []

        for i in range(self.n_iter):
            net_input = self.net_input(X)
            # Activation function has no effect in this implementation since
            # it is defined as an identity function.
            output = self.activation(net_input)
            errors = y - output
            # Calculate weights for 1 to m for whole training data set
            self.w_[1:] += self.eta * X.T.dot(errors)
            # For bias unit (zero weight) calculate gradient based on the whole
            # training data set.
            self.w_[0] += self.eta * errors.sum()
            cost = (errors**2).sum() / 2.0
            self.cost_.append(cost)
        return self

    def net_input(self, X):
        """
        @brief      Calculate net input.

        @details    Calculate z, the net input for neuron, which is the dot
                    product of the weights and the inputs.

        @param      X: array-like, shape = [n_samples, n_features]
                       Features matrix, where n_samples is the number of samples
                       and n_features is the number of features.

        @return     Net input vector of shape = [n_samples]
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
        """
        @brief      Compute linear activation.

        @details    Implemented as the identity function, therefore has no
                    effect in the code.  Added to illustrate how information
                    flows through a single layer neural network: features from
                    input data -> net input -> activation => output.

        @param      X: array-like, shape = [n_samples]
                       Net input vector, where n_samples is the number of
                       samples.

        @return     Activated vector of shape = [n_samples]
        """
        return X

    def predict(self, X):
        """
        @brief      Return class label after unit step.

        @details    Applies the decision (threshold) function to the net input
                    to return the class label 1 if net input > 0, or -1
                    otherwise.

        @param      X: array-like, shape = [n_samples, n_features]
                       Features matrix, where n_samples is the number of samples
                       and n_features is the number of features.

        @return     Class label 1 or -1 after unit step.
        """
        return np.where(self.activation(self.net_input(X)) >= 0.0, 1, -1)
