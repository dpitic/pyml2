"""
Single layer Adaline neuron using stochastic gradient descent.
"""
import numpy as np


class AdalineSGD(object):
    """
    ADAptive LInear NEuron classifier using stochastic gradient descent.
    This is a popular alternative to the batch gradient descent algorithm,
    sometimes called iterative or online gradient descent.  Instead of updating
    the weights based on the sum of the accumulated errors over all samples,
    the weights are updated incrementally for each training sample.
    """

    def __init__(self, eta=0.01, n_iter=10, shuffle=True, random_state=None):
        """
        Initialise a new AdalineSGD object.
        :param eta:          Learning rate.
        :param n_iter:       Number of passes over the training data set
                             (epochs).
        :param shuffle:      Shuffles the training data every epoch if True to
                             prevent cycles.
        :param random_state: Random number generator seed for random weight
                             initialisation.
        """
        self.eta = eta
        self.n_iter = n_iter
        self.w_initialised = False
        self.shuffle = shuffle
        self.random_state = random_state

    def fit(self, X, y):
        """
        Fit training data.  The method updates the weights after each training
        sample.
        :param X: array-like, shape = [n_samples, n_features]
                  Standardised training vectors matrix, where n_samples is the
                  number of samples and n_features is the number of features.
        :param y: array-like, shape = [n_samples]
                  Target values vector.
        :return:  Trained self object.
        """
        self._initialise_weights(X.shape[1])
        self.cost_ = []
        for i in range(self.n_iter):
            if self.shuffle:
                X, y = self._shuffle(X, y)
            cost = []
            for xi, target in zip(X, y):
                cost.append(self._update_weights(xi, target))
            avg_cost = sum(cost) / len(y)
            self.cost_.append(avg_cost)
        return self

    def partial_fit(self, X, y):
        """
        Fit training data without reinitialising the weights.

        This method can be used for online learning with streaming data to
        update the model with individual samples.
        :param X: Training data vector shape = [n_features].
        :param y: Target value (scalar value).
        :return:  Trained self object.
        """
        if not self.w_initialised:
            self._initialise_weights(X.shape[1])
        if y.ravel().shape[0] > 1:
            for xi, target in zip(X, y):
                self._update_weights(xi, target)
        else:
            self._update_weights(X, y)
        return self

    def _shuffle(self, X, y):
        """
        Shuffle the training data.
        :param X: Training data vectors (matrix n_samples x n_features).
        :param y: Target values vector (shape = [n_samples]).
        :return:  Reshuffled X and y.
        """
        # Generate random sequence of unique numbers in the range 0 to 100
        r = self.rgen.permutation(len(y))
        # Use random sequence as indices to shuffle feature matrix and class
        # label vector.
        return X[r], y[r]

    def _initialise_weights(self, m):
        """
        Initialise weights to small random numbers.
        :param m: Number of features.
        :return:  None
        """
        self.rgen = np.random.RandomState(self.random_state)
        self.w_ = self.rgen.normal(loc=0.0, scale=0.01, size=1 + m)
        self.w_initialised = True

    def _update_weights(self, xi, target):
        """
        Apply Adaline learning rule to update the weights.
        :param xi:     i-th feature vector (shape = [n_features]).
        :param target: Target vector.
        :return:       Cost vector.
        """
        output = self.activation(self.net_input(xi))
        error = target - output
        self.w_[1:] += self.eta * xi.dot(error)
        self.w_[0] += self.eta * error
        cost = 0.5 * error ** 2
        return cost

    def net_input(self, X):
        """
        Calculate the net input for the neuron.
        :param X: Features vector (shape = [n_features]).
        :return:  Net input vector.
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def activation(self, X):
        """
        Compute linear activation.  In this case implemented as the identity
        function.
        :param X: Net input vector (shape = [n_features]).
        :return:  Activated vector.
        """
        return X

    def predict(self, X):
        """
        Return class label after unit step.

        Applies the decision (threshold) function to the net input to return the
        class label 1 if net input > 0, or -1 otherwise.
        :param X: Standardised features vector (shape = [n_features]).
        :return:  1 or -1; class label after unit step.
        """
        return np.where(self.activation(self.net_input(X)) >= 0.0, 1, -1)
