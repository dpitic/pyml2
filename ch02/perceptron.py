"""
Single layer Perceptron model of an artificial neuron.
"""
import numpy as np


class Perceptron(object):
    """
    @brief      Perceptron classifier.
    @details    Perceptron classifier that can learn from data using the fit()
                method, and make predictions via the predict() method.  As a
                convention, an underscore (_) is appended to attributes that are
                not being initialised upon object initialisation (they are
                initialised in other methods).
    """

    def __init__(self, eta=0.01, n_iter=50, random_state=1):
        """
        @param      self          The object
        @param      eta:          float; Learning rate (between 0.0 and 1.0)
        @param      n_iter:       int; Number of epochs (passes over the
                                  training data set).
        @param      random_state: int; Random number generator seed for random
                                  weight initialisation.
        """
        self.eta = eta
        self.n_iter = n_iter
        self.random_state = random_state

    def fit(self, X, y):
        """
        @brief      Fit training data.
        @details    This method initialises the weights (w_) to a vector of real
                    numbers of m+1 dimensions where m is the number of features
                    in the data set plus 1 for the first element in the vector
                    that represents the bias unit.  w_[0] represents the bias
                    unit.

        @param      self  The object
        @param      X:    array-like, shape = [n_samples, n_features]
                          Training vectors matrix, where n_samples is the number
                          of samples and n_features is the number of features.
        @param      y:    array-like, shape = [n_samples]
                          Target values vector.

        @return     self: object
        """
        rgen = np.random.RandomState(self.random_state)
        # 1-d array of weights after fitting.  Contains small random numbers
        # drawn from a normal distribution with standard deviation 0.01.  The
        # weights are not initialised to zero because the learning rate (eta)
        # only has effect on the classification outcome if the weights are
        # initialised to non-zero values.  If all the weights were initialised
        # to zero, the learning rate parameter only affects the scale of the
        # weight vector, not the direction.
        self.w_ = rgen.normal(loc=0.0, scale=0.01, size=1 + X.shape[1])
        # list containing number of misclassifications (updates) in each epoch
        self.errors_ = []

        # Loop over individual samples in the training set and update the
        # weights according to the perceptron learning rule.
        for _ in range(self.n_iter):
            errors = 0
            for xi, target in zip(X, y):
                # Predict the class label for the weight update.
                update = self.eta * (target - self.predict(xi))
                self.w_[1:] += update * xi
                self.w_[0] += update  # bias unit
                errors += int(update != 0.0)
            # Collect the number of misclassifications during each epoch to
            # later analyse the performance of the perceptron during training.
            self.errors_.append(errors)
        return self

    def net_input(self, X):
        """
        @brief      Calculate net input for the perceptron.
        @details    Calculates z, the net input, which is the dot product of
                    the weights and the inputs.

        @param      self  The object
        @param      X:    array-like, shape = [n_features]
                          Features vector, where n_features is the number of
                          features.

        @return     Net input for the perceptron.
        """
        return np.dot(X, self.w_[1:]) + self.w_[0]

    def predict(self, X):
        """
        @brief      Return class label after unit step.
        @details    Applies the decision (threshold) function to the net input
                    to return the class label 1 if net input > 0, or -1
                    otherwise.

        @param      self  The object
        @param      X:    array-like, shape = [n_features]
                          Features vector, where n_features is the number of
                          features.

        @return     Class label 1 or -1 after unit step.
        """
        return np.where(self.net_input(X) >= 0.0, 1, -1)
