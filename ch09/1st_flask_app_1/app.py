"""
This is a demonstration of a very simple Flask web application.  It consists
of a simple web page with a form field that lets the user enter a name.  After
submitting the name to the web application, it will render it on a new page.
The web application directory tree looks like this

1st_flask_app_1/
    app.py
    static/
        style.css
    templates/
        _formhelpers.html
        first_app.html
        hello.html

This module is app.py, which contains the main code that will be executed by
the Python interpreter to run the Flask web application.  The templates
directory is the directory in which Flask will look for static HTML files for
rendering in the web browser.

Adding form validation and rendering.
"""
from flask import Flask, render_template, request
from wtforms import Form, TextAreaField, validators

app = Flask(__name__)


class HelloForm(Form):
    """Simple form with input text field.

    Simple form with a single text field that requires data in order to be
    valid.
    """
    sayHello = TextAreaField('', [validators.DataRequired()])


@app.route('/')
def index():
    """Render an input form on the landing page.

    :return: Rendered landing page with input form.
    """
    form = HelloForm(request.form)
    return render_template('first_app.html', form=form)


@app.route('/hello', methods=['POST'])
def hello():
    """Render the hello.html page after validating the HTML form.

    :return: Render the hello.html template if form validates, otherwise render
             the landing page.
    """
    form = HelloForm(request.form)
    if request.method == 'POST' and form.validate():
        name = request.form['sayHello']
        return render_template('hello.html', name=name)
    return render_template('first_app.html', form=form)


if __name__ == '__main__':
    app.run(debug=True)
