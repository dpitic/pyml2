"""
Utility module providing a tokenizer function and a HashingVectorizer object.
"""
import os
import pickle
import re

import numpy as np
from sklearn.feature_extraction.text import HashingVectorizer

# Get current directory (web application directory)
cur_dir = os.path.dirname(__file__)
# Stopwords
stop = pickle.load(
    open(os.path.join(cur_dir, 'pkl_objects', 'stopwords.pkl'), 'rb'))


def tokenizer(text):
    """Remove stop words and tokenize text.

    This preprocessor function removes punctuation marks from the input text
    except for emoticon characters.  It cleans unprocessed text data and
    separates it into word tokens while removing stop words.

    :param str text: Input text string.
    :return: Tokenized and cleaned text with stop words removed.
    """
    # Remove all of the HTML markup
    text = re.sub('<[^>]*>', '', text)
    # Find all emoticons
    emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text.lower())
    # Remove all non-word characters from the text string and convert to
    # lowercase characters, add emoticons to end of preprocessed document
    # string, and remove the nose character '-' from the emoticons for
    # consistency
    text = (re.sub('[\W]+', ' ', text.lower()) +
            ' '.join(emoticons).replace('-', ''))
    tokenized = [w for w in text.split() if w not in stop]
    return tokenized


# Unfortunately, CountVectorizer cannot be used for out-of-core learning
# because it requires holding the complete vocabulary in memory.  Also,
# TfidfVectorizer needs to keep all the feature vectors of the training
# data set in memory to calculate the inverse document frequencies.
# Another useful vectorizer for text processing implemented in scikit-learn
# is HashingVectorizer, which is data-independent and makes use of the
# hashing trick via the 32-bit MurmurHash3 function.  Initialise a
# HashingVectorizer object with the preprocesser tokenizer function, and
# set a large number of features to reduce the chance of causing hash
# collisions.
vect = HashingVectorizer(decode_error='ignore',
                         n_features=2 ** 21,
                         preprocessor=None,
                         tokenizer=tokenizer)


def main():
    """Check that the objects were serialised correctly."""
    # Load classifier
    print('Loading classifier ...')
    clf = pickle.load(open(os.path.join('pkl_objects', 'classifier.pkl'),
                           'rb'))
    label = {0: 'negative', 1: 'positive'}
    example = ['I love this movie']
    X = vect.transform(example)
    print('Example prediction: %s\nProbability: %.2f%%' %
          (label[clf.predict(X)[0]],
           np.max(clf.predict_proba(X)) * 100))


if __name__ == '__main__':
    main()
