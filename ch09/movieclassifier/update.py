"""
This module is used to locally update the classifier object using the data in
the review database.

The predictive model is updated automatically whenever a user provides feedback
about the classification.  However, this only works in the context of a running
web application.  The classifier object will be reinitialised from the pickled
file if the web application crashes or is restarted.

One option to apply the updates permanently would be to pickle the classifier
object after each user update.  However, this would become computationally very
inefficient with a growing number ofusers, and could corrupt the pickle file if
users provide feedback simultaneously.

An alternative solution is to update the predictive model from the feedback
data that is stored in the SQLite database.  One option would be to download
database from the web server, update the classifier object locally on a
computer, and upload the new pickle file to the web server.  This is the
purpose of this module.
"""

import os
import pickle
import sqlite3

import numpy as np

# Import HashingVectorizer from local directory
from vectorizer import vect


def update_model(db_path, model, batch_size=10000):
    """Update the predictive model from the database file.

    This function reads movie review entries from the movie review SQLite
    database in batches of 10,000 entries at a time by default, unless the
    database contains fewer entries.

    :param str db_path: Path to the SQLite movie review database.
    :param obj model: Classifier object.
    :param int batch_size: Number of review results to use per update.
    :return: Updated model.
    """
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute('SELECT * from review_db')

    results = c.fetchmany(batch_size)
    while results:
        data = np.array(results)
        X = data[:, 0]
        y = data[:, 1].astype(int)

        classes = np.array([0, 1])
        X_train = vect.transform(X)
        model.partial_fit(X_train, y, classes=classes)
        results = c.fetchmany(batch_size)

    conn.close()
    return model


cur_dir = os.path.dirname(__file__)

# Get classifier from pickle file in pkl_objects subdirectory
print('Loading predictive model ...')
clf = pickle.load(open(os.path.join(cur_dir, 'pkl_objects', 'classifier.pkl'),
                       'rb'))
# Get database path from current directory
db = os.path.join(cur_dir, 'reviews.sqlite')

# Update predictive model using information in movie reviews database
print('Updating predictive model with movie reviews in database ...')
clf = update_model(db_path=db, model=clf, batch_size=10000)

# Update the pickled classifier object
print('Saving updated predictive model ...')
pickle.dump(clf, open(os.path.join(cur_dir, 'pkl_objects', 'classifier.pkl'),
                      'wb'), protocol=pickle.HIGHEST_PROTOCOL)
