"""
Sequential feature selection algorithm.
"""

from itertools import combinations

import numpy as np
from sklearn.base import clone
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split


class SBS():
    """
    Sequential Backward Selection (SBS).

    This class implements the classic sequential feature selection algorithm
    SBS, which aims to reduce the dimensionality of the initial feature
    subspace with a minimum decay in performance of the classifier to
    improve upon computational efficiency.  In certain cases, SBS can even
    improve the predictive power of the model if a model suffers from
    overfitting.

    The idea behind the SBS algorithm is to sequentially remove features from
    the full feature subset until the new feature subspace contains the
    desired number of features.
    """

    def __init__(self,
                 estimator,
                 k_features,
                 scoring=accuracy_score,
                 test_size=0.25,
                 random_state=1):
        """
        @brief      Initialise an SBS object.

        @details    Initialise a Sequential Backward Selection (SBS) object.

        @param      estimator: the estimator classifier.
        @param      k_features: desired number of features to return.
        @param      scoring: used to evaluate the performance of the model.
        @param      test_size: percentage of the data set used for test.
        @param      random_state:
        """
        self.scoring = scoring
        self.estimator = clone(estimator)
        self.k_features = k_features
        self.test_size = test_size
        self.random_state = random_state

    def fit(self, X, y):
        """
        @brief      Fit the algorithm model to the data set.

        @details    Fit the model to the data set.  This method splits the
                    data set into training and test data sets.

        @param      X: feature data set matrix.
        @param      y: class labels.

        @return     Fitted SBS object.
        """
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=self.test_size, random_state=self.random_state)

        dim = X_train.shape[1]
        self.indices_ = tuple(range(dim))
        self.subsets_ = [self.indices_]
        score = self._calc_score(X_train, y_train, X_test, y_test,
                                 self.indices_)

        self.scores_ = [score]

        while dim > self.k_features:
            scores = []
            subsets = []

            for p in combinations(self.indices_, r=dim - 1):
                score = self._calc_score(X_train, y_train, X_test, y_test, p)
                scores.append(score)
                subsets.append(p)

            best = np.argmax(scores)
            self.indices_ = subsets[best]
            self.subsets_.append(self.indices_)
            dim -= 1

            self.scores_.append(scores[best])

        self.k_score_ = self.scores_[-1]

        return self

    def transform(self, X):
        """
        @brief      Transform the features.

        @details    Transform the feature matrix into a data array with the
                    selected feature columns.

        @param      X: feature matrix.

        @return     Transformed feature matrix.
        """
        return X[:, self.indices_]

    def _calc_score(self, X_train, y_train, X_test, y_test, indices):
        """
        @brief      Calculate the model accuracy score.

        @details    Calculate the model accuracy score.

        @param      X_train: training feature matrix.
        @param      y_train: training class labels.
        @param      X_test: test feature matrix.
        @param      y_test: test class labels.
        @param      indices: selected indices.

        @return     Model score.
        """
        self.estimator.fit(X_train[:, indices], y_train)
        y_pred = self.estimator.predict(X_test[:, indices])
        score = self.scoring(y_test, y_pred)
        return score
