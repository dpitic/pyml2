"""
Partitioning a data set into separate training and test sets.

This module uses the Wine data set to explore different techniques for feature
selection to reduce the dimensionality of a data set.  The Wine data set
consists of 178 wine samples with 13 features describing their different
chemical properties; it is hosted on the UCI machine learning repository:
https://archive.ics.uci.edu/ml/datasets/Wine
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler, StandardScaler

from sbs import SBS


def main():
    # Use pandas to directly read the open source Wine data set.  The samples
    # belong to one of three different classes, 1, 2, and 3, which refer to
    # the three differenty types of grape grown in the same region in Italy,
    # but derived from different wint cultivars.
    df_wine = pd.read_csv(
        'https://archive.ics.uci.edu/'
        'ml/machine-learning-databases/'
        'wine/wine.data',
        header=None)
    df_wine.columns = [
        'Class label', 'Alcohol', 'Malic acid', 'Ash', 'Alcalinity of ash',
        'Magnesium', 'Total phenols', 'Flavanoids', 'Nonflavanoid phenols',
        'Proanthocyanins', 'Colour intensity', 'Heu',
        'OD280/OD315 of diluted wines', 'Proline'
    ]
    print('Class labels', np.unique(df_wine['Class label']))
    print(df_wine.head())
    # A convenient way to randomly partition this data set into separate test
    # and training data sets is to use the train_test_split() function from
    # scikit-learn model_selection submodule.
    X, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.3, random_state=0, stratify=y)
    # Bringing features onto the same scale
    # Feature scaling is a crucial step in the preprocessing pipeline.
    # Decision trees and random forests are two of the very few machine
    # learning algorithms where feature scaling is not needed; those algorithms
    # are scale invariant.  However, the majority of machinelearning and
    # optimisation algorithms behave much better if features are on the same
    # scale.  There are two common approaches to bringing features onto the
    # same scale: normalisation and standardisation.  Most often, normalisation
    # refers to rescaling the features to a range of [0,1], which is a special
    # case of min-max scaling.  To normalise the data, we can simply apply the
    # min-max scaling to each feature column.
    mms = MinMaxScaler()
    X_train_norm = mms.fit_transform(X_train)
    X_test_norm = mms.fit_transform(X_test)
    # Although normalisation via min-max scaling is commonly used when we need
    # values in a bounded interval, standardisation can be more practical for
    # many machine learning algorithms, especially for optimisation algorithms
    # such as gradient descent.  The reason is that many linear models such as
    # logistic regression and SVM initialize the weights to 0 or small random
    # values close to 0.  Using standardisation, we centre the feature columns
    # at mean 0 with standard deviation 1, so that the feature columns have
    # the same parameters as a standard normal distribution (zero mean and
    # unit variance), which makes it easier to learn the weights.
    # Additionally, standardisation maintains useful information about
    # outliers and makes the algorithm less sensitive to them in contrast to
    # min-max scaling, which scales the data to a limited range of values.
    # The following code performs standardisation and normalisation on an
    # array containing the integers in the range [0,5].
    ex = np.array([0, 1, 2, 3, 4, 5])
    print('Standardised:', (ex - ex.mean()) / ex.std())
    print('Normalised:  ', (ex - ex.min()) / (ex.max() - ex.min()))
    # Scikit-learn implements a class for standardisation.  It is important
    # that we fit the StandardScaler class only once on the training data, and
    # use those parameters to transform the test set or any new data point.
    stdc = StandardScaler()
    X_train_std = stdc.fit_transform(X_train)
    X_test_std = stdc.transform(X_test)
    # Selecting meaningful features.
    # If we notice that a model perform much better on a training data set
    # than on the test data set, this observation is a strong indication of
    # overfitting.  This means the model fits the parameters too closely with
    # regard to the particular observations in the training data set, but does
    # not genralise well to new data, and the model is said to have high
    # variance.  The reason for overfitting is that the model is too complex
    # for the given training data.  Common solutions to reduce the
    # generalization error are:
    # 1. Collect more training data.
    # 2. Introduce a penalty for complexity via regularization.
    # 3. Choose a simpler model with fewer parameters.
    # 4. Reduce the dimensionality of the data.
    # This module explors common ways to reduce overfitting by regularization
    # and dimensionality reduction via feature selection, which leads to
    # simpler models by requiring fewer parameters to be fitted to the data.
    lr = LogisticRegression(
        penalty='l1', C=1.0, solver='liblinear', multi_class='ovr')
    # Note that C=1.0 is the default.  You can increase or decrease it to make
    # the regularization effect stronger or weaker, respectively.
    lr.fit(X_train_std, y_train)
    print('Training accuracy:', lr.score(X_train_std, y_train))
    print('Test accuracy:', lr.score(X_test_std, y_test))
    # Both training and test accuracies (both 100%) indicate that our model
    # does a perfect job on both data sets.  Accessing the intercept terms via
    # the lr.intercept_ attribute shows the array returns three values.
    print(lr.intercept_)
    # Since we fit the LogisticRegression object on a multiclass data set, it
    # uses the One-versus-Rest (OvR) approach by default, where the first
    # intercept belongs to the model that fits class 1 versus class 2 and 3,
    # the second value is the intercept of the model that fits class 2 versus
    # class 1 and 3, and the third value is the intercept of the model that
    # fits class 3 versus class 1 and 2.
    print(lr.coef_)
    # The weight array that we accessed via the lr.coef_ attribute contains
    # three rows of weight coefficients, one weight vector for each class.
    # Each row consists of 13 weights where each weight is multiplied by the
    # respective feature in the 13-dimensional Wine data set to calculate the
    # net input z.  As a result of L1 regularization, which serves as a method
    # for feature selection, we just trained a mdoel that is robust to the
    # potentially irrelevant features in the data set.  Next we will vary the
    # regularization strength and plot the regularization path - the weight
    # coefficients of the different features for different regularizaiton
    # strengths.
    fig = plt.figure()
    ax = plt.subplot(111)
    colours = [
        'blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'pink',
        'lightgreen', 'lightblue', 'gray', 'indigo', 'orange'
    ]
    weights, params = [], []
    for c in np.arange(-4., 6.):
        lr = LogisticRegression(
            penalty='l1',
            C=10.**c,
            random_state=0,
            solver='liblinear',
            multi_class='ovr')
        lr.fit(X_train_std, y_train)
        weights.append(lr.coef_[1])
        params.append(10**c)
    weights = np.array(weights)
    for column, colour in zip(range(weights.shape[1]), colours):
        plt.plot(
            params,
            weights[:, column],
            label=df_wine.columns[column + 1],
            color=colour)
    plt.axhline(0, color='black', linestyle='--', linewidth=3)
    plt.xlim([10**(-5), 10**5])
    plt.ylabel('weight coefficient')
    plt.xlabel('C')
    plt.xscale('log')
    plt.legend(loc='upper left')
    ax.legend(
        loc='upper center', bbox_to_anchor=(1.38, 1.03), ncol=1, fancybox=True)
    # The resulting plot provides us with further insight into the behaviour
    # of L1 regularization.  As can be seen, all feature weights will be zero
    # if we penalize the model with a string regularization parameter (C<0.1);
    # C is the inverse of the regularization parameter lambda.
    plt.show()
    # Sequential feature selection algorithms
    # An alternative way to reduce the complexity of the model and avoid
    # overfitting is dimensionality reduction via feature selection, which is
    # especially useful for unregularized models.  There are two main
    # categories of dimensionality reduction techniques: feature selection
    # and feature extraction.  Via feature selection, we select a subset of
    # the original features, whereas in feature extraction, we derive
    # information from the feature set to construct a new feature subspace.
    # This section looks at a classic family of feature selection algorithms.
    # Sequential feature selection algorithms are a family of greedy search
    # algorithms that are used to reduce an initial d-dimensional feature
    # space to a k-dimensional feature subspace, where k<d.  The motivation
    # behind feature selection algorithms is to automatically select a subset
    # of features that are most relevant to the problem, to improve
    # computational efficiency or reduce the generalization error of the model
    # by removing irrelevant features or noise, which can be useful for
    # algorithms that don't support regularizaiton.  A classic sequential
    # feature selection algorithm is Sequential Backward Selection (SBS),
    # aims to reduce the dimensionality of the initial feature subspace with a
    # minimum decay in performance of the classifier to improve upon
    # computational efficiency.  In certain cases, SBS can even improve the
    # predictive power of the model if a model suffers from overfitting.
    knn = KNeighborsClassifier(n_neighbors=5)
    sbs = SBS(knn, k_features=1)
    sbs.fit(X_train_std, y_train)
    # Plot the classification accuracy of the KNN classifier that was
    # calculated on the validation data set.
    k_feat = [len(k) for k in sbs.subsets_]
    plt.plot(k_feat, sbs.scores_, marker='o')
    plt.ylim([0.7, 1.02])
    plt.ylabel('Accuracy')
    plt.xlabel('Number of features')
    plt.grid()
    plt.show()
    # The figure shows the accuracy of the KNN classifier improved on the
    # validation data set as the number of features were reduced, which is
    # likely due to a decrease in the effects of dimensionality.  It also
    # shows that the classifier achieved 100% accuracy for
    # k={3,7,8,9,10,11,12}.  Next we will see what the smallest feature subset
    # (k=3) that yielded such a good performance on the validation data set
    # looks like.
    k3 = list(sbs.subsets_[10])
    print(df_wine.columns[1:][k3])
    # Evaluate the performance of the KNN classifier on the original test set.
    knn.fit(X_train_std, y_train)
    print('Training accuracy:', knn.score(X_train_std, y_train))
    print('Test accuracy:', knn.score(X_test_std, y_test))
    # Use the selected three feature subset to see the KNN performance.
    knn.fit(X_train_std[:, k3], y_train)
    print('Training accuracy:', knn.score(X_train_std[:, k3], y_train))
    print('Test accuracy:', knn.score(X_test_std[:, k3], y_test))
    # Random forests provide another useful approach to select relevant
    # features from a data set.  Using random forests, we can measure the
    # feature importance as the average impurity decrease computed from all
    # decision trees in the forest, without making any assumptions about
    # whether the data is linearly separable or not.  The random forest
    # implementation in scikit-learn already collects the feature importance
    # values so they can be accessed via the feature_importances_ attribute
    # after fitting a RandomForestClassifier.  The following code will now
    # train a forest of 500 trees on the Wine data set and rank the 13
    # features by their respective importance measures.  Tree based models
    # don't require standardised or normalised features.
    feat_labels = df_wine.columns[1:]
    forest = RandomForestClassifier(n_estimators=500, random_state=1)
    forest.fit(X_train, y_train)
    importances = forest.feature_importances_
    indices = np.argsort(importances)[::-1]
    for f in range(X_train.shape[1]):
        print("%2d) %-*s %f" % (f + 1, 30, feat_labels[indices[f]],
                                importances[indices[f]]))
    plt.title('Feature Importance')
    plt.bar(range(X_train.shape[1]), importances[indices], align='center')
    plt.xticks(range(X_train.shape[1]), feat_labels[indices], rotation=90)
    plt.xlim([-1, X_train.shape[1]])
    plt.tight_layout()
    plt.show()
    # Scikit-learn also implements a SelectFromModel object that selects
    # features based on a user specified threshold after model fitting, which
    # is useful if the RandomForestClassifier is used as a feature selector
    # and intermediate step in scikit-learn Pipeline object, which enables
    # connection of different preprocessing steps with an estimator.  For
    # example, the threshold could be set to 0.1 to reduce the data set to
    # the five most important features using the following code.
    sfm = SelectFromModel(forest, threshold=0.1, prefit=True)
    X_Selected = sfm.transform(X_train)
    print('Number of features that meet this threshold criterion:',
          X_Selected.shape[1])
    for f in range(X_Selected.shape[1]):
        print("%2d) %-*s %f" % (f + 1, 30, feat_labels[indices[f]],
                                importances[indices[f]]))


if __name__ == '__main__':
    main()
