"""
This module is used to demonstrate preprocessing data sets.
"""
from io import StringIO

import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer, LabelEncoder, OneHotEncoder


def main():
    # Create some sample data in CSV format
    csv_data = '''A,B,C,D
    1.0,2.0, 3.0, 4.0
    5.0,6.0,,8.0
    10.0,11.0,12.0,'''
    # Read sample CSV data into pandas DataFrame
    df = pd.read_csv(StringIO(csv_data))
    print(df)
    # The isnull() method returns a DataFrame with boolean values that indicate
    # whether a cell contains a numeric value (False) or if data is missing
    # (True).  The sum() method returns the number of missing values per
    # column.
    print(df.isnull().sum())
    # Although scikit-learn was developed for working with NumPy arrays, it
    # can sometimes be more convenient to preprocess data using pandas
    # DataFrame.  We can access the underlying NumPy array of a DataFrame via
    # the values attribute before we feed it into a scikit-learn estimator.
    print(df.values)
    # One of the easiest ways to deal with missing data is to simply remove the
    # corresponding features (columns) or samples (rows) from the data set;
    # rows with missing values can be dropped using the dropna() method
    print(df.dropna(axis=0))
    # Columns that have at least one NaN in any row can be dropped by setting
    # axis=1
    print(df.dropna(axis=1))
    # Only drop rows where all columns are NaN (returns the whole array here
    # since we don't have a row with all values NaN)
    print(df.dropna(how='all'))
    # Drop rows that have less than 4 real values
    print(df.dropna(thresh=4))
    # Only drop rows where NaN appear in specific columns (here: 'C')
    print(df.dropna(subset=['C']))
    # Imputing missing values
    # One of the most common interpolation techniques is mean imputation, where
    # the missing value is simply replaced with the mean value of the entire
    # feature column (axis=0).  The Imputer class from scikit-learn provides a
    # convenient way to achieve this.
    imr = Imputer(missing_values='NaN', strategy='mean', axis=0)
    imr = imr.fit(df.values)
    imputed_data = imr.transform(df.values)
    print(imputed_data)
    # Handling categorical data
    # Nominal and ordinal features.  Ordinal features are categorical values
    # that can be sorted or ordered.  Nominal features are categorial values
    # that don't imply any order.  Create an example dataset.
    df = pd.DataFrame([['green', 'M', 10.1, 'class1'],
                       ['red', 'L', 13.5, 'class2'],
                       ['blue', 'XL', 15.3, 'class1']])
    df.columns = ['colour', 'size', 'price', 'classlabel']
    print(df)
    # Mapping ordinal features
    # To ensure the learning algorithm interprets the ordinal features
    # correctly, the categorical string values must be converted into integers.
    # Arbitrarily define integer values for size category.
    size_mapping = {'XL': 3, 'L': 2, 'M': 1}
    df['size'] = df['size'].map(size_mapping)
    print(df)
    # To transform the integer size values back to the original string
    # representations, define a reverse mapping dictionary.
    inv_size_mapping = {v: k for k, v, in size_mapping.items()}
    print(df['size'].map(inv_size_mapping))
    # Encoding class labels
    # Many machine learning libraries require class labels to be encoded as
    # integer values.  Although most estimators for classification in
    # scikit-learn convert class labels to integers internally, it is
    # considered good practice to provide class labels as integer arrays to
    # avoid technical glitches.  Class labels are not ordinal, so it doesn't
    # matter which integer numbers are assigned to a particular string label.
    # Thus class labels can be simply enumerated starting at 0.
    class_mapping = {
        label: idx
        for idx, label in enumerate(np.unique(df['classlabel']))
    }
    print(class_mapping)
    # The mapping dictionary can be used to transform the class labels into
    # integers.
    df['classlabel'] = df['classlabel'].map(class_mapping)
    print(df)
    # The key-value pairs in the mapping dictionary can be reversed to map the
    # converted class labels back to the original string representation.
    inv_class_mapping = {v: k for k, v in class_mapping.items()}
    df['classlabel'] = df['classlabel'].map(inv_class_mapping)
    print(df)
    # Alternatively, there is a convenient LabelEncoder class in scikit-learn
    # which is used to achieve this.
    class_le = LabelEncoder()
    y = class_le.fit_transform(df['classlabel'].values)
    print(y)
    # The inverse_transform() method can be used to transform the integer class
    # labels back to their original string representations.
    print(class_le.inverse_transform(y))
    # Performing one-hot encoding on nominal features to transform the colour
    # column of the data set.
    X = df[['colour', 'size', 'price']].values
    colour_le = LabelEncoder()
    X[:, 0] = colour_le.fit_transform(X[:, 0])
    print(X)
    # If this array is used as input to the classifier, the learning algorithm
    # will assume that green is larger than blue, and red is larger than green.
    # Although this assumption is incorrect, the algorithm could still produce
    # useful results; however, those results would not be optimal.  A common
    # workaround for this problem is to use a technique called one-hot encoding.
    # The idea behind this approach is to create a new dummy feature for each
    # unique value in the nominal feature column.  Here we would convert the
    # colour feature into three new features: blue, green and red.  Binary
    # values can then be used to indicated the particular colour of a sample;
    # for example, a blue sample can be encoded as blue=1, green=0, red=0.  To
    # perform this transformation, we can use the OneHotEncoder that is
    # implemented in scikit-learn.preprocessing module.
    ohe = OneHotEncoder(categorical_features=[0])
    print(ohe.fit_transform(X).toarray())
    # An even more convenient way to create those dummy features via one-hot
    # encoding is to use the get_dummies() method implemented in pandas.  When
    # applied to a DataFrame, it will only convert string columns and leave all
    # other columns unchanged.
    print(pd.get_dummies(df[['price', 'colour', 'size']]))
    # Using one-hot encoding data sets introduces multicolinearity, which may
    # be an issue for certain methods (e.g. those that require matrix
    # inversion).  If features are highly correlated, matrices are
    # computationally difficult to invert, which can lead to numerically
    # unstable estimates.  To reduce the correlation among variables, we can
    # simply remove one feature column from the one-hot encoded array.  This
    # does not lose any important information; for example, if we remove the
    # column colour_blue, the feature information is still preserved since
    # colour_green=0 and colour_red=0 implies the observation must be blue.
    # Passing a drop_first=True argument to get_dummies() drops the first
    # column.
    print(pd.get_dummies(df[['price', 'colour', 'size']], drop_first=True))
    # The OneHotEncoder does not have a parameter for column removal, but we
    # can simply slice the one-hot encoded NumPy array.
    ohe = OneHotEncoder(categorical_features=[0])
    print(ohe.fit_transform(X).toarray()[:, 1:])


if __name__ == '__main__':
    main()
