"""
This module demonstrates various clustering analysis.
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib import cm
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import pdist, squareform
from sklearn.cluster import DBSCAN, AgglomerativeClustering, KMeans
from sklearn.datasets import make_blobs, make_moons
from sklearn.metrics import silhouette_samples


def main():
    # Generate random 2D data set
    X, y = make_blobs(
        n_samples=150,
        n_features=2,
        centers=3,
        cluster_std=0.5,
        shuffle=True,
        random_state=0)
    plt.scatter(
        X[:, 0], X[:, 1], c='white', marker='o', edgecolor='black', s=50)
    plt.grid()
    plt.title('Blobs')
    plt.show()

    # Apply (classic) k-means algorithm to the data set
    km = KMeans(
        n_clusters=3,
        init='random',
        n_init=10,
        max_iter=300,
        tol=1e-04,
        random_state=0)
    # Predict the cluster labels
    y_km = km.fit_predict(X)
    # Visualise the clusters that k-means identified
    plt.scatter(
        X[y_km == 0, 0],
        X[y_km == 0, 1],
        s=50,
        c='lightgreen',
        marker='s',
        edgecolor='black',
        label='cluster 1')
    plt.scatter(
        X[y_km == 1, 0],
        X[y_km == 1, 1],
        s=50,
        c='orange',
        marker='o',
        edgecolor='black',
        label='cluster 2')
    plt.scatter(
        X[y_km == 2, 0],
        X[y_km == 2, 1],
        s=50,
        c='lightblue',
        marker='v',
        edgecolor='black',
        label='cluster 3')
    plt.scatter(
        km.cluster_centers_[:, 0],
        km.cluster_centers_[:, 1],
        s=250,
        marker='*',
        c='red',
        edgecolor='black',
        label='centroids')
    plt.legend(scatterpoints=1)
    plt.grid()
    plt.title('K-means Clusters')
    plt.show()

    # Using the elbow method to find the optimum number of clusters
    # Print the within-cluster SSE (distortion)
    print('\nDistortion: %.2f' % km.inertia_)
    # Plot the distortion for different values of k to determine the optimum
    # number of clusters (k).  The plot shows the elbow at k=3, which is a
    # good choice for this data set.
    distortions = []
    for i in range(1, 11):
        km = KMeans(
            n_clusters=i,
            init='k-means++',
            n_init=10,
            max_iter=300,
            random_state=0)
        km.fit(X)
        distortions.append(km.inertia_)
    plt.plot(range(1, 11), distortions, marker='o')
    plt.xlabel('Number of clusters')
    plt.ylabel('Distortion')
    plt.title('K-means++ Distortion')
    plt.show()

    # Quantifying the quality of clustering via silhouette plots
    km = KMeans(
        n_clusters=3,
        init='k-means++',
        n_init=10,
        max_iter=300,
        tol=1e-04,
        random_state=0)
    y_km = km.fit_predict(X)

    cluster_labels = np.unique(y_km)
    n_clusters = cluster_labels.shape[0]
    silhouette_vals = silhouette_samples(X, y_km, metric='euclidean')
    y_ax_lower, y_ax_upper = 0, 0
    yticks = []
    for i, c in enumerate(cluster_labels):
        c_silhouette_vals = silhouette_vals[y_km == c]
        c_silhouette_vals.sort()
        y_ax_upper += len(c_silhouette_vals)
        colour = cm.jet(float(i) / n_clusters)
        plt.barh(
            range(y_ax_lower, y_ax_upper),
            c_silhouette_vals,
            height=1.0,
            edgecolor='none',
            color=colour)
        yticks.append((y_ax_lower + y_ax_upper) / 2.)
        y_ax_lower += len(c_silhouette_vals)

    silhouette_avg = np.mean(silhouette_vals)
    plt.axvline(silhouette_avg, color='red', linestyle='--')

    plt.yticks(yticks, cluster_labels + 1)
    plt.ylabel('Clusters')
    plt.xlabel('Silhouette coefficient')
    plt.title('Silhouette Plot')
    plt.show()

    # Demonstrate what a silhouette plot looks like for a relatively bad
    # clustering, using k-means with only 2 centroids
    km = KMeans(
        n_clusters=2,
        init='k-means++',
        n_init=10,
        max_iter=300,
        tol=1e-04,
        random_state=0)
    y_km = km.fit_predict(X)

    plt.scatter(
        X[y_km == 0, 0],
        X[y_km == 0, 1],
        s=50,
        c='lightgreen',
        edgecolor='black',
        marker='s',
        label='cluster 1')
    plt.scatter(
        X[y_km == 1, 0],
        X[y_km == 1, 1],
        s=50,
        c='orange',
        edgecolor='black',
        marker='o',
        label='cluster 2')

    plt.scatter(
        km.cluster_centers_[:, 0],
        km.cluster_centers_[:, 1],
        s=250,
        marker='*',
        c='red',
        label='centroids')
    plt.legend()
    plt.grid()
    plt.title('Bad Clustering Example')
    plt.show()

    # Create silhouette plot to evaluate the results
    cluster_labels = np.unique(y_km)
    n_clusters = cluster_labels.shape[0]
    silhouette_vals = silhouette_samples(X, y_km, metric='euclidean')
    y_ax_lower, y_ax_upper = 0, 0
    yticks = []
    for i, c in enumerate(cluster_labels):
        c_silhouette_vals = silhouette_vals[y_km == c]
        c_silhouette_vals.sort()
        y_ax_upper += len(c_silhouette_vals)
        colour = cm.jet(float(i) / n_clusters)
        plt.barh(
            range(y_ax_lower, y_ax_upper),
            c_silhouette_vals,
            height=1.0,
            edgecolor='none',
            color=colour)

        yticks.append((y_ax_lower + y_ax_upper) / 2.)
        y_ax_lower += len(c_silhouette_vals)

    silhouette_avg = np.mean(silhouette_vals)
    plt.axvline(silhouette_avg, color='red', linestyle='--')

    plt.yticks(yticks, cluster_labels + 1)
    plt.ylabel('Cluster')
    plt.xlabel('Silhouette coefficient')
    plt.title('Bad Clustering Example')
    plt.show()

    # Organising clusters as a hierarchical tree
    # Grouping clusters in bottom-up fashion
    # This section focuses on hierarchical complete linkage clustering.
    # Generate some random sample data: the rows represent observations
    # (IDs 0-4), and the columns are the different features (X, Y, Z) of the
    # samples.
    np.random.seed(123)
    variables = ['X', 'Y', 'Z']
    labels = ['ID_0', 'ID_1', 'ID_2', 'ID_3', 'ID_4']
    X = np.random.random_sample([5, 3]) * 10
    df = pd.DataFrame(X, columns=variables, index=labels)
    print('\nHierarchical complete linkage clustering sample data:')
    print(df)

    # Performing hierarchical clustering on a distance matrix
    # Calculate the distance matrix by calculating the Euclidean distance
    # between each pair of sample points in the data set based on the features
    # X, Y and Z.
    row_dist = pd.DataFrame(
        squareform(pdist(df, metric='euclidean')),
        columns=labels,
        index=labels)
    print('\nPair-wise distances:')
    print(row_dist)

    # Apply the complete linkage agglomeration to the cluster using the
    # linkage() function from SciPy's cluster.hierarchy submodule, which
    # returns a linkage matrix.
    # We can either pass a condensed distance matrix (upper triangular) from
    # the pdist() function, or we can pass the original data array and define
    # the metric='euclinean' argument in the linkage.  However, we should not
    # pass the squareform() distance matrix, which would yield different
    # distance values although the overall clustering would be the same.
    # 1. Incorrect approach: Squareform distance matrix.
    row_clusters = linkage(row_dist, method='complete', metric='euclidean')
    lm = pd.DataFrame(
        row_clusters,
        columns=[
            'row label 1', 'row label 2', 'distance', 'no. of items in clust.'
        ],
        index=['cluster %d' % (i + 1) for i in range(row_clusters.shape[0])])
    print('\n1. Incorrect approach: Squareform distance matrix')
    print(lm)
    # 2. Correct approach: Condensed distance matrix
    row_clusters = linkage(pdist(df, metric='euclidean'), method='complete')
    lm = pd.DataFrame(
        row_clusters,
        columns=[
            'row label 1', 'row label 2', 'distance', 'no. of items in clust.'
        ],
        index=['cluster %d' % (i + 1) for i in range(row_clusters.shape[0])])
    print('\n2. Correct approach: Condensed distance matrix')
    print(lm)
    # 3. Correct approach: Input sample matrix
    row_clusters = linkage(df.values, method='complete', metric='euclidean')
    lm = pd.DataFrame(
        row_clusters,
        columns=[
            'row label 1', 'row label 2', 'distance', 'no. of items in clust.'
        ],
        index=['cluster %d' % (i + 1) for i in range(row_clusters.shape[0])])
    print('\n3. Correct approach: Input sample matrix')
    print(lm)
    # Visualise the computed linkage matrix in the form of a dendrogram.
    row_dendr = dendrogram(row_clusters, labels=labels)
    plt.ylabel('Euclidean distance')
    plt.title('Dendrogram')
    plt.show()

    # Attach dendrogram to a heat map
    # Hierarchical clustering dendrograms are often used in combination with
    # a heat map, which enables the representation of the individual values
    # in the sample matrix with a colour code.  This section will attach a
    # dendogram to a heat map plot and order the rows in the heat map
    # correspondingly.
    # 1. Create a new figure object and define the x & y axis positions, width
    # and height of the dendogram via the add_axes attribute.  Rotate the
    # dendogram 90 degrees CCW.
    fig = plt.figure(figsize=(8, 8), facecolor='white')
    axd = fig.add_axes([0.09, 0.1, 0.2, 0.6])
    row_dendr = dendrogram(row_clusters, orientation='left')
    # 2. Reorder the data in the initial DataFrame according to the clustering
    # labels that can be accessed from the dendrogram object, which is
    # essentially a Python dictionary, via the leaves key.
    df_rowclust = df.iloc[row_dendr['leaves'][::-1]]
    # 3. Construct the heat map from the reordered DataFrame and position it
    # next to the dendrogram.
    axm = fig.add_axes([0.23, 0.1, 0.6, 0.6])
    cax = axm.matshow(df_rowclust, interpolation='nearest', cmap='hot_r')
    # 4. Modify the aesthetics of the dendrogram by removing the axis ticks
    # and hiding the axis spines.  Add a colour bar and assign the feature and
    # sample names to the x and y axis tick labels.
    axd.set_xticks([])
    axd.set_yticks([])
    for i in axd.spines.values():
        i.set_visible(False)
    fig.colorbar(cax)
    axm.set_xticklabels([''] + list(df_rowclust.columns))
    axm.set_yticklabels([''] + list(df_rowclust.index))
    plt.title('Heat Map Dendrogram')
    plt.show()

    # Applying agglomerative clustering via scikit-learn
    # The previous section demonstrated how to perform agglomerative
    # hierarchical clustering using SciPy.  Scikit-learn also implements an
    # AgglomerativeClustering class which allows us to choose the number of
    # clusters that we want to return.  This is useful if we want to prune the
    # hierarchical cluster tree.  By setting the n_cluster parameter to 3, we
    # will now cluster the samples into three groups using the same complete
    # linkage approach based on the Euclidean distance metric, as before.
    # This shows the predicted cluster labels and we can see that the first
    # and fifth sample (ID_0 and ID_4) were assigned to one cluster (label 1),
    # and the samples ID_1 and ID_2 were assigned to a second cluster
    # (label 0).  The sample ID_3 was put into its own cluster (label 2).
    ac = AgglomerativeClustering(
        n_clusters=3, affinity='euclidean', linkage='complete')
    labels = ac.fit_predict(X)
    print('\nCluster labels: %s' % labels)
    # Rerun the AgglomerativeClustering using n_clusters=2.  In this pruned
    # clustering hierarchy, label ID_3 was assigned to the same cluster as
    # ID_0 and ID_4, as expected.
    ac = AgglomerativeClustering(
        n_clusters=2, affinity='euclidean', linkage='complete')
    labels = ac.fit_predict(X)
    print('\nCluster labels: %s' % labels)

    # Density-Based Spatial Clustering of Applications with Noise (DBSCAN)
    X, y = make_moons(n_samples=200, noise=0.05, random_state=0)
    plt.scatter(X[:, 0], X[:, 1])
    plt.title('Half-moon data set')
    plt.show()
    # See if k-means and complete linkage clustering can identify the half
    # moon shapes as separate clusters
    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 3))
    km = KMeans(n_clusters=2, random_state=0)
    y_km = km.fit_predict(X)
    ax1.scatter(
        X[y_km == 0, 0],
        X[y_km == 0, 1],
        c='lightblue',
        edgecolor='black',
        marker='o',
        s=40,
        label='cluster 1')
    ax1.scatter(
        X[y_km == 1, 0],
        X[y_km == 1, 1],
        c='red',
        edgecolor='black',
        marker='s',
        s=40,
        label='cluster 2')
    ax1.set_title('K-means clustering')
    ac = AgglomerativeClustering(
        n_clusters=2, affinity='euclidean', linkage='complete')
    y_ac = ac.fit_predict(X)
    ax2.scatter(
        X[y_ac == 0, 0],
        X[y_ac == 0, 1],
        c='lightblue',
        edgecolor='black',
        marker='o',
        s=40,
        label='cluster 1')
    ax2.scatter(
        X[y_ac == 1, 0],
        X[y_ac == 1, 1],
        c='red',
        edgecolor='black',
        marker='s',
        s=40,
        label='cluster 2')
    ax2.set_title('Agglomerative clustering')
    plt.legend()
    plt.show()
    # Apply the DBSCAN algorithm to the data set
    db = DBSCAN(eps=0.2, min_samples=5, metric='euclidean')
    y_db = db.fit_predict(X)
    plt.scatter(
        X[y_db == 0, 0],
        X[y_db == 0, 1],
        c='lightblue',
        edgecolor='black',
        marker='o',
        s=40,
        label='cluster 1')
    plt.scatter(
        X[y_db == 1, 0],
        X[y_db == 1, 1],
        c='red',
        edgecolor='black',
        marker='s',
        s=40,
        label='cluster 2')
    plt.legend()
    plt.title('DBSCAN Algorithm Clustering')
    plt.show()


if __name__ == '__main__':
    main()
