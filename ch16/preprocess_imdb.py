"""Preprocess and prepare the IMDb movie data set.

The IMDb movie data set contains two columns 'review' and 'sentiment', where
'review' contains the text of movie reviews and 'sentiment' contains the 0 or
1 labels.  The text component of the movie reviews are sequences of words;
therefore the idea is to build an RNN model to process the words in each
sequence, and at the end, classify the entire sequence to 0 or 1 classes.

To prepare the data for input to a neural network, it must be encoded into
numeric values.  This is done by finding the unique words in the entire data
set, which can be done by using sets in Python.  It was found that using sets
for finding unique words in such a large data set was not efficient.  A more
efficient method is to use the Counter class from the collections package.
The data preparation is performed by the preprocess_imdb()function.
"""
from collections import Counter
from string import punctuation

import numpy as np
import pandas as pd
import pyprind


def preprocess_imdb(sequence_length=200):
    """Preprocess and prepare the IMDb movie data set.

    This function reads the clean IMDb data set from chapter 8 and prepares it
    for input to a neural network by encoding it into numeric values.  The
    data set was already shuffled.

    :param int sequence_length: Model hyperparameter corresponding to the RNN
    architecture parameter T, representing the word sequence length.
    :return array X_train: Training sequences; first 25000 elements of the
    encoded data set.
    :return array y_train: Training sentiment labels; first 25000 elements of
    the data set.
    :return array X_test: Test sequences; last 25000 elements of the encoded
    data set.
    :return array y_test: Test sentiment labels; last 25000 elements of the
    data set.
    """
    df = pd.read_csv('../ch08/movie_data.csv', encoding='utf8')
    print(df.head(3))

    # Preprocess the data: separate words and count each word's occurrence
    counts = Counter()
    pbar = pyprind.ProgBar(len(df['review']),
                           title='Counting word occurrences')
    # Collect the counts of occurrence of each unique word in the text.  We
    # are only interested in the set of unique words an don't require the word
    # counts, which are created as a side product (in contrast to the
    # bag-of-words model).
    for i, review in enumerate(df['review']):
        text = ''.join([c if c not in punctuation else ' ' + c + ' ' \
                        for c in review]).lower()
        df.loc[i, 'review'] = text
        pbar.update()
        counts.update(text.split())

    # Create a mapping in the form of a dictionary that maps each unique word
    # in the data set to a unique integer number.  This can be used to
    # convert the entire text of a review into a list of numbers.  The unique
    # words are sorted based on their counts, but any arbitrary order can be
    # used without affecting the final results.
    word_counts = sorted(counts, key=counts.get, reverse=True)
    print(word_counts[:5])
    # Dictionary mapping
    word_to_int = {word: ii for ii, word in enumerate(word_counts, 1)}

    mapped_reviews = []
    pbar = pyprind.ProgBar(len(df['review']), title='Map reviews to ints')
    for review in df['review']:
        mapped_reviews.append([word_to_int[word] for word in review.split()])
        pbar.update()

    # So far we have converted sequences of words into sequences of integers.
    # However, there is one issue that still needs to be resolved - the
    # sequences currently have different lengths.  In order to generate input
    # data that is compatible with RNN architecture, we need to ensure that
    # all sequences have the same length.  Define a parameter called
    # sequence_length that will be set to 200.  Sequences that have fewer than
    # 200 words will be left padded with zeros; those that are longer than
    # 200 words are cut such that only the last 200 corresponding words will
    # be used.  The sequence_length parameter represents the T parameter in
    # the RNN architecture.  This will be implemented by creating a matrix of
    # zeros, where each row corresponds to a sequence of size 200.  The index
    # of words in each sequence will be filled from the right-hand side of the
    # matrix.
    sequences = np.zeros((len(mapped_reviews), sequence_length), dtype=int)
    for i, row in enumerate(mapped_reviews):
        review_arr = np.array(row)
        sequences[i, -len(row):] = review_arr[-sequence_length:]

    # Split the data set into separate training and test data sets.  Since
    # the data set was already shuffled, we can simply take the first half
    # of the data set for training and the second half for testing.
    X_train = sequences[:25000, :]
    y_train = df.loc[:25000, 'sentiment'].values
    X_test = sequences[25000:, :]
    y_test = df.loc[25000:, 'sentiment'].values

    # The n_words parameter for the RNN must be set equal to the number of
    # unique words (plus 1 since we use zeros to fill sequences whose size is
    # less than 200) and it's used while creating the embedded layer.
    n_words = max(list(word_to_int.values())) + 1

    return X_train, y_train, X_test, y_test, n_words


def create_batch_generator(x, y=None, batch_size=64):
    """Generate minibatches.

    This helper function breaks a given data set (which could be a training set
    or test set) into chunks and returns a generator to iterate through these
    chunks (also known as minibatches).
    """
    n_batches = len(x) // batch_size
    x = x[:n_batches * batch_size]
    if y is not None:
        y = y[:n_batches * batch_size]
    for ii in range(0, len(x), batch_size):
        if y is not None:
            yield x[ii:ii + batch_size], y[ii:ii + batch_size]
        else:
            yield x[ii:ii + batch_size]


def main():
    # Test the preprocessing function using default parameters
    X_train, y_train, X_test, y_test, n_words = preprocess_imdb()
    print('X_train\n', X_train.shape)
    print('y_train\n', y_train.shape)
    print('X_test\n', X_test.shape)
    print('y_test\n', y_test.shape)
    print('n_words =', n_words)


if __name__ == '__main__':
    main()
