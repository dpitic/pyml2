"""Implementation of RNN model to perform sentiment analysis."""
import os

import numpy as np
import tensorflow as tf

import preprocess_imdb as ppimdb


class SentimentRNN:
    """Implementation of an RNN model for performing sentiment analysis."""

    def __init__(self,
                 n_words,
                 seq_len=200,
                 lstm_size=256,
                 num_layers=1,
                 batch_size=64,
                 learning_rate=0.0001,
                 embed_size=200):
        """
        Constructor used to set model parameters and create a computation
        graph.  Calls the build() method to build the RNN model.
        """
        # The n_words parameter must be set equal to the number of unique words
        # (plus 1, since we use zero to fill sequences whose size is less than
        # 200) and it's used while creating the embedding layer along with the
        # embeded_size hyperparameter.
        self.n_words = n_words
        # The seq_len variable must be set according to the length of the
        # sequences that were created in the preprocessing stage.
        self.seq_len = seq_len
        self.lstm_size = lstm_size  # number of hidden units
        self.num_layers = num_layers
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.embed_size = embed_size

        self.g = tf.Graph()
        with self.g.as_default():
            tf.compat.v1.set_random_seed(123)
            self.build()
            self.saver = tf.compat.v1.train.Saver()
            self.init_op = tf.compat.v1.global_variables_initializer()

    def build(self):
        """Build the RNN model."""
        # Define placeholders for feeding the input data
        tf_x = tf.compat.v1.placeholder(tf.int32,
                                        shape=(self.batch_size, self.seq_len),
                                        name='tf_x')
        tf_y = tf.compat.v1.placeholder(tf.float32,
                                        shape=(self.batch_size),
                                        name='tf_y')
        tf_keepprob = tf.compat.v1.placeholder(tf.float32, name='tf_keepprob')

        # Create the embedding layer
        # The data preparation step generated sequences of the same length.
        # The elements of these sequences were integer numbers that
        # corresponded to the indices of unique words.  These word indices can
        # be converted into input features in several different ways.  One
        # naive way is to apply one-hot encoding to convert indices into
        # vectors of zeros and ones.  Each word will be mapped to a vector
        # whose size is the number of unique words in the entire data set.
        # Given that the number of unique words (size of the vocabulary) can
        # be in the order of 20,000, which will also be the number of our
        # input features, a model trained on such features may suffer from the
        # curse of dimensionality.  Additionally, these features are very
        # sparse, since all are zero except one.  A more elegant way is to map
        # each word to a vector of fixed size with real-valued elements.  In
        # contrast to the one-hot encoded vectors, we can use finite size
        # vectors to represent an infinite number of real numbers.  This is
        # idea behind the embedding technique, which is a feature learning
        # technique that can be used to automatically learn the salient
        # features to represent the words in the data set.  Given the number
        # of unique words (unique_words), we can choose the size of the
        # embedding vectors to be much smaller than the number of unique words
        # (embedding_size << unique_words) to represent the entire vocabulary
        # as input features.
        embedding = tf.compat.v1.Variable(tf.random.uniform(
            (self.n_words, self.embed_size), minval=-1, maxval=1),
            name='embedding')
        # Embedded representation
        embed_x = tf.nn.embedding_lookup(embedding, tf_x, name='embeded_x')

        # Define LSTM cell and stack them together
        cells = tf.nn.rnn_cell.MultiRNNCell([
            tf.compat.v1.nn.rnn_cell.DropoutWrapper(
                tf.nn.rnn_cell.BasicLSTMCell(self.lstm_size),
                output_keep_prob=tf_keepprob) for i in range(self.num_layers)
        ])

        # Define the initial state
        self.initial_state = cells.zero_state(self.batch_size, tf.float32)
        print('  << initial state >> ', self.initial_state)

        # lstm_outputs shape: [batch_size, max_time, cells.output_size]
        lstm_outputs, self.final_state = tf.nn.dynamic_rnn(
            cells, embed_x, initial_state=self.initial_state)
        print('\n  << lstm_output  >> ', lstm_outputs)
        print('\n  << final state  >> ', self.final_state)

        # Apply a fully connected layer after on top of RNN output
        logits = tf.layers.dense(inputs=lstm_outputs[:, -1],
                                 units=1,
                                 activation=None,
                                 name='logits')

        logits = tf.squeeze(logits, name='logits_squeezed')
        print('\n  << logits       >> ', logits)

        y_proba = tf.nn.sigmoid(logits, name='probabilities')
        predictions = {
            'probabilities': y_proba,
            'labels': tf.cast(tf.round(y_proba), tf.int32, name='labels')
        }
        print('\n  << predictions  >> ', predictions)

        # Define the cost function
        cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
            labels=tf_y, logits=logits),
            name='cost')

        # Define the optimiser
        optimiser = tf.compat.v1.train.AdamOptimizer(self.learning_rate)
        train_op = optimiser.minimize(cost, name='train_op')

    def train(self, X_train, y_train, num_epochs):
        """Train the model.

        This function creates the TensorFlow session for launching the
        computation graph.  It iterates through the minibatches of data, and
        runs for a fixed number of epochs, to minimise the cost function
        defined in the graph.  It also saves the model after 10 epochs for
        checkpointing.                                                                           
        """
        with tf.compat.v1.Session(graph=self.g) as sess:
            sess.run(self.init_op)
            iteration = 1
            for epoch in range(num_epochs):
                state = sess.run(self.initial_state)

                for batch_x, batch_y in ppimdb.create_batch_generator(
                        X_train, y_train, self.batch_size):
                    feed = {
                        'tf_x:0': batch_x,
                        'tf_y:0': batch_y,
                        'tf_keepprob:0': 0.5,
                        self.initial_state: state
                    }
                    loss, _, state = sess.run(
                        ['cost:0', 'train_op', self.final_state],
                        feed_dict=feed)

                    if iteration % 20 == 0:
                        print('Epoch: %d/%d Iteration: %d '
                              '| Train loss: %.5f' %
                              (epoch + 1, num_epochs, iteration, loss))

                    iteration += 1

                if (epoch + 1) % 10 == 0:
                    self.saver.save(sess, 'model/sentiment-%d.ckpt' % epoch)

    def predict(self, X_data, return_proba=False):
        """Perform prediction.

        This method creates a new session, restores the last checkpoint saved
        during the training process, and carries out the predictions for the
        test data.
        """
        preds = []
        with tf.compat.v1.Session(graph=self.g) as sess:
            self.saver.restore(sess, tf.train.latest_checkpoint('model/'))
            test_state = sess.run(self.initial_state)
            for ii, batch_x in enumerate(
                    ppimdb.create_batch_generator(X_data,
                                                  None,
                                                  batch_size=self.batch_size),
                    1):
                feed = {
                    'tf_x:0': batch_x,
                    'tf_keepprob:0': 1.0,
                    self.initial_state: test_state
                }
                if return_proba:
                    pred, test_state = sess.run(
                        ['probabilities:0', self.final_state], feed_dict=feed)
                else:
                    pred, test_state = sess.run(['labels:0', self.final_state],
                                                feed_dict=feed)
                preds.append(pred)
        return np.concatenate(preds)


def main():
    # Load data set
    X_train, y_train, X_test, y_test, n_words = ppimdb.preprocess_imdb()

    # Train
    rnn = SentimentRNN(n_words=n_words,
                       seq_len=200,  # sequence_length in preprocess_imdb()
                       embed_size=256,
                       lstm_size=128,
                       num_layers=1,
                       batch_size=100,
                       learning_rate=0.001)

    if not os.path.isdir('model'):
        rnn.train(X_train, y_train, num_epochs=40)

    # Test
    preds = rnn.predict(X_test)
    y_true = y_test[:len(preds)]
    print('Test Acc: %.3f' % (np.sum(preds == y_true) / len(y_true)))

    # Get probabilities
    proba = rnn.predict(X_test, return_proba=True)


if __name__ == '__main__':
    main()
