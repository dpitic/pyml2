"""Character level language modelling using TensorFlow."""
import numpy as np

import char_rnn


def reshape_data(sequence, batch_size, num_steps):
    """Reshape the data into batches of sequences and return the input and
    output data.

    The goal is to predict the next character based on the sequence of
    characters that have already been observed so far.  Therefore, shift the
    input (x) and output (y) of the neural network by one character.
    """
    mini_batch_length = batch_size * num_steps
    num_batches = int(len(sequence) / mini_batch_length)
    if num_batches * mini_batch_length + 1 > len(sequence):
        num_batches = num_batches - 1
    # Truncate the sequence at the end to get rid of the remaining characters
    # that do not make a full batch
    x = sequence[0:num_batches * mini_batch_length]
    y = sequence[1:num_batches * mini_batch_length + 1]
    # Split x and y into a list batches of sequences
    x_batch_splits = np.split(x, batch_size)
    y_batch_splits = np.split(y, batch_size)
    # Stack the batches together: batch_size x mini_batch_length
    x = np.stack(x_batch_splits)
    y = np.stack(y_batch_splits)

    return x, y


def create_batch_generator(data_x, data_y, num_steps):
    """Split the data arrays and return a batch generator."""
    batch_size, tot_batch_length = data_x.shape
    num_batches = int(tot_batch_length / num_steps)
    for b in range(num_batches):
        yield (data_x[:, b * num_steps:(b + 1) * num_steps],
               data_y[:, b * num_steps:(b + 1) * num_steps])


def get_top_char(probas, char_size, top_n=5):
    """Randomly select one character according to the given probabilities."""
    p = np.squeeze(probas)
    p[np.argsort(p)[:-top_n]] = 0.0
    p = p / np.sum(p)
    ch_id = np.random.choice(char_size, 1, p=p)[0]
    return ch_id


def main():
    # Read the raw text file and preprocess
    with open('pg2265.txt', 'r', encoding='utf-8') as f:
        text = f.read()

    # Remove the beginning portion that contains legal descriptions
    text = text[16251:]
    # Get the set of unique characters
    chars = set(text)
    # Character to integer encoder
    char2int = {ch: i for i, ch in enumerate(chars)}
    # Integer to character decoder
    int2char = dict(enumerate(chars))
    # Encode the characters in the text in NumPy array
    text_ints = np.array([char2int[ch] for ch in text], dtype=np.int32)

    # Testing
    train_x, train_y = reshape_data(text_ints, 64, 10)
    print(train_x.shape)
    print(train_x[0, :10])
    print(train_y[0, :10])
    print(''.join(int2char[i] for i in train_x[0, :50]))

    # Batch generator
    bgen = create_batch_generator(train_x[:, :100], train_y[:, :100], 15)
    for b in bgen:
        print(b[0].shape, b[1].shape, end='  ')
        print(''.join(int2char[i] for i in b[0][0, :]).replace('\n',
                                                               '*'), '    ',
              ''.join(int2char[i] for i in b[1][0, :]).replace('\n', '*'))

    batch_size = 64
    num_steps = 100
    train_x, train_y = reshape_data(text_ints, batch_size, num_steps)

    # Run for 100 epochs
    rnn = char_rnn.CharRNN(num_classes=len(chars), batch_size=batch_size)
    rnn.train(train_x, train_y, num_epochs=100, ckpt_dir='./model-100/')

    np.random.seed(123)
    rnn = char_rnn.CharRNN(len(chars),
                           chars=chars,
                           char2int=char2int,
                           int2char=int2char,
                           sampling=True)
    print(rnn.sample(ckpt_dir='./model-100/', output_length=500))

    # Run for 200 epochs
    rnn = char_rnn.CharRNN(num_classes=len(chars), batch_size=batch_size)
    rnn.train(train_x, train_y, num_epochs=200, ckpt_dir='./model-200/')

    del rnn

    np.random.seed(123)
    rnn = char_rnn.CharRNN(len(chars),
                           chars=chars,
                           char2int=char2int,
                           int2char=int2char,
                           sampling=True)
    print(rnn.sample(ckpt_dir='./model-200/', output_length=500))


if __name__ == '__main__':
    main()
