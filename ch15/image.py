"""Demonstration of using Pillow to read an RGB image."""
import imageio


def main():
    FILENAME = 'example-image.png'
    try:
        img = imageio.imread(FILENAME, pilmode='RGB')
    except AttributeError:
        s = ("imageio.imread() requires Python's image library PIL"
             " You can satisfy this requirement by installing the"
             " user friendly fork PILLOW via `pip install pillow`.")
        raise AttributeError(s)

    print('Image shape:', img.shape)
    print('Number of channels:', img.shape[2])
    print('Image data type:', img.dtype)
    print(img[100:102, 100:102, :])


if __name__ == '__main__':
    main()
