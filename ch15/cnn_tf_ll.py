"""Implementing a CNN in the TensorFlow low-level API"""
import os

import numpy as np
import tensorflow as tf

import cnn_utils
import mlutils


def conv_layer(input_tensor,
               name,
               kernel_size,
               n_output_channels,
               padding_mode='SAME',
               strides=(1, 1, 1, 1)):
    """Wrapper function for a convolution layer.

    This function performs all of the necessary work for building a
    convolution layer, including defining the weights, biases and initialising
    them, and the convolution operations.

    :param tensor input_tensor: Tensor given as input to the convolution layer.
    :param string name: Name of the layer, which is used as the scope name.
    :param tuple kernel_size: Dimensions of the kernel tensor provided as a
    tuple or a list.
    :param int n_output_channels: Number of output feature maps.
    :return: Convolution layer.
    """
    with tf.compat.v1.variable_scope(name):
        # get n_input_channels:
        #  input tensor shape:
        #  [batch x width x height x channels_in]
        input_shape = input_tensor.get_shape().as_list()
        n_input_channels = input_shape[-1]

        weights_shape = (list(kernel_size) +
                         [n_input_channels, n_output_channels])

        weights = tf.compat.v1.get_variable(name='_weights',
                                            shape=weights_shape)
        print(weights)
        biases = tf.compat.v1.get_variable(
            name='_biases', initializer=tf.zeros(shape=[n_output_channels]))
        print(biases)
        conv = tf.nn.conv2d(input=input_tensor,
                            filter=weights,
                            strides=strides,
                            padding=padding_mode)
        print(conv)
        conv = tf.nn.bias_add(conv, biases, name='net_pre-activation')
        print(conv)
        conv = tf.nn.relu(conv, name='activation')
        print(conv)
        return conv


def fc_layer(input_tensor, name, n_output_units, activation_fn=None):
    """Define the fully connected layer.

    :param tensor input_tensor: The input tensor.
    :param string name: The name of the layer which is used as the scope name.
    :param int n_output_units: Number of output units.
    """
    with tf.compat.v1.variable_scope(name):
        input_shape = input_tensor.get_shape().as_list()[1:]
        n_input_units = np.prod(input_shape)
        if len(input_shape) > 1:
            input_tensor = tf.reshape(input_tensor, shape=(-1, n_input_units))

        weights_shape = [n_input_units, n_output_units]

        weights = tf.compat.v1.get_variable(name='_weights',
                                            shape=weights_shape)
        print(weights)
        biases = tf.compat.v1.get_variable(
            name='_biases', initializer=tf.zeros(shape=[n_output_units]))
        print(biases)
        layer = tf.matmul(input_tensor, weights)
        print(layer)
        layer = tf.nn.bias_add(layer, biases, name='net_pre-activation')
        print(layer)
        if activation_fn is None:
            return layer

        layer = activation_fn(layer, name='activation')
        return layer


def build_cnn(learning_rate=1e-4):
    """Build the whole convolution network."""
    # Placeholders for X and y
    tf_x = tf.compat.v1.placeholder(tf.float32, shape=[None, 784], name='tf_x')
    tf_y = tf.compat.v1.placeholder(tf.int32, shape=[None], name='tf_y')

    # Reshape x to 4D tensor: [batchsize, width, height, 1]
    tf_x_image = tf.reshape(tf_x, shape=[-1, 28, 28, 1], name='tf_x_reshaped')
    # One-hot encoding
    tf_y_onehot = tf.one_hot(indices=tf_y,
                             depth=10,
                             dtype=tf.float32,
                             name='tf_y_onehot')

    # 1st layer: Conv_1
    print('\nBuilding 1st layer:')
    h1 = conv_layer(tf_x_image,
                    name='conv_1',
                    kernel_size=(5, 5),
                    padding_mode='VALID',
                    n_output_channels=32)
    # Max pooling
    h1_pool = tf.nn.max_pool(h1,
                             ksize=[1, 2, 2, 1],
                             strides=[1, 2, 2, 1],
                             padding='SAME')

    # 2nd layer: Conv_2
    print('\nBuilding 2nd layer:')
    h2 = conv_layer(h1_pool,
                    name='conv_2',
                    kernel_size=(5, 5),
                    padding_mode='VALID',
                    n_output_channels=64)
    # Max pooling
    h2_pool = tf.nn.max_pool(h2,
                             ksize=[1, 2, 2, 1],
                             strides=[1, 2, 2, 1],
                             padding='SAME')

    # 3rd layer: Fully connected
    print('\nBuilding 3rd layer:')
    h3 = fc_layer(h2_pool,
                  name='fc_3',
                  n_output_units=1024,
                  activation_fn=tf.nn.relu)
    # Dropout
    keep_prob = tf.compat.v1.placeholder(tf.float32, name='fc_keep_prob')
    h3_drop = tf.nn.dropout(h3, keep_prob=keep_prob, name='dropout_layer')

    # 4th layer: Fully connected (linear activation)
    print('\nBuilding 4th layer:')
    h4 = fc_layer(h3_drop, name='fc_4', n_output_units=10, activation_fn=None)

    # Prediction
    predictions = {
        'probabilities': tf.nn.softmax(h4, name='probabilities'),
        'labels': tf.cast(tf.argmax(h4, axis=1), tf.int32, name='labels')
    }

    # Visualise the graph with TensorBoard

    # Loss function and optimisation
    cross_entropy_loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits=h4, labels=tf_y_onehot),
        name='cross_entropy_loss')

    # Optimiser
    # Adam Optimiser is a robust gradient-based optimisation method suited for
    # nonconvex optimisation and machine learning problems.  The key advantage
    # of Adam is in the choice of update step size derived from the running
    # average of gradient moments.
    optimiser = tf.train.AdamOptimizer(learning_rate)
    optimiser = optimiser.minimize(cross_entropy_loss, name='train_op')

    # Compute prediction accuracy
    correct_predictions = tf.equal(predictions['labels'],
                                   tf_y,
                                   name='correct_preds')

    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32),
                              name='accuracy')


def save(saver, sess, epoch, path='./model/'):
    """Save trained model."""
    if not os.path.isdir(path):
        os.makedirs(path)
    print('Saving model in', path)
    saver.save(sess, os.path.join(path, 'cnn-model.ckpt'), global_step=epoch)


def load(saver, sess, path, epoch):
    """Load trained model."""
    print('Loading model from', path)
    saver.restore(sess, os.path.join(path, 'cnn-model.ckpt-%d' % epoch))


def train(sess,
          training_set,
          validation_set=None,
          initialise=True,
          epochs=20,
          shuffle=True,
          dropout=0.5,
          random_seed=None):
    """Train the model."""
    X_data = np.array(training_set[0])
    y_data = np.array(training_set[1])
    training_loss = []

    # Initialise variables
    if initialise:
        sess.run(tf.compat.v1.global_variables_initializer())

    np.random.seed(random_seed)  # for shuffling in batch_generator()
    for epoch in range(1, epochs + 1):
        batch_gen = mlutils.batch_generator(X_data, y_data, shuffle=shuffle)
        avg_loss = 0.0
        for i, (batch_x, batch_y) in enumerate(batch_gen):
            feed = {
                'tf_x:0': batch_x,
                'tf_y:0': batch_y,
                'fc_keep_prob:0': dropout
            }
            loss, _ = sess.run(['cross_entropy_loss:0', 'train_op'],
                               feed_dict=feed)
            avg_loss += loss

        training_loss.append(avg_loss / (i + 1))
        print('Epoch %02d Training Avg. Loss: %7.3f' % (epoch, avg_loss),
              end=' ')
        if validation_set is not None:
            feed = {
                'tf_x:0': validation_set[0],
                'tf_y:0': validation_set[1],
                'fc_keep_prob:0': 1.0
            }
            valid_acc = sess.run('accuracy:0', feed_dict=feed)
            print(' Validation Acc: %7.3f' % valid_acc)
        else:
            print()


def predict(sess, X_test, return_proba=False):
    """
    Obtain predictions probabilities or prediction labels of the test data.
    """
    feed = {'tf_x:0': X_test, 'fc_keep_prob:0': 1.0}
    if return_proba:
        return sess.run('probabilities:0', feed_dict=feed)
    else:
        return sess.run('labels:0', feed_dict=feed)


def main():
    # Test conv_layer()
    print('Test conv_layer()')
    g = tf.Graph()
    with g.as_default():
        x = tf.compat.v1.placeholder(tf.float32, shape=[None, 28, 28, 1])
        conv_layer(x,
                   name='convtest',
                   kernel_size=(3, 3),
                   n_output_channels=32)

    # Test fc_layer()
    print('\nTest fc_layer()')
    g = tf.Graph()
    with g.as_default():
        x = tf.compat.v1.placeholder(tf.float32, shape=[None, 28, 28, 1])
        fc_layer(x, name='fctest', n_output_units=32, activation_fn=tf.nn.relu)

    # Create TensorFlow graph object, set the graph level random seed and build
    # the CNN model in that graph
    random_seed = 123

    np.random.seed(random_seed)

    # Create a graph
    g = tf.Graph()
    with g.as_default():
        tf.set_random_seed(random_seed)
        # Build the graph
        build_cnn()

        # saver
        saver = tf.compat.v1.train.Saver()

    # Load the data set
    X_train, y_train, X_valid, y_valid, X_test, y_test = \
        cnn_utils.train_test_split()
    X_train_centred, X_valid_centred, X_test_centred = cnn_utils.preprocess()

    # Create a TensorFlow session and train the CNN model.  This is the
    # initial model training for 20 epochs and a checkpoint will be saved.
    if not os.path.isdir('model'):
        print('Training model...')
        with tf.compat.v1.Session(graph=g) as sess:
            train(sess,
                  training_set=(X_train_centred, y_train),
                  validation_set=(X_valid_centred, y_valid),
                  initialise=True,
                  random_seed=123)
            # Save the trained model for future use
            save(saver, sess, epoch=20)

    # Calculate prediction accuracy on the test data set, restoring the
    # saved model
    del g

    # Create new graph and build the model
    g2 = tf.Graph()
    with g2.as_default():
        tf.compat.v1.set_random_seed(random_seed)
        # Build the graph
        build_cnn()

        # Saver
        saver = tf.compat.v1.train.Saver()

    # Create a new session and restore the model
    with tf.compat.v1.Session(graph=g2) as sess:
        load(saver, sess, epoch=20, path='./model/')

        preds = predict(sess, X_test_centred, return_proba=False)

        print('Test Accuracy: %.3f%%' %
              (100 * np.sum(preds == y_test) / len(y_test)))

    # Run the prediction on some test samples
    np.set_printoptions(precision=2, suppress=True)

    with tf.compat.v1.Session(graph=g2) as sess:
        load(saver, sess, epoch=20, path='./model/')

        print(predict(sess, X_test_centred[:10], return_proba=False))
        print(predict(sess, X_test_centred[:10], return_proba=True))

    # Continue training for 20 more epochs without re-initialising
    # (initialise=False).  Create a new session and restore the model.
    with tf.compat.v1.Session(graph=g2) as sess:
        # Check whether complete model exists
        if not os.path.isfile('./model/cnn-model.ckpt-40.meta'):
            # Load initially trained model
            load(saver, sess, epoch=20, path='./model/')
            print('Training model for another 20 more epochs...')
            train(sess,
                  training_set=(X_train_centred, y_train),
                  validation_set=(X_valid_centred, y_valid),
                  initialise=False,
                  epochs=20,
                  random_seed=123)

            save(saver, sess, epoch=40, path='./model/')
        else:
            # Load completely trained model (40 epochs)
            print('Loading completely trained model (40 epochs)...')
            load(saver, sess, epoch=40, path='./model/')

        preds = predict(sess, X_test_centred, return_proba=False)

        print('Test Accuracy: %.3f%%' %
              (100 * np.sum(preds == y_test) / len(y_test)))


if __name__ == '__main__':
    main()
