"""Implementation of a CNN in the TensorFlow layers API"""
import os

import numpy as np
import tensorflow as tf

import cnn_utils
import mlutils


class ConvNN:
    """Implementation of a Convolution Neural Network (CNN) in the TensorFlow
    Layers API.
    """

    def __init__(self,
                 batchsize=64,
                 epochs=20,
                 learning_rate=1e-4,
                 dropout_rate=0.5,
                 shuffle=True,
                 random_seed=None):
        np.random.seed(random_seed)
        self.batchsize = batchsize
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.dropout_rate = dropout_rate
        self.shuffle = shuffle

        g = tf.Graph()
        with g.as_default():
            tf.set_random_seed(random_seed)
            # Build the network
            self.build()

            # Initialiser
            self.init_op = tf.compat.v1.global_variables_initializer()

            # Saver
            self.saver = tf.compat.v1.train.Saver()

        # Create a session
        self.sess = tf.compat.v1.Session(graph=g)

    def build(self):
        """Build the CNN"""
        # Placeholders for X and y
        tf_x = tf.compat.v1.placeholder(tf.float32,
                                        shape=[None, 784],
                                        name='tf_x')
        tf_y = tf.compat.v1.placeholder(tf.int32, shape=[None], name='tf_y')
        is_train = tf.compat.v1.placeholder(tf.bool, shape=(), name='is_train')

        # Reshape x to 4D tensor: [batch_size, width, height, 1]
        tf_x_image = tf.compat.v1.reshape(tf_x,
                                          shape=[-1, 28, 28, 1],
                                          name='input_x_2dimages')
        # One-hot encoding
        tf_y_onehot = tf.compat.v1.one_hot(indices=tf_y,
                                           depth=10,
                                           dtype=tf.float32,
                                           name='input_y_onehot')

        # 1st layer: Conv_1
        h1 = tf.compat.v1.layers.conv2d(tf_x_image,
                                        kernel_size=(5, 5),
                                        filters=32,
                                        activation=tf.nn.relu)
        # Max pooling
        h1_pool = tf.compat.v1.layers.max_pooling2d(h1,
                                                    pool_size=(2, 2),
                                                    strides=(2, 2))

        # 2nd layer: Conv_2
        h2 = tf.compat.v1.layers.conv2d(h1_pool,
                                        kernel_size=(5, 5),
                                        filters=64,
                                        activation=tf.nn.relu)
        # Max pooling
        h2_pool = tf.compat.v1.layers.max_pooling2d(h2,
                                                    pool_size=(2, 2),
                                                    strides=(2, 2))

        # 3rd layer: Fully connected
        input_shape = h2_pool.get_shape().as_list()
        n_input_units = np.prod(input_shape[1:])
        h2_pool_flat = tf.reshape(h2_pool, shape=[-1, n_input_units])
        h3 = tf.compat.v1.layers.dense(h2_pool_flat,
                                       1024,
                                       activation=tf.nn.relu)

        # Dropout
        h3_drop = tf.compat.v1.layers.dropout(h3,
                                              rate=self.dropout_rate,
                                              training=is_train)

        # 4th layer: Fully Connected (linear activation)
        h4 = tf.compat.v1.layers.dense(h3_drop, 10, activation=None)

        # Prediction
        predictions = {
            'probabilities': tf.nn.softmax(h4, name='probabilities'),
            'labels': tf.cast(tf.argmax(h4, axis=1), tf.int32, name='labels')
        }

        # Loss function and optimisation
        cross_entropy_loss = tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits(logits=h4,
                                                    labels=tf_y_onehot),
            name='cross_entropy_loss')

        # Optimiser
        optimiser = tf.train.AdamOptimizer(self.learning_rate)
        optimiser = optimiser.minimize(cross_entropy_loss, name='train_op')

        # Finding accuracy
        correct_predictions = tf.equal(predictions['labels'],
                                       tf_y,
                                       name='correct_preds')
        accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32),
                                  name='accuracy')

    def save(self, epoch, path='./tflayers-model/'):
        """Save the trained model."""
        if not os.path.isdir(path):
            os.makedirs(path)
        print('Saving model in', path)
        self.saver.save(self.sess,
                        os.path.join(path, 'model.ckpt'),
                        global_step=epoch)

    def load(self, epoch, path):
        """Load trained model."""
        print('Loading model from', path)
        self.saver.restore(self.sess,
                           os.path.join(path, 'model.ckpt-%d' % epoch))

    def train(self, training_set, validation_set=None, initialise=True):
        """Train the CNN model."""
        # Initialise variables if required
        if initialise:
            self.sess.run(self.init_op)

        self.train_cost_ = []
        X_data = np.array(training_set[0])
        y_data = np.array(training_set[1])

        for epoch in range(1, self.epochs + 1):
            batch_gen = mlutils.batch_generator(X_data,
                                                y_data,
                                                shuffle=self.shuffle)
            avg_loss = 0.0
            for i, (batch_x, batch_y) in enumerate(batch_gen):
                feed = {
                    'tf_x:0': batch_x,
                    'tf_y:0': batch_y,
                    'is_train:0': True  # for dropout
                }
                loss, _ = self.sess.run(['cross_entropy_loss:0', 'train_op'],
                                        feed_dict=feed)
                avg_loss += loss

            print('Epoch %02d: Training Avg. Loss: '
                  '%7.3f' % (epoch, avg_loss),
                  end=' ')
            if validation_set is not None:
                feed = {
                    'tf_x:0': batch_x,
                    'tf_y:0': batch_y,
                    'is_train:0': False  # for dropout
                }
                valid_acc = self.sess.run('accuracy:0', feed_dict=feed)
                print('Validation Acc: %7.3f' % valid_acc)
            else:
                print()

    def predict(self, X_test, return_proba=False):
        """Run predictions"""
        feed = {'tf_x:0': X_test, 'is_train:0': False}  # for dropout
        if return_proba:
            return self.sess.run('probabilities:0', feed_dict=feed)
        else:
            return self.sess.run('labels:0', feed_dict=feed)


def main():
    # Load the data set
    X_train, y_train, X_valid, y_valid, X_test, y_test = \
        cnn_utils.train_test_split()
    X_train_centred, X_valid_centred, X_test_centred = cnn_utils.preprocess()

    if not os.path.isfile('tflayers-model'):
        cnn = ConvNN(random_seed=123)
        cnn.train(training_set=(X_train_centred, y_train),
                  validation_set=(X_valid_centred, y_valid))
        cnn.save(epoch=20)

    del cnn

    # Do predictions on the test data set
    cnn2 = ConvNN(random_seed=123)
    cnn2.load(epoch=20, path='./tflayers-model/')
    print(cnn2.predict(X_test_centred[:10, :]))
    preds = cnn2.predict(X_test_centred)
    print('Test Accuracy: %.2f%%' %
          (100 * np.sum(y_test == preds) / len(y_test)))


if __name__ == '__main__':
    main()
