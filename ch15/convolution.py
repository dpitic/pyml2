"""Convolution Neural Networks."""
import numpy as np
import scipy.signal


def conv1d(x, w, p=0, s=1):
    """Naive implementation of convolution computation for 1D vectors.

    Compute the convolution y = x * w
    :param list x: first convolution operand with n elements.
    :param list w: second convolution operand with m elements.
    :param int p: amount of padding to apply to x.
    :param int s: stride (convolution hyperparameter).
    :return array: y = x * w
    """
    w_rot = np.array(w[::-1])
    x_padded = np.array(x)
    if p > 0:
        zero_pad = np.zeros(shape=p)
        x_padded = np.concatenate([zero_pad, x_padded, zero_pad])
    res = []
    # Compute colvolution y = x * w
    for i in range(0, int(len(x) / s), s):
        res.append(np.sum(x_padded[i:i + w_rot.shape[0]] * w_rot))
    return np.array(res)


def conv2d(X, W, p=(0, 0), s=(1, 1)):
    """Naive implementation of convolution computation for 2D matrix.

    Compute the convolution Y = X * W
    :param array X: input matrix.
    :param array W: kernel matrix.
    :param tuple p: padding applied to input matrix X.
    :param tuple s: stride.
    :return array: convolution matrix.
    """
    W_rot = np.array(W)[::-1, ::-1]
    X_orig = np.array(X)
    n1 = X_orig.shape[0] + 2 * p[0]
    n2 = X_orig.shape[1] + 2 * p[1]
    X_padded = np.zeros(shape=(n1, n2))
    X_padded[p[0]:p[0] + X_orig.shape[0], p[1]:p[1] + X_orig.shape[1]] = X_orig
    res = []
    # Compute convolution Y = X * W
    for i in range(0,
                   int((X_padded.shape[0] - W_rot.shape[0]) / s[0]) + 1, s[0]):
        res.append([])
        for j in range(0,
                       int((X_padded.shape[1] - W_rot.shape[1]) / s[1]) + 1,
                       s[1]):
            X_sub = X_padded[i:i + W_rot.shape[0], j:j + W_rot.shape[1]]
            res[-1].append(np.sum(X_sub * W_rot))
    return (np.array(res))


def main():
    # Determining the size of the convolution output
    # Demonstrating how to compute convolutions in one dimension
    x = [1, 3, 2, 4, 5, 6, 1, 3]
    w = [1, 0, 3, 1, 2]
    print('Convld Implementation: ', conv1d(x, w, p=2, s=1))
    print('NumPy Results          ', np.convolve(x, w, mode='same'))

    # Performing a discrete convolution in 2D
    X = [[1, 3, 2, 4], [5, 6, 1, 3], [1, 2, 0, 2], [3, 4, 3, 2]]
    W = [[1, 0, 3], [1, 2, 1], [0, 1, 1]]
    print('conv2d Implementation:\n', conv2d(X, W, p=(1, 1), s=(1, 1)))
    print('Scipy Results:\n', scipy.signal.convolve2d(X, W, mode='same'))


if __name__ == '__main__':
    main()
