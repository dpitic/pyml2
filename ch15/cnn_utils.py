"""CNN utility funcitons."""
import numpy as np

import mlutils


def train_test_split():
    """Return train, validation and test data set."""
    X_data, y_data = mlutils.load_mnist('mnist', kind='train')
    print('Rows: %d, Columns: %d' % (X_data.shape[0], X_data.shape[1]))
    X_test, y_test = mlutils.load_mnist('mnist', kind='t10k')
    print('Rows: %d, Columns: %d' % (X_test.shape[0], X_test.shape[1]))

    X_train, y_train = X_data[:50000, :], y_data[:50000]
    X_valid, y_valid = X_data[50000:, :], y_data[50000:]

    print('Training:    ', X_train.shape, y_train.shape)
    print('Validaiton:  ', X_valid.shape, y_valid.shape)
    print('Test set:    ', X_test.shape, y_test.shape)

    return X_train, y_train, X_valid, y_valid, X_test, y_test


def preprocess():
    """Return the centred training, validation and test data."""
    X_train, y_train, X_valid, y_valid, X_test, y_test = train_test_split()
    # Compute the mean of each feature using the training data and calculate
    # the standard deviation across all features.
    mean_vals = np.mean(X_train, axis=0)
    std_val = np.std(X_train)

    X_train_centred = (X_train - mean_vals) / std_val
    X_valid_centred = (X_valid - mean_vals) / std_val
    X_test_centred = (X_test - mean_vals) / std_val

    return X_train_centred, X_valid_centred, X_test_centred


def main():
    preprocess()


if __name__ == '__main__':
    main()
