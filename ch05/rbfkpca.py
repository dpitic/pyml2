"""
This module implements the Radial Basis Function (RBF) kernel PCA algorithm
and a function to project a new data point onto a new feature subspace.
"""

import numpy as np
from scipy import exp
from scipy.linalg import eigh
from scipy.spatial.distance import pdist, squareform


def rbf_kernel_pca(X, gamma, n_components):
    """
    @brief      RBF kernel PCA implementation.

    @details    Radial Basis Function (RBF) kernel Principal Component
                Analysis (PCA) implementation.  One downside of RBF kernel PCA
                for dimensionality reduction is that we have to specify the
                gamma parameter.  Finding an appropriate value requires
                experimentation and is best done using algorithms for
                parameter tuning.

    @param      X: NumPy ndarray, shape = [n_samples, n_features]
    @param      gamma: float tuning parameter of the RBF kernel.
    @param      n_component: int number of principal components to return.

    @return     alphas: NumPy ndarray, shape = [n_samples, k_features]
                Eigenvectors of the kernel matrix representing the projected
                data set.
                lambdas: list of eigenvalues of the kernel matrix.
    """
    # Calculate the pairwise squared Euclidean distances in the MxN
    # dimensional data set.
    sq_dists = pdist(X, 'sqeuclidean')

    # Convert pairwise distances into a square matrix
    mat_sq_dists = squareform(sq_dists)

    # Compute the symmetric kernel matrix
    K = exp(-gamma * mat_sq_dists)

    # Centre the kernel matrix
    N = K.shape[0]
    one_n = np.ones((N, N)) / N
    K = K - one_n.dot(K) - K.dot(one_n) + one_n.dot(K).dot(one_n)

    # Obtaining eigenpairs from the centred kernel matrix scipy.linalg.eigh
    # returns them in ascending order
    eigvals, eigvecs = eigh(K)
    eigvals, eigvecs = eigvals[::-1], eigvecs[:, ::-1]

    # Collect the top k eigenvectors (projected samples)
    alphas = np.column_stack((eigvecs[:, i] for i in range(n_components)))

    # Collect the corresponding eigenvalues
    lambdas = [eigvals[i] for i in range(n_components)]
    return alphas, lambdas


def project_x(x_new, X, gamma, alphas, lambdas):
    """
    @brief      Project any new data sample onto the new feature subspace.

    @details    This function is used to project a new data sample onto a new
                subspace alphas.

    @param      x_new: new data point to project.
    @param      X: array containing original data set.mai
    @param      gamma: tuning parameter of the RBF kernel.
    @param      alphas: NumPy ndarray, shape = [n_samples, k_features]
                Eigenvectors of the kernel matrix representing the projected
                data set.
    @param      lambdas: list of eigenvalues of the kernel matrix.
    """
    pair_dist = np.array([np.sum((x_new - row)**2) for row in X])
    k = np.exp(-gamma * pair_dist)
    return k.dot(alphas / lambdas)
