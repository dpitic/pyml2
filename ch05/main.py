"""
This module explores three different fundamental dimensionality reduction
techniques for feature extraction: standard PCA, LDA, and kernel PCA.

Using PCA we project data onto a lower-diensional subspace to maximise the
variance along the orthogonal feature axes, while ignoring the class lables;
therefore PCA is an unsupervised dimensionality reduction technique.  This is
similar to the feature extraction techniques used to reduce the number of
features in a data set.  The difference between feature selection and feature
extraction is that while we maintain the original features when using feature
selection, such as sequential backward selection, feature extraction is used to
transform or project the data onto a new feature space.

LDA is a technique for supervised dimensionality reduction, which means that it
considers class information in the training data set, to attempt to maximise
the class separability in a linear feature space.

Kernel PCA is a nonlinear feature extraction technique.  Using the kernel trick
and a temporary projection onto a higher-dimensinoal feature space, it enables
compression of data sets consisting of nonlinear features onto a lower
dimensional subspace where the classes become linearly separable.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.datasets import make_circles, make_moons
from sklearn.decomposition import PCA, KernelPCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import mlutils
import rbfkpca


def main():
    # Extract the principal components step by step
    # 1. Standardise the data.
    # 2. Construct the covariance matrix.
    # 3. Obtain the eigenvalues and eigenvectors of the covariance matrix.
    # 4. Sort the eigenvalues by decreasing order to rank the eigenvectors.
    # Start by loading the Wine data set.
    df_wine = pd.read_csv(
        'https://archive.ics.uci.edu/ml/'
        'machine-learning-databases/wine/wine.data',
        header=None)
    # Preprocess the Wine data into separate training and test sets, using
    # 70/30% split, and standardise it to unit variance
    X, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.3, stratify=y, random_state=0)
    # 1. Standardise the features
    sc = StandardScaler()
    X_train_std = sc.fit_transform(X_train)
    X_test_std = sc.transform(X_test)
    # 2. Construct the covariance matrix
    # The symmetric d x d dimensional covariance matrix, where d is the number
    # of dimensions in the data set, stores the pairwise covariances between
    # the different features.  The samples means are zero if we standardise
    # the data set.  A positive covariance between two features indicates that
    # the features increase or decrease together, whereas a negative
    # covariance indicates that the features vary in opposite directions.
    # The eigenvectors of the covariance matrix represent the principal
    # components (the directions of maximum variance), whereas the
    # corresponding eigenvalues will define their magnitude.  For the Wine
    # data set, we would obtain 13 eigenvectors and eigenvalues from the 13x13
    # dimensional covariance matrix.
    cov_mat = np.cov(X_train_std.T)
    # 3. Obtain the eigenpairs of the Wine covariance matrix
    # Use the linalg.eig() function from NumPy to obtain the eigenpairs.
    eigen_vals, eigen_vecs = np.linalg.eig(cov_mat)
    print('\nEigenvalues \n%s' % eigen_vals)
    # Total and explained variance
    # Since we want to reduce the dimensionality of the data set by
    # compressing it onto a new feature subspace, we only select the subset of
    # the eigenvectors (principal components) that contains most of the
    # information (variance).  The eigenvalues define the magnitude of the
    # eigenvectors, so we have to sort the eigenvalues by decreasing magnitude
    # and we are interested in the top k eigenvectors based on the values of
    # their corresponding eigenvalues.  First plot the variance explained
    # ratios of the eigenvalues, which are simply the fraction of the
    # eigenvalue and the total sum of the eigenvalues.
    tot = sum(eigen_vals)
    var_exp = [(i / tot) for i in sorted(eigen_vals, reverse=True)]
    # Calculate the cumulative sum of the explained variances using the NumPy
    # cumsum() function.
    cum_var_exp = np.cumsum(var_exp)
    plt.bar(
        range(1, 14),
        var_exp,
        alpha=0.5,
        align='center',
        label='individual explained variance')
    plt.step(
        range(1, 14),
        cum_var_exp,
        where='mid',
        label='cumulative explained variance')
    plt.ylabel('Explained variance ratio')
    plt.xlabel('Principal component index')
    plt.legend(loc='best')
    plt.show()
    # 4. Feature transformation
    # Sort the eigenpairs by descending order of the eigenvalues, construct
    # a projection matrix from the selected eigenvectors, and use the
    # projection matrix to transform the data onto the lower dimensional
    # subspace.
    # Sort the eigenpairs by decreasing order of the eigenvalues.  Start by
    # making a list of (eigenvalue, eigenvector) tuples
    eigen_pairs = [(np.abs(eigen_vals[i]), eigen_vecs[:, i])
                   for i in range(len(eigen_vals))]
    # Sort the (eigenvalue, eigenvector) tuples from high to low
    eigen_pairs.sort(key=lambda k: k[0], reverse=True)
    # Collect the two eigenvectors that correspond to the two largest
    # eigenvalues, to capture about 60% of the variance in the Wine data set.
    # This is an arbitrary choice for illustration purposes.  In practice the
    # number of principal components has to be determined by a compromise
    # between computational efficiency and the performance of the classifier.
    w = np.hstack((eigen_pairs[0][1][:, np.newaxis],
                   eigen_pairs[1][1][:, np.newaxis]))
    # The matrix is a 13x2 dimensional projection matrix from the top two
    # eigenvectors.
    print('\nMatrix W:\n', w)
    # Transform a sample x (represented as a 1x13 dimensional row vector) onto
    # the PCA subspace using the projection matrix, to obtain x', a 2-D sample
    # vector consisting of two new features.
    print('\nTransformed sample:\n', X_train_std[0].dot(w))
    # Similarly, transform the entire 124x13 dimensional training data set
    # onto the two principal components by calculating the matrix dot product.
    X_train_pca = X_train_std.dot(w)
    # Visualise the transformed Wine training data set, now stored as an
    # 124x2 dimensional matrix, in a two dimensional scatter plot.
    colours = ['r', 'b', 'g']
    markers = ['s', 'x', 'o']
    for l, c, m in zip(np.unique(y_train), colours, markers):
        plt.scatter(
            X_train_pca[y_train == l, 0],
            X_train_pca[y_train == l, 1],
            c=c,
            label=l,
            marker=m)
    plt.title('Training data principal components')
    plt.xlabel('PC 1')
    plt.ylabel('PC 2')
    plt.legend(loc='lower left')
    plt.show()
    # Principal component analysis in scikit-learn
    # The following code demonstrates how to use the PCA class implemented in
    # scikit-learn.  PCA class is another one of scikit-learn's transformer
    # Classes, where we first fit the model using the training data before we
    # transform both the training data and the test data set using the same
    # model parameters.
    pca = PCA(n_components=2)
    lr = LogisticRegression(solver='liblinear', multi_class='auto')
    X_train_pca = pca.fit_transform(X_train_std)
    X_test_pca = pca.transform(X_test_std)
    lr.fit(X_train_pca, y_train)
    mlutils.plot_decision_regions(X_train_pca, y_train, classifier=lr)
    plt.title('Transformed training data decision regions')
    plt.xlabel('PC 1')
    plt.ylabel('PC 2')
    plt.legend(loc='lower left')
    plt.show()
    # Plot the decision regions of the logistic regression on the transformed
    # test data set to see if it can separate the classes well.
    mlutils.plot_decision_regions(X_test_pca, y_test, classifier=lr)
    plt.title('Transformed test data decision regions')
    plt.xlabel('PC 1')
    plt.ylabel('PC 2')
    plt.legend(loc='lower left')
    plt.show()
    # If we are interested in the explained variance ratios of the different
    # principal components, we can simply initialise the PCA class with the
    # n_components=None, so all principal components are kept and the
    # explained variance ratio can then be accessed via the
    # explained_variance_ratio_ attribute.
    pca = PCA(n_components=None)
    X_train_pca = pca.fit_transform(X_train_std)
    print('\nExplained variance ratios:\n', pca.explained_variance_ratio_)
    # Linear Discriminant Analysis
    # Main steps that are required to perform LDA
    # 1. Standardise the d-dimensional data set (d = number of features)
    # 2. For each class, compute the d-dimensional mean vector
    # 3. Construct the between class and within class scatter matrices
    # 4. Compute the eigenvectors and corresponding eigenvalues
    # 5. Sort the eigenvalues by decreasing order to rank the corresponding
    #    eigenvectors.
    # 6. Choose the k eigenvectors that correspond to the k largest
    #    eigenvalues to construct a d x k dimensional transformation matrix W;
    #    the eigenvectors are the columns of this matrix.
    # 7. Project the samples onto the new feature subspace using the
    #    transformation matrix W
    # LDA is similar to PCA, where the matrices are decomposed into
    # eigenvalues and eigenvectors, which will form lower dimensional feature
    # space.  However, LDA takes class label information into account, which
    # represented in the form of the mean vectors computed in step 2.  Since
    # the features in the Wine data set have already been standardised, the
    # first step can be skipped.
    # 2. Compute the d-dimensional mean vectors
    # Each mean vector stores the mean feature value with respect to the
    # samples of class i
    np.set_printoptions(precision=4)
    mean_vecs = []
    print('\nMean vectors for each class:')
    for label in range(1, 4):
        mean_vecs.append(np.mean(X_train_std[y_train == label], axis=0))
        print('\nMV %s: %s\n' % (label, mean_vecs[label - 1]))
    # 3. Compute the scatter matrices
    d = 13  # number of features
    S_W = np.zeros((d, d))
    for label, mv in zip(range(1, 4), mean_vecs):
        class_scatter = np.zeros((d, d))  # scatter matrix for each class
        for row in X_train_std[y_train == label]:
            row, mv = row.reshape(d, 1), mv.reshape(d, 1)  # make column vecs.
            class_scatter += (row - mv).dot((row - mv).T)
        S_W += class_scatter  # sum class scatter matrices
    print(
        '\nWithin-class scatter matrix: %sx%s' % (S_W.shape[0], S_W.shape[1]))
    # The assumption made when computing the scatter matrices is that the
    # class labels in the training data set are uniformly distributed.  If we
    # print the number of class labels, we see this assumption is invalid.
    print('\nClass label distribution: %s' % np.bincount(y_train)[1:])
    d = 13  # number of features
    S_W = np.zeros((d, d))
    for label, mv in zip(range(1, 4), mean_vecs):
        class_scatter = np.cov(X_train_std[y_train == label].T)
        S_W += class_scatter
    print('\nScaled within-class scatter matrix: %sx%s' % (S_W.shape[0],
                                                           S_W.shape[1]))
    # Compute the between class scatter matrix
    mean_overall = np.mean(X_train_std, axis=0)
    d = 13  # number of features
    S_B = np.zeros((d, d))
    for i, mean_vec in enumerate(mean_vecs):
        n = X_train[y_train == i + 1, :].shape[0]
        mean_vec = mean_vec.reshape(d, 1)  # make column vector
        mean_overall = mean_overall.reshape(d, 1)  # make column vector
        S_B += n * (mean_vec - mean_overall).dot((mean_vec - mean_overall).T)
    print(
        '\nBetween-class scatter matrix: %sx%s' % (S_B.shape[0], S_B.shape[1]))
    # 4. Compute the eigenvectors and corresponding eigenvalues
    # Selecting linear discriminants for the new feature subspace.  The
    # remaining steps of the LDA are similar to the steps of the PCA.  Instead
    # of performing the eigendecomposition on the covariance matrix, we solve
    # the generalised eigenvalue problem.
    eigen_vals, eigen_vecs = np.linalg.eig(np.linalg.inv(S_W).dot(S_B))
    # Sort the eigenvalues in descending order
    eigen_pairs = [(np.abs(eigen_vals[i]), eigen_vecs[:, i])
                   for i in range(len(eigen_vals))]
    eigen_pairs = sorted(eigen_pairs, key=lambda k: k[0], reverse=True)
    # Visually confirm that the list is correctly sorted by decreasing
    # eigenvalues
    print('\nEigenvalues in descending order:\n')
    for eigen_val in eigen_pairs:
        print(eigen_val[0])
    # In LDA the number of linear discriminants is at most c-1, where c is the
    # number of class lables, since the in-between scatter matrix is the sum
    # of c matrices with rank 1 or less.  The output shows we have only two
    # nonzero eigenvalues (eigenvalues 3-13 are not exactly zero, but this is
    # due to the floating point arithmetic in NumPy).  To measure how much of
    # the class-discriminatory information is caputred by the linear
    # discrimintants (eigenvectors), plto the linear discriminants by
    # decreasing eigenvalues.
    tot = sum(eigen_vals.real)
    discr = [(i / tot) for i in sorted(eigen_vals.real, reverse=True)]
    cum_discr = np.cumsum(discr)
    plt.bar(
        range(1, 14),
        discr,
        alpha=0.5,
        align='center',
        label='individual "discriminability"')
    plt.step(
        range(1, 14),
        cum_discr,
        where='mid',
        label='cumulative "discriminability"')
    plt.ylabel('"discriminability" ratio')
    plt.xlabel('Linear Discriminants')
    plt.ylim([-0.1, 1.1])
    plt.legend(loc='best')
    plt.title('Linear Discriminants')
    plt.show()
    # Stack the two most discriminative eigenvector columns to create the
    # transformation matrix W.
    w = np.hstack((eigen_pairs[0][1][:, np.newaxis].real,
                   eigen_pairs[1][1][:, np.newaxis].real))
    print('\nMatrix W:\n', w)
    # Projecting samples onto the new feature space
    # Using the transformation matrix W, we can now transform the training
    # data set by multiplying the matrices: X' = X.W
    X_train_lda = X_train_std.dot(w)
    colours = ['r', 'b', 'g']
    markers = ['s', 'x', 'o']
    for l, c, m in zip(np.unique(y_train), colours, markers):
        plt.scatter(
            X_train_lda[y_train == l, 0],
            X_train_lda[y_train == l, 1] * (-1),
            c=c,
            label=l,
            marker=m)
    plt.xlabel('LD 1')
    plt.ylabel('LD 2')
    plt.legend(loc='lower right')
    plt.title('New Feature Space')
    plt.show()
    # LDA via scikit-learn
    lda = LDA(solver='svd', n_components=2)
    X_train_lda = lda.fit_transform(X_train_std, y_train)
    # Examine how the logistic regression classifier handles the lower
    # dimensional training data set after the LDA tranformation.
    lr = LogisticRegression(solver='liblinear', multi_class='auto')
    lr.fit(X_train_lda, y_train)
    mlutils.plot_decision_regions(X_train_lda, y_train, classifier=lr)
    plt.xlabel('LD 1')
    plt.ylabel('LD 2')
    plt.legend(loc='lower left')
    plt.title('Logistic Regression Decision Regions')
    plt.show()
    # Looking at the resulting plot, we can see that the logistic regression
    # model misclassifies one of the samples from class 2.  By lowering the
    # regularisation strength, we could probably shift the decision boundaries
    # so that the logistic regression model classifies all samples in the
    # training data set correctly.  However, more importantly, examine the
    # results on the test set.
    X_test_lda = lda.transform(X_test_std)
    mlutils.plot_decision_regions(X_test_lda, y_test, classifier=lr)
    plt.xlabel('LD 1')
    plt.ylabel('LD 2')
    plt.legend(loc='lower left')
    plt.title('Test Data Set Decision Regions')
    plt.show()
    # Using kernel principal component analysis for nonlinear mappings.
    # Many machine learning algorithms make assumptions about the linear
    # separability of the input data.  The perceptron even requires perfectly
    # linearly separable training data to converge.  Other algorithms assume
    # the lack of perfect linear separability is due to noise: Adaline,
    # logistic regression, and the (standard) SVM.  However, if we are dealing
    # with nonlinear problems, which may be encountered frequently in real
    # world applications, linear transformation techniques for dimensionality
    # reduction, such as PCA and LDA, may not be the best choice.  This section
    # examines kernelised versions of PCA, or KPCA, which relates to the
    # concepts of kernel SVM.  Using kernel PCA, we can tranform data that is
    # not linearly separable into new, lower-dimensional subspace that is
    # suitable for linear classifiers.
    #
    # Kernel functions and the kernel trick.
    # We can tackle nonlinear problems by projecting them onto a new feature
    # space of higher dimensionality where the classes become linearly
    # separable.  A nonlinear mapping function (phi) is used to transform the
    # samples onto this higher k-dimensional subspace.  The mapping function
    # creates nonlinear combinations of the original features to map the
    # original d-dimensional data set onto the larger k-dimensional feature
    # space.  We perform nonlinear mapping via kernel PCA that transforms the
    # data onto a higher-dimensional space.  We then use standard PCA in this
    # higher-dimensional space to project the data back onto a lower
    # dimensional space, where the samples can be separated by a linear
    # classifier (under the condition that the samples can be separated by
    # density in the input space).  One downside of this approach is that it
    # is computationally very expensive, and this is where we use the kernel
    # trick.  Using the kernel trick, we can compute the similarity between
    # two high-dimension feature vectors in the original feature space.
    # Example 1 - separating half-moon shapes
    # Apply the RBF kernel PCA on some nonlinear example data sets.  Create
    # 2-dimensional data set of 100 sample points representing two half-moon
    # shapes.
    X, y = make_moons(n_samples=100, random_state=123)
    plt.scatter(X[y == 0, 0], X[y == 0, 1], color='red', marker='^', alpha=0.5)
    plt.scatter(
        X[y == 1, 0], X[y == 1, 1], color='blue', marker='o', alpha=0.5)
    plt.title('Example 1 - Half-moon sample data')
    plt.show()
    # Clearly, the half-moon shapes are not linearly separable, and the goal
    # is to unfold the half-moons via kernel PCA so that the data set can
    # serve as a suitable input for a linear classifier.  First examine how
    # the data set looks if wer project it onto the principal components via
    # standard PCA.
    scikit_pca = PCA(n_components=2)
    X_spca = scikit_pca.fit_transform(X)
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(7, 3))
    ax[0].scatter(
        X_spca[y == 0, 0],
        X_spca[y == 0, 1],
        color='red',
        marker='^',
        alpha=0.5)
    ax[0].scatter(
        X_spca[y == 1, 0],
        X_spca[y == 1, 1],
        color='blue',
        marker='o',
        alpha=0.5)
    ax[1].scatter(
        X_spca[y == 0, 0],
        np.zeros((50, 1)) + 0.02,
        color='red',
        marker='^',
        alpha=0.5)
    ax[1].scatter(
        X_spca[y == 1, 0],
        np.zeros((50, 1)) - 0.02,
        color='blue',
        marker='o',
        alpha=0.5)
    ax[0].set_xlabel('PC1')
    ax[0].set_ylabel('PC2')
    ax[1].set_ylim([-1, 1])
    ax[1].set_yticks([])
    ax[1].set_xlabel('PC1')
    plt.show()
    # Use the RBF kernel PCA function we implemented.
    X_kpca, lambdas = rbfkpca.rbf_kernel_pca(X, gamma=15, n_components=2)
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(7, 3))
    ax[0].scatter(
        X_kpca[y == 0, 0],
        X_kpca[y == 0, 1],
        color='red',
        marker='^',
        alpha=0.5)
    ax[0].scatter(
        X_kpca[y == 1, 0],
        X_kpca[y == 1, 1],
        color='blue',
        marker='o',
        alpha=0.5)
    ax[1].scatter(
        X_kpca[y == 0, 0],
        np.zeros((50, 1)) + 0.02,
        color='red',
        marker='^',
        alpha=0.5)
    ax[1].scatter(
        X_kpca[y == 1, 0],
        np.zeros((50, 1)) - 0.02,
        color='blue',
        marker='o',
        alpha=0.5)
    ax[0].set_xlabel('PC1')
    ax[0].set_ylabel('PC2')
    ax[1].set_ylim([-1, 1])
    ax[1].set_yticks([])
    ax[1].set_xlabel('PC1')
    plt.title('Example 1 - RBF kernel PCA')
    plt.show()
    # The plot shows that the two classes are linearly well separated so that
    # it becomes a suitable training data set for linear classifiers.
    # Unfortunately, there is not universal value for the tuning parameter
    # gamma that works well for different data sets.  Finding a value that is
    # appropriate for a given problem requires experimentation.
    # Example 2 - separating concentric circles
    X, y = make_circles(
        n_samples=1000, random_state=123, noise=0.1, factor=0.2)
    plt.scatter(X[y == 0, 0], X[y == 0, 1], color='red', marker='^', alpha=0.5)
    plt.scatter(
        X[y == 1, 0], X[y == 1, 1], color='blue', marker='o', alpha=0.5)
    plt.title('Concentric Circles')
    plt.show()
    # Start with the standard PCA approach to compare it to the results of the
    # RBF kernel PCA.
    scikit_pca = PCA(n_components=2)
    X_spca = scikit_pca.fit_transform(X)
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(7, 3))
    ax[0].scatter(
        X_spca[y == 0, 0],
        X_spca[y == 0, 1],
        color='red',
        marker='^',
        alpha=0.5)
    ax[0].scatter(
        X_spca[y == 1, 0],
        X_spca[y == 1, 1],
        color='blue',
        marker='o',
        alpha=0.5)
    ax[1].scatter(
        X_spca[y == 0, 0],
        np.zeros((500, 1)) + 0.02,
        color='red',
        marker='^',
        alpha=0.5)
    ax[1].scatter(
        X_spca[y == 1, 0],
        np.zeros((500, 1)) - 0.02,
        color='blue',
        marker='o',
        alpha=0.5)
    ax[0].set_xlabel('PC1')
    ax[0].set_ylabel('PC2')
    ax[1].set_ylim([-1, 1])
    ax[1].set_yticks([])
    ax[1].set_xlabel('PC1')
    plt.title('Standard PCA')
    plt.show()
    # The plot shows that the standard PCA is not able to produce results
    # suitable for training a linear classifier.  Given an appropriate value
    # for gamma, examine if the RBF kernel PCA provides better results.
    X_kpca, lambdas = rbfkpca.rbf_kernel_pca(X, gamma=15, n_components=2)
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(7, 3))
    ax[0].scatter(
        X_kpca[y == 0, 0],
        X_kpca[y == 0, 1],
        color='red',
        marker='^',
        alpha=0.5)
    ax[0].scatter(
        X_kpca[y == 1, 0],
        X_kpca[y == 1, 1],
        color='blue',
        marker='o',
        alpha=0.5)
    ax[1].scatter(
        X_kpca[y == 0, 0],
        np.zeros((500, 1)) + 0.02,
        color='red',
        marker='^',
        alpha=0.5)
    ax[1].scatter(
        X_kpca[y == 1, 0],
        np.zeros((500, 1)) - 0.02,
        color='blue',
        marker='o',
        alpha=0.5)
    ax[0].set_xlabel('PC1')
    ax[0].set_ylabel('PC2')
    ax[1].set_ylim([-1, 1])
    ax[1].set_yticks([])
    ax[1].set_xlabel('PC1')
    plt.title('RBF Kernel PCA')
    plt.show()
    # The RBF kernel PCA projected the data onto a new subspace where the two
    # classes become linearly separable.

    # Projecting new data points
    # In the previous two examples of kernel PCA, we projected a single data
    # set onto a new feature subspace.  In real applications, there may be more
    # than one data set that we want to tranform, such as training and test
    # data sets, and typically also new samples we will collect after the
    # model building and evaluation.  This section will examine how to project
    # data points that were not part of the training data set.

    # The standard PCA approach projects data by calculating the dot product
    # between a transformation matrix and the input samples; the columns of the
    # projection matrix are the top k eigenvectors (v) that were obtained from
    # the covariance matrix.  Now, the question is how can we tranfer this
    # concept to kernel PCA?

    # Create a new half-moon data set and project it onto a 1-d subspace using
    # the RBF kernel PCA
    X, y = make_moons(n_samples=100, random_state=123)
    alphas, lambdas = rbfkpca.rbf_kernel_pca(X, gamma=15, n_components=1)
    # To check we implemented the code for projecting new samples, let us
    # assume that the 26th point from the half-moon data set is a new data
    # point x', and our task is to project it onto this new subspace.
    x_new = X[25]
    print('\nNew half-moon data point (26th point):\n', x_new)
    x_proj = alphas[25]  # original projection
    print('\nOriginal projection:\n', x_proj)
    x_reproj = rbfkpca.project_x(
        x_new, X, gamma=15, alphas=alphas, lambdas=lambdas)
    print('\nReproduced original projection:\n', x_reproj)
    # Visualise the projection on the first principal component.
    plt.scatter(
        alphas[y == 0, 0], np.zeros((50)), color='red', marker='^', alpha=0.5)
    plt.scatter(
        alphas[y == 1, 0], np.zeros((50)), color='blue', marker='o', alpha=0.5)
    plt.scatter(
        x_proj,
        0,
        color='black',
        label='original projection of point X[25]',
        marker='^',
        s=100)
    plt.scatter(
        x_reproj,
        0,
        color='green',
        label='remapped point X[25]',
        marker='x',
        s=500)
    plt.legend(scatterpoints=1)
    plt.title('Projecting New Data Points')
    plt.show()
    # The plot shows that the new sample point x' was correctly mapped onto the
    # first principal component.
    #
    # Kernel principal component analysis in scikit-learn
    # Scikit-learn implements a kernel PCA class in the sklearn.decomposition
    # submodule.  The usage is similar to the standard PCA class, and the
    # kernel can be specified via the kernel parameter.
    X, y = make_moons(n_samples=100, random_state=123)
    scikit_kpca = KernelPCA(n_components=2, kernel='rbf', gamma=15)
    X_skernpca = scikit_kpca.fit_transform(X)

    plt.scatter(
        X_skernpca[y == 0, 0],
        X_skernpca[y == 0, 1],
        color='red',
        marker='^',
        alpha=0.5)
    plt.scatter(
        X_skernpca[y == 1, 0],
        X_skernpca[y == 1, 1],
        color='blue',
        marker='o',
        alpha=0.5)
    plt.xlabel('PC1')
    plt.ylabel('PC2')
    plt.title('Kernel PCA in scikit-learn')
    plt.show()
    # The results of this plot show that the KernelPCA implementation in
    # scikit-learn is consistent with our own implementation.


if __name__ == '__main__':
    main()
