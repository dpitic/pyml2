"""
Preprocess the IMDb movie data set into a more convenient format and store as
a CSV file.
"""

import os

import numpy as np
import pandas as pd
import pyprind


def main():
    # Directory of the movie data set
    basepath = 'aclImdb'
    labels = {'pos': 1, 'neg': 0}
    # 50k documents to be read
    pbar = pyprind.ProgBar(50000)
    df = pd.DataFrame()
    print('Preprocessing raw data in %s' % basepath)
    # Iterate over the train and test subdirectories
    for s in ('test', 'train'):
        # Iterate over the pos and neg subdirectories
        for l in ('pos', 'neg'):
            path = os.path.join(basepath, s, l)
            for file in sorted(os.listdir(path)):
                with open(os.path.join(path, file), 'r',
                          encoding='utf-8') as infile:
                    txt = infile.read()
                # Append file text with class labels; 1=positive, 0=negative
                df = df.append([[txt, labels[l]]], ignore_index=True)
                pbar.update()
    df.columns = ['review', 'sentiment']

    # Shuffle the DataFrame because the assembled data set is sorted
    np.random.seed(0)
    df = df.reindex(np.random.permutation(df.index))
    # Save data to CSV file
    csv_file = 'movie_data.csv'
    df.to_csv(csv_file, index=False, encoding='utf-8')

    # Print the first three samples and DataFrame shape
    df = pd.read_csv(csv_file, encoding='utf-8')
    print('\nHead 3:\n', df.head(3))
    print('\nShape:\n', df.shape)


if __name__ == '__main__':
    main()
