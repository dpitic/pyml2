"""
Main module for Chapter 8, intended to be executed after the raw IMDb data has
been downloaded and preprocessed into the CSV file.
"""

import os
import time

import pandas as pd
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

import preprocess as pp
import tokens


def main():
    # Load the pre-processed data file
    print('Reading IMDb data set file ...')
    df = pd.read_csv('movie_data.csv', encoding='utf-8')

    # Clean the data set
    print('Cleaning data set ...')
    df['review'] = df['review'].apply(pp.preprocessor)

    # Training a logistic regression model for document classification
    # This code will train a logistic regression model to classify the movie
    # reviews into positive and negative reviews.  First divide the DataFrame
    # of cleaned text documents into 25,000 documents for training and 25,000
    # documents for testing.
    X_train = df.loc[:25000, 'review'].values
    y_train = df.loc[:25000, 'sentiment'].values
    X_test = df.loc[25000:, 'review'].values
    y_test = df.loc[25000:, 'sentiment'].values
    # TfidfVectorizer combines CountVectorizer and TfidfTransformer
    tfidf = TfidfVectorizer(strip_accents=None,
                            lowercase=False,
                            preprocessor=None)

    # English stopwords
    stop = tokens.get_english_stopwords()

    # Trained model filename
    filename = 'gs_lr_tfidf.joblib'
    # Check whether model exists
    if os.path.isfile(filename):
        # Load the model
        print('Found model %s. Loading ...' % filename)
        gs_lr_tfidf = joblib.load(filename)
    else:
        # Create and train the model
        param_grid = [
            # TfidfVectorizer with default settings
            {
                'vect__ngram_range': [(1, 1)],
                'vect__stop_words': [stop, None],
                'vect__tokenizer':
                [tokens.tokenizer_simple, tokens.tokenizer_porter],
                'clf__penalty': ['l1', 'l2'],
                'clf__C': [1.0, 10.0, 100.0]
            },
            # TfidfVectorizer parameters to train model based on raw term
            # frequencies
            {
                'vect__ngram_range': [(1, 1)],
                'vect__stop_words': [stop, None],
                'vect__tokenizer':
                [tokens.tokenizer_simple, tokens.tokenizer_porter],
                'vect__use_idf': [False],
                'vect__norm': [None],
                'clf__penalty': ['l1', 'l2'],
                'clf__C': [1.0, 10.0, 100.0]
            },
        ]

        lr_tfidf = Pipeline([('vect', tfidf),
                             ('clf',
                              LogisticRegression(random_state=0,
                                                 solver='liblinear'))])
        # Use a GridSearchCV object to find the optimal set of parameters for
        # the logistic regression model using 5-fold stratified
        # cross-validation.
        gs_lr_tfidf = GridSearchCV(lr_tfidf,
                                   param_grid,
                                   scoring='accuracy',
                                   cv=5,
                                   verbose=1,
                                   n_jobs=-1)
        print('\nTraining model ...')
        start_time = time.time()
        gs_lr_tfidf.fit(X_train, y_train)
        elapsed_time = time.time() - start_time
        print('\nTraining time: ',
              time.strftime('%H:%M:%S', time.gmtime(elapsed_time)))

        # Save the trained model
        joblib.dump(gs_lr_tfidf, filename)
        print('\nModel saved as', filename)

    # Output model metrics
    print('Best parameter set:\n%s' % gs_lr_tfidf.best_params_)
    print('\nCV Accuracy %.3f' % gs_lr_tfidf.best_score_)
    clf = gs_lr_tfidf.best_estimator_
    print('Test Accuracy %.3f' % clf.score(X_test, y_test))


if __name__ == '__main__':
    main()
