"""
Bag of Words model.  This module demonstrates how to construct the vocabulary
of the bag of words model.
"""

import numpy as np
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer


def main():
    # Construct the vocabulary of the bag of words model and transform the
    # sentences into sparse feature vectors.
    count = CountVectorizer()
    docs = np.array([
        'The sun is shining', 'The weather is sweet',
        'The sun is shining, the weather is sweet, and one and one is two'
    ])
    bag = count.fit_transform(docs)
    # Print the contents of the vocabulary
    print('Vocabulary:\n', count.vocabulary_)
    # The vocabulary is stored as a Python dictionary that maps the unique
    # words to integer indices.  Print the feature vectors created.  Each
    # index position in the feature vectors corresponds to the integer values
    # stored as dictionary items in the vocabulary.  The rows represent the
    # sentences and the values are the count of words.  The values in the
    # feature vectors are called raw term frequencies tf(t,d); the number of
    # times a term t occurs in a document d.
    print('\nFeature vectors:\n', bag.toarray())

    # Accessing word relevancy via term frequency-inverse document frequency
    # Scikit-learn implements the TfidfTransformer class that takes the raw
    # term frequencies from the CountVectorizer class as input and transforms
    # them into tf-idfs
    tfidf = TfidfTransformer(use_idf=True, norm='l2', smooth_idf=True)
    np.set_printoptions(precision=2)
    print('\nTerm frequency-inverse document frequency (tfidf):\n',
          tfidf.fit_transform(count.fit_transform(docs)).toarray())
    # Manually calculating the tfidf
    tf_is = 3  # term frequency if 'is'
    n_docs = 3  # number of documents
    # Inverse document frequency of 'is'
    idf_is = np.log((n_docs + 1) / (3 + 1))
    tfidf_is = tf_is * (idf_is + 1)
    print('\ntf-idf of term "is" = %.2f' % tfidf_is)
    tfidf = TfidfTransformer(use_idf=True, norm=None, smooth_idf=True)
    raw_tfidf = tfidf.fit_transform(count.fit_transform(docs)).toarray()[-1]
    print('\nRaw tf-idf:\n', raw_tfidf)
    l2_tfidf = raw_tfidf / np.sqrt(np.sum(raw_tfidf**2))
    print('\nL2-normalised tf-idf:\n', l2_tfidf)


if __name__ == '__main__':
    main()
