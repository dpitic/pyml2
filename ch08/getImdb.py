"""
This script downloads the IMDb data set and decompresses it.
"""

import os
import sys
import tarfile
import time

source = 'http://ai.stanford.edu/~amaas/data/sentiment/aclImdb_v1.tar.gz'
target = 'aclImdb_v1.tar.gz'


def reporthook(count, block_size, total_size):
    """
    @brief      Download report hook.

    @details    Outputs the download progress to stdout.

    @param      count: number of blocks downloaded.
    @param      block_size: size of block.
    @param      total_size: total size of download.
    """
    global start_time
    if count == 0:
        start_time = time.time()
        return
    duration = time.time() - start_time
    progress_size = int(count * block_size)
    speed = progress_size / (1024.**2 * duration)
    percent = count * block_size * 100. / total_size
    sys.stdout.write('\r%d%% | %d MB | %.2f MB/s | %d sec elapsed' %
                     (percent, progress_size / (1024.**2), speed, duration))
    sys.stdout.flush()


def main():
    # Check if file is already downloaded
    if os.path.isfile(target):
        print('%s already downloaded' % target)
    else:
        # Download file
        if sys.version_info < (3, 0):
            # For Python 2 support only
            import urllib
            urllib.urlretrieve(source, target, reporthook)
        else:
            # Python 3
            import urllib.request
            print('Downloading data file ...')
            urllib.request.urlretrieve(source, target, reporthook)
    # If data set folder does not exit, extract data set.
    if not os.path.isdir('aclImdb'):
        print('\nDecompressing file ...')
        with tarfile.open(target, 'r:gz') as tar:
            tar.extractall()
    else:
        print('\n%s already decompressed. Exiting.' % target)


if __name__ == '__main__':
    main()
