"""
This module demonstrates topic modelling with Latent Dirichlet Allocation
(LDA) using scikit-learn on the IMDb data set.
"""

import pandas as pd
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer


def main():
    """
    LDA with scikit-learn
    Use the LatentDirichletAllocation class to decompose the movie review
    data set and categorise it into different topics.  The analysis is
    restricted to 10 different topics.
    """

    # Load the CSV data set file into a Pandas DataFrame
    data_file = 'movie_data.csv'
    print('Loading IMDb movie review data set from file ...')
    df = pd.read_csv(data_file, encoding='utf-8')
    print('Head:\n', df.head(3))

    # Use CountVectorizer to create the bag-of-words matrix as input to the
    # LDA, using scikit-learn built-in English stop word library.  Set maximum
    # document frequency of words to be considered to 10% in order to exclude
    # words that occur too frequently across documents.  The rationale behind
    # the removal of frequently occurring words is that these might be common
    # words appearing across all documents and are therefore less likely
    # associated with a specific topic category of a given document.  Also,
    # limit number of words to be considered to the most frequently occurring
    # 5,000 words, in order to limit the dimensionality of the data set so
    # that it improves the inference performed by the LDA.  These are both
    # arbitrarily chosen hyperparameter values.
    count = CountVectorizer(stop_words='english',
                            max_df=0.1,
                            max_features=5000)
    print('Creating the bag-of-words matrix ...')
    X = count.fit_transform(df['review'].values)
    # Create and fit a LatentDirichletAllocation estimator to the bag-of-words
    # matrix and infer the 10 different topics from the documents.
    lda = LatentDirichletAllocation(n_components=10,
                                    random_state=123,
                                    learning_method='batch',
                                    n_jobs=-1)
    print('Fitting Latent Dirichlet Allocation estimator ...')
    X_topics = lda.fit_transform(X)
    # The components_ attributes stores a matrix containing the word
    # importance
    print('Shape of word importance matrix:', lda.components_.shape)
    # To analyse the results, print the five most important words for each of
    # the 10 topics.  The word importance values are ranked in increasing
    # order, thus to print the top five words, we need to sort the topic array
    # in reverse order.
    n_top_words = 5
    feature_names = count.get_feature_names()
    print('5 most important words in each topic:')
    for topic_idx, topic in enumerate(lda.components_):
        print('Topic %d' % (topic_idx + 1))
        print(" ".join([feature_names[i] for i in topic.argsort()\
                        [:-n_top_words - 1:-1]]))

    # To confirm that the categories make sense based on the reviews, plot
    # three movies from the horror movie category (category 6 @ index 5).
    # Print the first 300 characters from the top three horror movies.
    horror = X_topics[:, 5].argsort()[::-1]
    for iter_idx, movie_idx in enumerate(horror[:3]):
        print('\nHorror movie #%d:' % (iter_idx + 1))
        print(df['review'][movie_idx][:300], '...')


if __name__ == '__main__':
    main()
