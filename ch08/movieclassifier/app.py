"""
Movie classifier Flask web application main file.
"""
import os
import pickle
import sqlite3

import numpy as np
from flask import Flask, render_template, request
from wtforms import Form, TextAreaField, validators

# Import HashingVectorizer from local directory
from vectorizer import vect

app = Flask(__name__)

# Prepare the classifier
cur_dir = os.path.dirname(__file__)
# The classifier object will be reset to its original pickled state every time
# the web application is restarted.
clf = pickle.load(open(os.path.join(cur_dir, 'pkl_objects', 'classifier.pkl'),
                       'rb'))
db = os.path.join(cur_dir, 'reviews.sqlite')


def classify(document):
    """Return the predicted class label for document.

    Negative classification is 0; positive classification is 1.

    :param str document: Input document to classify.
    :return: Classified label (str) and probability (float).
    """
    label = {0: 'negative', 1: 'positive'}
    X = vect.transform([document])
    y = clf.predict(X)[0]
    proba = np.max(clf.predict_proba(X))
    return label[y], proba


def train(document, y):
    """Update the classifier with the document and label.

    :param str document: Document to train the classifier.
    :param int y: Label for the document; 0 negative, 1 positive.
    """
    X = vect.transform([document])
    clf.partial_fit(X, [y])


def sqlite_entry(path, document, y):
    """Store the document and class label in the SQLite database.

    This function is used to store the submitted movie review in the SQLite
    database along with the class label and timestamp.

    :param str path: SQLite database path.
    :param str document: Review document to enter into the database.
    :param int y: Review class label; 0 is negative, 1 is positive.
    """
    conn = sqlite3.connect(path)
    c = conn.cursor()
    c.execute("INSERT INTO review_db (review, sentiment, date) "
              "VALUES (?, ?, DATETIME('now'))", (document, y))
    conn.commit()
    conn.close()


# Web application
class ReviewForm(Form):
    """Movie review input form with validation."""
    moviereview = TextAreaField('',
                                [validators.DataRequired(),
                                 validators.length(min == 15)])


@app.route('/')
def index():
    """Return the landing page of the web application.

    The landing page is the review form which requires the user to submit a
    valid movie review.

    :return: Review input form HTML.
    """
    form = ReviewForm(request.form)
    return render_template('reviewform.html', form=form)


@app.route('/results', methods=['POST'])
def results():
    """Return the predicted results page of the web application.

    This fetches the contents of the submitted web form and passes it onto the
    classifier to predict the sentiment of the movie, which is then displayed
    in the rendered page.

    :return: Predicted sentiment results page.
    """
    form = ReviewForm(request.form)
    if request.method == 'POST' and form.validate():
        review = request.form['moviereview']
        y, proba = classify(review)
        return render_template('results.html',
                               content=review,
                               prediction=y,
                               probability=round(proba * 100, 2))
    return render_template('reviewform.html', form=form)


@app.route('/thanks', methods=['POST'])
def feedback():
    """Obtain feedback on classification accuracy and thank the user.

    This function fetches the predicted class label from the results page if a
    user clicked on the feedback button, and transforms the predicted sentiment
    back into an integer class label that will be used to update the classifier
    via the train function.  Also, a new entry into the SQLite database will be
    made if feedback was provided, and subsequently a thank you message will be
    rendered.

    :return: Thank you page.
    """
    # Get information from web form
    feedback = request.form['feedback_button']
    review = request.form['review']
    prediction = request.form['prediction']

    inv_label = {'negative': 0, 'positive': 1}
    y = inv_label[prediction]
    if feedback == 'Incorrect':
        y = int(not (y))
    # Update the model with the user's feedback
    train(review, y)
    # Add user's review, with correction, to database
    sqlite_entry(db, review, y)
    return render_template('thanks.html')


if __name__ == '__main__':
    app.run(debug=True)
