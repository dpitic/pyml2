"""
This module demonstrates the concept of tokens.
"""
import os
import pickle
import re
import sqlite3
import sys
from distutils.version import LooseVersion as Version

import nltk
import numpy as np
import pyprind
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from sklearn import __version__ as sklearn_version
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.linear_model import SGDClassifier


def tokenizer_simple(text):
    """
    @brief      Word tokenizer.

    @details    Tokenize text string by splitting it into individual words
                separated by whitespace.

    @param      text: string to tokenize.

    @return     List of strings representing the individual words in the input
                text.
    """
    return text.split()


def tokenizer_porter(text):
    """
    @brief      Tokenize text string to reduce words to their root form.

    @details    Uses the Porter Stemmer algorithm.

    @param      text: string to tokenize.

    @return     List of word tokens in their root form.
    """
    porter = PorterStemmer()
    return [porter.stem(word) for word in text.split()]


def get_english_stopwords(verbose=False):
    """
    @brief      Return English stop words from NLTK.

    @details    Checks whether the NLTK data folder exists in the user's home
                directory before downloading the stop words.

    @param      verbose: Output to stdout if True; default=False

    @return     English stop words.
    """
    if not os.path.isdir(os.path.expanduser('~') + '/nltk_data'):
        if verbose:
            print('Downloading stopwords ...')
        nltk.download('stopwords')
    else:
        if verbose:
            print('Stopwords already downloaded, skipping.')
    stop = stopwords.words('english')
    return stop


def stream_docs(path):
    """
    @brief      Generator function that reads and returns one document at a
                time.

    @param      path: file containing lines of documents.

    @return     One document at a time.
    """
    with open(path, 'r', encoding='utf-8') as csv:
        next(csv)  # skip header
        for line in csv:
            text, label = line[:-3], int(line[-2])
            yield text, label


def get_minibatch(doc_stream, size):
    """
    @brief      Return a particular number of documents specified by the size
                parameter from the document stream.

    @param      doc_stream: document stream.
    @param      size: number of documents to return.

    @return     size number of documents
    """
    docs, y = [], []
    try:
        for _ in range(size):
            text, label = next(doc_stream)
            docs.append(text)
            y.append(label)
    except StopIteration:
        return None, None
    return docs, y


def main():
    # Preprocessing documents into tokens

    # Test the tokenizer() function
    print('\nTokenizer test:\n',
          tokenizer_simple('runners like running and thus they run'))

    # Word stemming is the process of transforming a word into its root form.
    # It enables words to be mapped to the same stem.  The original stemming
    # algorithm is known as the Porter Stemmer Algorithm, and is implemented
    # in the NLTK.
    print('\nPorter Stemmer tokenizer test:\n',
          tokenizer_porter('runners like running and thus they run'))

    # Stop word removal
    # Stop words are words that are extremely common in texts, but probably
    # bear no (or only little) useful information that can be used to
    # distinguish between different classes of documents.  Examples include
    # "is", "and", and "like".  Removing stop words can be useful if we are
    # working with raw or normalised term frequencies rather than tf-idfs,
    # which are already downweighting frequently occurring words.  In order to
    # remove the stop words from the movie reviews, we will use the set of 127
    # English stop words that are available from the NLTK library.
    stop = get_english_stopwords(verbose=True)
    string = 'a runner likes running and runs a lot'
    print('\nApply English stop word set to remove stop words:\n',
          [w for w in tokenizer_porter(string)[-10:] if w not in stop])

    # Test the stream_docs() function by reading the first document from the
    # CSV file
    filename = 'movie_data.csv'
    if not os.path.isfile(filename):
        sys.exit('Cannot find data file ' + str(filename))

    print('\nFirst document from data file:\n',
          next(stream_docs(path=filename)))

    def tokenizer(text):
        """
        @brief      Clean unprocessed text data and separate into word tokens
                    while removing stop words.

        @details    This preprocessor function removes punctuation marks from
                    the input text except for emoticon characters.  It cleans
                    unprocessed text data and separates it into word tokens
                    while removing stop words.

        @param      text: input text to be preprocessed.

        @return     Clean tokenized with stop words removed.
        """
        # Remove all of the HTML markup
        text = re.sub('<[^>]*>', '', text)
        # Find all emoticons
        emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text.lower())
        # Remove all non-word characters from the text string and convert to
        # lowercase characters, add emoticons to end of preprocessed document
        # string, and remove the nose character '-' from the emoticons for
        # consistency
        text = (re.sub('[\W]+', ' ', text.lower()) +
                ' '.join(emoticons).replace('-', ''))
        tokenized = [w for w in text.split() if w not in stop]
        return tokenized

    # Unfortunately, CountVectorizer cannot be used for out-of-core learning
    # because it requires holding the complete vocabulary in memory.  Also,
    # TfidfVectorizer needs to keep all the feature vectors of the training
    # data set in memory to calculate the inverse document frequencies.
    # Another useful vectorizer for text processing implemented in scikit-learn
    # is HashingVectorizer, which is data-independent and makes use of the
    # hashing trick via the 32-bit MurmurHash3 function.  Initialise a
    # HashingVectorizer object with the preprocesser tokenizer function, and
    # set a large number of features to reduce the chance of causing hash
    # collisions.
    vect = HashingVectorizer(decode_error='ignore',
                             n_features=2**21,
                             preprocessor=None,
                             tokenizer=tokenizer)

    # Initialise a logistic regression classifier in the SGDClassifier
    if Version(sklearn_version) < '0.18':
        clf = SGDClassifier(loss='log', random_state=1, n_iter=1)
    else:
        clf = SGDClassifier(loss='log', random_state=1, max_iter=1)

    doc_stream = stream_docs(path=filename)

    pbar = pyprind.ProgBar(45)  # 45 iterations
    classes = np.array([0, 1])
    # Start the out-of-core learning, iterating over 45 mini-batches of 1000
    # documents
    print('\nPerforming out-of-core learning with mini-batches of 1000 '
          'docs ...')
    for _ in range(45):
        X_train, y_train = get_minibatch(doc_stream, size=1000)
        if not X_train:
            break
        X_train = vect.transform(X_train)
        clf.partial_fit(X_train, y_train, classes=classes)
        pbar.update()

    # Use the last 5000 documents to evaluate the performance of the model
    print('\nUse the last 5000 documents to evaluate the performance '
          'of the model')
    X_test, y_test = get_minibatch(doc_stream, size=5000)
    X_test = vect.transform(X_test)
    print('Accuracy: %.3f' % clf.score(X_test, y_test))
    # Use the last 5000 documents (test data set) to update the model
    clf = clf.partial_fit(X_test, y_test)

    # Serialising fitted scikit-learn estimators
    # After training the logistic regression model, save the classifier along
    # with the stop words, Porter Stemmer, and HashingVectorizer as serialised
    # objects to local disk, for later use in the web application.
    dest = os.path.join('movieclassifier', 'pkl_objects')
    # Create the directory ./movieclassifier/pkl_objects if it doesn't exist
    if not os.path.exists(dest):
        print('Creating directory: ', dest)
        os.makedirs(dest)
    else:
        print('%s already exists, skipping.' % dest)

    # Save the stopwords and classifier
    pickle.dump(stop,
                open(os.path.join(dest, 'stopwords.pkl'), 'wb'),
                protocol=pickle.HIGHEST_PROTOCOL)
    pickle.dump(clf,
                open(os.path.join(dest, 'classifier.pkl'), 'wb'),
                protocol=pickle.HIGHEST_PROTOCOL)

    # Check that the objects were serialised correctly
    os.chdir('movieclassifier')
    # Load classifier
    print('Loading classifier ...')
    clf = pickle.load(open(os.path.join('pkl_objects', 'classifier.pkl'),
                           'rb'))
    label = {0: 'negative', 1: 'positive'}
    example = ['I love this movie']
    X = vect.transform(example)
    print('Example prediction: %s\nProbability: %.2f%%' %
          (label[clf.predict(X)[0]],
           np.max(clf.predict_proba(X))*100))

    # Setting up an SQLite database for data storage
    # Set up a simple SQLite database to collect optional feedback about the
    # predictions from users of the web application.  This feedback is used
    # to update the classification model.  The database will be created inside
    # the 'movieclassifier' directory, and store two example movie reviews.
    reviews_db = 'reviews.sqlite'
    # Check if the reviews database exists, because it must be deleted in
    # order to be able to rerun this code.  SQLite doesn't implement a replace
    # function for existing tables.
    if os.path.exists(reviews_db):
        print('Found existing instance of reviews database:', reviews_db)
        print('Removing database.')
        os.remove(reviews_db)
    else:
        print('Creating new reviews database:', reviews_db)

    # Create new reviews database
    conn = sqlite3.connect(reviews_db)
    c = conn.cursor()
    c.execute(
        'CREATE TABLE review_db (review TEXT, sentiment INTEGER, date TEXT)')

    # Insert sample reviews
    example1 = 'I love this movie'
    c.execute(
        "INSERT INTO review_db (review, sentiment, date) "
        "VALUES (?, ?, DATETIME('now'))", (example1, 1))
    example2 = 'I disliked this movie'
    c.execute(
        "INSERT INTO review_db (review, sentiment, date) "
        "VALUES (?, ?, DATETIME('now'))", (example2, 0))
    conn.commit()
    conn.close()

    # Check that the DB entries have been stored correctly
    print('Check the DB entries have been stored correctly ...')
    conn = sqlite3.connect(reviews_db)
    c = conn.cursor()
    c.execute("SELECT * FROM review_db WHERE date BETWEEN "
              "'2017-01-01 00:00:00' AND DATETIME('now')")
    results = c.fetchall()
    conn.close()
    print(results)


if __name__ == '__main__':
    main()
