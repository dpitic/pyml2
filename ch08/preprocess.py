"""
This module implements text preprocessing functions.
"""
import re

import pandas as pd


def preprocessor(text):
    """
    @brief      Text preprocessor to remove punctuation marks.

    @details    This preprocessor function removes punctuation marks from the
                input text except for emoticon characters.

    @param      text: input text to be preprocessed.

    @return     Preprocessed text string with punctuation removed.
    """
    # Remove all of the HTML markup
    text = re.sub('<[^>]*>', '', text)
    # Find all emoticons
    emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text)
    # Remove all non-word characters from the text string and convert to
    # lowercase characters, add emoticons to end of preprocessed document
    # string, and remove the nose character '-' from the emoticons for
    # consistency
    text = (re.sub('[\W]+', ' ', text.lower()) + ' '.join(emoticons).replace(
        '-', ''))
    return text


def main():
    # Load the pre-processed data file
    df = pd.read_csv('movie_data.csv', encoding='utf-8')

    # Cleaning text data
    # Before building the bag-of-words model, it is important to clean the text
    # data by striping it of all unwanted characters.  To illustrate the
    # importance of this, display the last 50 characters from the first
    # document in the reshuffled movie review data set.
    print('\nLast 50 chars from 1st document:\n', df.loc[0, 'review'][-50:])
    print('\nAgain, but after preprocessing:\n',
          preprocessor(df.loc[0, 'review'][-50:]))
    # Confirm the preprocessor works correctly
    print('\nTest string:\n', preprocessor("</a>This :) is :( a test :-)!"))


if __name__ == '__main__':
    main()
