# pyml2

Python Machine Learning 2nd ed. by Sebastian Raschka and Vahid Mirjalili, 2017.

## Notes

### Virtual Environment

The ```conda``` virtual environment used for development is ```ml```.  To ensure
compatibility across Linux and macOS and across Emacs, Sublime Text, and Visual
Studio Code, symlink the installed ```conda``` virtual environment ```envs/```
directory to ```${HOME}/.conda/envs/```.

### ```PYTHONPATH```

The ```PYTHONPATH``` environment variable must be set to include the
```mlutils/``` folder, in order to be able to execute the main Python scripts
from within the chapter sub-folders.

### ```pyprind```

Install ```pyprind``` Python module using Anaconda ```pip``` in the ```conda```
virtual environment used for development (```ml```).  This will install the
module in the appropriate ```site-packages/``` of the virtual environment.  Also
works as expected if installed in the default Python ```site-packages/```
directory, preferably as a user install.

Other Python modules such as ```pyls``` appear to work in the default Python
```site-packages/``` directory.  Also works if installed in the ```conda```
virtual environment used for development (```ml```).
